/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.famix.importer.bindingtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.launching.JavaRuntime;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.preferences.EvolizerPreferences;
import org.evolizer.famix.importer.FamixImporterPlugin;
import org.evolizer.famix.importer.FamixModelFactory;
import org.evolizer.famix.importer.ProjectParser;
import org.evolizer.famix.importer.unresolved.UnresolvedInvocationHandler;
import org.evolizer.famix.importer.util.DAOModel;
import org.evolizer.famix.model.entities.AbstractFamixEntity;
import org.evolizer.famix.model.entities.FamixAssociation;
import org.evolizer.famix.model.entities.FamixAttribute;
import org.evolizer.famix.model.entities.FamixClass;
import org.evolizer.famix.model.entities.FamixLocalVariable;
import org.evolizer.famix.model.entities.FamixMethod;
import org.evolizer.famix.model.entities.FamixModel;
import org.evolizer.famix.model.entities.FamixParameter;
import org.evolizer.famix.model.entities.SourceAnchor;
import org.junit.BeforeClass;
import org.junit.Test;

public class UnresolvedDataBaseTest extends AbstractEvolizerFamixImporterTest{
	private static FamixModel createdModel;
	private static DAOModel aDAOModel;
	private static FamixModel aModel;
	private static FamixModelFactory aFactory = new FamixModelFactory();
	private static UnresolvedInvocationHandler fUnresolvedInvocationHandler;

	private static void setUpProject() throws CoreException, IOException{
		String name = "TestProject";
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root= workspace.getRoot();
		root.delete(true, false, null);
		IProject project= root.getProject(name);
		project.create(null);
		project.open(null);
		
		IProjectDescription desc = project.getDescription();
		desc.setNatureIds(new String[] {
				JavaCore.NATURE_ID});
		project.setDescription(desc, null);
		
		IJavaProject javaProj = JavaCore.create(project);		
		IFolder binDir = project.getFolder("bin");
		IPath binPath = binDir.getFullPath();
		javaProj.setOutputLocation(binPath, null);
		
		IClasspathEntry cpe = JavaRuntime.getDefaultJREContainerEntry();
		javaProj.setRawClasspath(new IClasspathEntry[] {cpe},null);
		
		IFolder folder = project.getFolder("src");
		folder.create(true, true, null);
		IClasspathEntry entry = JavaCore.newSourceEntry(folder.getFullPath());
		IClasspathEntry[] entries = javaProj.getRawClasspath();
		IClasspathEntry[] newEntries = new IClasspathEntry[entries.length + 1];
		System.arraycopy(entries, 0, newEntries, 0, entries.length);
		newEntries[newEntries.length - 1] = entry;
		javaProj.setRawClasspath(newEntries, null);
		
		IPackageFragmentRoot javaRoot = javaProj.getPackageFragmentRoot(folder);
		javaRoot.createPackageFragment("testPackage", true, null);
		javaRoot = javaProj.getPackageFragmentRoot(folder);
		IPackageFragment packageFragment = javaRoot.getPackageFragment("testPackage");
		IFolder packageFolder = project.getFolder(packageFragment.getResource().getProjectRelativePath());
		IFile sourceFile = packageFolder.getFile("Test.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/Test.java"), IResource.NONE, null);
		
		sourceFile = packageFolder.getFile("Test2.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/Test2.java"), IResource.NONE, null);
		
		sourceFile = packageFolder.getFile("Test3.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/Test3.java"), IResource.NONE, null);
		
		sourceFile = packageFolder.getFile("Test4.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/Test4.java"), IResource.NONE, null);
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		setUpProject();
		AbstractEvolizerFamixImporterTest.setUpBeforeClass();
		
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IJavaModel model =  JavaCore.create(workspace.getRoot());
		IJavaProject project = model.getJavaProject("TestProject");
		
		project.getProject().setPersistentProperty(EvolizerPreferences.DB_HOST, "mysql://localhost");
		project.getProject().setPersistentProperty(EvolizerPreferences.DB_NAME, "evolizer_test");
		project.getProject().setPersistentProperty(EvolizerPreferences.DB_USER, "evolizer");
		project.getProject().setPersistentProperty(EvolizerPreferences.DB_PASSWORD, "evolizer");
		EvolizerSessionHandler.getHandler().dropSchema(project.getProject());
		EvolizerSessionHandler.getHandler().createSchema(project.getProject());
		
		List<IJavaElement> selection = new ArrayList<IJavaElement>();
		selection.add(project);
		ProjectParser parser = new ProjectParser(selection);
		parser.parse(null);
		createdModel = parser.getModel();
		
		fUnresolvedInvocationHandler = new UnresolvedInvocationHandler(createdModel, parser.getUnresolvedCalls());
		fUnresolvedInvocationHandler.process(new NullProgressMonitor());
		fUnresolvedInvocationHandler.addInvocations(new NullProgressMonitor());
		
		EvolizerSessionHandler.getHandler().getCurrentSession(project.getProject());
		aDAOModel = new DAOModel(EvolizerSessionHandler.getDBUrl(project.getProject()),createdModel);
		aDAOModel.store(null);
		aModel = aDAOModel.loadModel(createdModel.getName());
	}
	
	@Test
	public void testUnresolvedInheritance(){
		FamixClass clazz = (FamixClass)aModel.getElement(aFactory.createClass("testPackage.Test",null));
		assertNotNull(clazz);
		FamixClass superClass = (FamixClass) aModel.getElement(aFactory.createClass("<undef>.SomeClass",null));
		assertNotNull(superClass);
		
		Set<FamixAssociation> lRelations = aModel.getAssociations(superClass);
		assertTrue("FamixClass " + superClass.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		int containsInheritsTo = TestHelper.containsRelationTo(superClass, lRelations);
		assertTrue("Missing inheritance relationship in base class" + clazz.getUniqueName() + " to " + superClass.getUniqueName(), containsInheritsTo > 0);
		
		lRelations = aModel.getAssociations(clazz);
		assertTrue("FamixClass " + clazz.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		containsInheritsTo = TestHelper.containsRelationTo(superClass, lRelations);
		assertTrue("Missing inheritance relationship in sub class " + clazz.getUniqueName() + " to " + superClass.getUniqueName(), containsInheritsTo > 0);
	}
	
	@Test
	public void testUnresolvedInterfaces(){
		FamixClass interfaceClass = (FamixClass) aModel.getElement(aFactory.createClass("<undef>.SomeInterface",null));
		FamixClass baseClass = (FamixClass) aModel.getElement(aFactory.createClass("testPackage.Test",null));
		assertNotNull(interfaceClass);
		assertNotNull(baseClass);
		
		Set<FamixAssociation> lRelations = aModel.getAssociations(baseClass);
		assertTrue("FamixClass " + baseClass.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		int containsInheritsTo = TestHelper.containsRelationTo(interfaceClass, lRelations);
		assertTrue("Missing inheritance relationship in class" + baseClass.getUniqueName() + " to " + interfaceClass.getUniqueName(), containsInheritsTo > 0);
		
		lRelations = aModel.getAssociations(interfaceClass);
		assertTrue("FamixClass " + interfaceClass.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		containsInheritsTo = TestHelper.containsRelationTo(interfaceClass, lRelations);
		assertTrue("Missing inheritance relationship in sub class " + baseClass.getUniqueName() + " to " + interfaceClass.getUniqueName(), containsInheritsTo > 0);
	}
	
	@Test
	public void testUnresolvedAttribute(){
		FamixClass clazz = (FamixClass)aModel.getElement(aFactory.createClass("<undef>.Warrior",null));
		FamixAttribute simpleAttribute = (FamixAttribute) aModel.getElement(aFactory.createAttribute("testPackage.Test.gigs",null));
		assertNotNull(clazz);
		assertNotNull(simpleAttribute);
		assertEquals(simpleAttribute.getDeclaredClass(), clazz);
		
		assertEquals("Type string of " + simpleAttribute.getUniqueName() + " must have modifier PRIVATE", 
				simpleAttribute.getModifiers() & Modifier.PRIVATE, Modifier.PRIVATE);
		assertEquals("Type string of " + simpleAttribute.getUniqueName() + " must have modifier STATIC", 
				simpleAttribute.getModifiers() & Modifier.STATIC, Modifier.STATIC);
		assertEquals("Type string of " + simpleAttribute.getUniqueName() + " must have modifier FINAL", 
				simpleAttribute.getModifiers() & Modifier.FINAL, Modifier.FINAL);
	}
	
	
	@Test
	public void testMethodParameters(){
		FamixMethod method = (FamixMethod)aModel.getElement(aFactory.createMethod("testPackage.Test.myMethod(int,double,<undef>.Warlock)",null));
		assertNotNull(method);
		assertEquals(3, method.getParameters().size());
		
		FamixParameter param = (FamixParameter) aModel.getElement(aFactory.createFormalParameter("testPackage.Test.myMethod(int,double,<undef>.Warlock).i",null,0));
		assertNotNull(param);
		assertEquals(new Integer(0), param.getParamIndex());
		
		FamixClass clazz = (FamixClass) aModel.getElement(aFactory.createClass("int",null));
		assertEquals(clazz, param.getDeclaredClass());
		assertTrue(method.getParameters().contains(param));
		assertEquals(method, param.getParent());
		
		param = (FamixParameter) aModel.getElement(aFactory.createFormalParameter("testPackage.Test.myMethod(int,double,<undef>.Warlock).d",null,1));
		clazz = (FamixClass) aModel.getElement(aFactory.createClass("double",null));
		assertEquals(clazz, param.getDeclaredClass());
		assertNotNull(param);
		assertTrue(method.getParameters().contains(param));
		assertEquals(method, param.getParent());
		assertEquals(new Integer(1), param.getParamIndex());
		
		param = (FamixParameter) aModel.getElement(aFactory.createFormalParameter("testPackage.Test.myMethod(int,double,<undef>.Warlock).zorf",null,2));
		clazz = (FamixClass) aModel.getElement(aFactory.createClass("<undef>.Warlock",null));
		assertEquals(clazz, param.getDeclaredClass());
		assertNotNull(param);
		assertTrue(method.getParameters().contains(param));
		assertEquals(method, param.getParent());
		assertEquals(new Integer(2), param.getParamIndex());
	}
	
	@Test
	public void testUnresolvedArrayParameter(){
		FamixMethod method = (FamixMethod)aModel.getElement(aFactory.createMethod("testPackage.Test.myMethod(<undef>.Hunter[])",null));
		assertNotNull(method);
		
		FamixParameter param = (FamixParameter) aModel.getElement(aFactory.createFormalParameter("testPackage.Test.myMethod(<undef>.Hunter[]).h",null,0));
		assertNotNull(param);
		assertEquals(new Integer(0), param.getParamIndex());
		FamixClass clazz = (FamixClass)aModel.getElement(aFactory.createClass(AbstractFamixEntity.ARRAY_TYPE_NAME,null));
		assertEquals(clazz, param.getDeclaredClass());
		assertTrue(method.getParameters().contains(param));
		assertEquals(method, param.getParent());
	}
	
	@Test
	public void testUnresolvedReturnParameter(){
		FamixClass clazz = (FamixClass) aModel.getElement(aFactory.createClass("<undef>.Dudu",null));
		assertNotNull(clazz);
		FamixMethod method = (FamixMethod)aModel.getElement(aFactory.createMethod("testPackage.Test.someMethod()",null));
		assertNotNull(method);
		assertEquals(clazz, method.getDeclaredReturnClass());
	}
	
	@Test
	public void testUnresolvedArrayReturnParameter(){
		FamixClass clazz = (FamixClass)aModel.getElement(aFactory.createClass(AbstractFamixEntity.ARRAY_TYPE_NAME,null));
		assertNotNull(clazz);
		FamixMethod method = (FamixMethod)aModel.getElement(aFactory.createMethod("testPackage.Test.method()",null));
		assertNotNull(method);
		assertEquals(method.getDeclaredReturnClass(), clazz);
	}
	
	@Test
	public void testUnresolvedArrayAttribute(){
		FamixClass clazz = (FamixClass)aModel.getElement(aFactory.createClass(AbstractFamixEntity.ARRAY_TYPE_NAME,null));
		assertNotNull(clazz);
		FamixAttribute simpleAttribute = (FamixAttribute) aModel.getElement(aFactory.createAttribute("testPackage.Test2.paladins",null));
		assertNotNull(simpleAttribute);
		assertEquals(clazz, simpleAttribute.getDeclaredClass());
	}
	
	@Test
	public void testUnresolvedMethodInvocation(){
		FamixMethod caller = (FamixMethod)aModel.getElement(aFactory.createMethod("testPackage.Test2.<init>()",null));
		assertNotNull(caller);
		FamixMethod callee = (FamixMethod)aModel.getElement(aFactory.createMethod("<undef>.Paladin.palaStats()",null));
		for(AbstractFamixEntity entity : aModel.getFamixEntities()){
			System.out.println(entity);
		}
		assertNotNull(callee);
		
		Set<FamixAssociation> callerRelations = aModel.getAssociations(caller);
		assertTrue("FamixMethod " + caller.getUniqueName() + " must contain relationships", callerRelations.size() > 0);
		
		int containsInvocationTo = TestHelper.containsRelationTo(callee, callerRelations);
		assertTrue("Missing invocation relationship from " + caller.getUniqueName() + " to " + callee.getUniqueName(), containsInvocationTo > 0);
	}
	
	@Test
	public void testUnresolvedMethodInvocationWithParam(){
		FamixMethod callee = (FamixMethod)aModel.getElement(aFactory.createMethod("<undef>.Paladin.heal(int,java.lang.Object,<undef>)",null));
		assertNotNull(callee);
		
		FamixMethod caller = (FamixMethod)aModel.getElement(aFactory.createMethod("testPackage.Test2.<init>()",null));
		assertNotNull(caller);
		
		Set<FamixAssociation> callerRelations = aModel.getAssociations(caller);
		assertTrue("FamixMethod " + caller.getUniqueName() + " must contain relationships", callerRelations.size() > 0);
		
		int containsInvocationTo = TestHelper.containsRelationTo(callee, callerRelations);
		assertTrue("Missing invocation relationship from " + caller.getUniqueName() + " to " + callee.getUniqueName(), containsInvocationTo > 0);
	}
	
	@Test
	public void testUnresolvedSuperConstructorInvocation(){
		FamixMethod caller = (FamixMethod)aModel.getElement(aFactory.createMethod("testPackage.Test3.<init>()",null));
		assertNotNull(caller);
		
		FamixMethod unresolvedSuperConstructor = (FamixMethod)aModel.getElement(aFactory.createMethod("<undef>.<init>(double,java.lang.Object,<undef>,<undef>)",null));
		assertNotNull(unresolvedSuperConstructor);
		
		Set<FamixAssociation> lRelations = aModel.getAssociations(caller);
		assertTrue("FamixMethod " + caller.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		int containsInvocationTo = TestHelper.containsRelationTo(unresolvedSuperConstructor, lRelations);
		assertTrue("Missing super constructor invocation relationship from " + caller.getUniqueName() + " to " + unresolvedSuperConstructor.getUniqueName(), containsInvocationTo > 0);

		lRelations = aModel.getAssociations(unresolvedSuperConstructor);
		assertTrue("FamixMethod " + unresolvedSuperConstructor.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		containsInvocationTo = TestHelper.containsRelationTo(unresolvedSuperConstructor, lRelations);
		assertTrue("Missing super constructor invocation relationship from " + caller.getUniqueName() + " to " + unresolvedSuperConstructor.getUniqueName(), containsInvocationTo > 0);
	}
	
	@Test
	public void testUnresolvedSuperMethodInvocation(){
		
		FamixMethod caller = (FamixMethod)aModel.getElement(aFactory.createMethod("testPackage.Test3.<init>()",null));
		assertNotNull(caller);
		
		FamixMethod unresolvedSuperMethod = (FamixMethod)aModel.getElement(aFactory.createMethod("<undef>.method(java.lang.String,<undef>)",null));
		assertNotNull(unresolvedSuperMethod);
		
		Set<FamixAssociation> lRelations = aModel.getAssociations(caller);
		assertTrue("FamixMethod " + caller.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		int containsInvocationTo = TestHelper.containsRelationTo(unresolvedSuperMethod, lRelations);
		assertTrue("Missing super constructor invocation relationship from " + caller.getUniqueName() + " to " + unresolvedSuperMethod.getUniqueName(), containsInvocationTo > 0);

		lRelations = aModel.getAssociations(unresolvedSuperMethod);
		assertTrue("FamixMethod " + unresolvedSuperMethod.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		containsInvocationTo = TestHelper.containsRelationTo(unresolvedSuperMethod, lRelations);
		assertTrue("Missing super constructor invocation relationship from " + caller.getUniqueName() + " to " + unresolvedSuperMethod.getUniqueName(), containsInvocationTo > 0);
		
	}
	
	@Test
	public void testMethodLocalVariableContainsSimple() {
		FamixMethod simpleMethod = (FamixMethod) aModel.getElement(aFactory.createMethod("testPackage.Test3.gather()",null));
		assertNotNull(simpleMethod);
		FamixLocalVariable simpleLocalVariableWithoutAnchor = aFactory.createLocalVariable("testPackage.Test3.gather().a",simpleMethod);
		simpleLocalVariableWithoutAnchor.setSourceAnchor(new SourceAnchor("/TestProject/src/testPackage/Test3.java",new Integer(203),new Integer(204)));
		FamixLocalVariable simpleLocal = (FamixLocalVariable) aModel.getElement(simpleLocalVariableWithoutAnchor);
		assertNotNull(simpleLocal);
		
		assertTrue("FamixMethod must contain local variable", simpleMethod.getLocalVariables().size() > 0);
		boolean containsLocal = containsLocalVariable(simpleMethod, simpleLocal);
		assertTrue("FamixMethod must contain local variable simpleLocal", containsLocal);
		assertEquals("No or wrong parent method for local variable simpleLocal", simpleMethod, simpleLocal.getParent());
		
		FamixClass clazz = (FamixClass) aModel.getElement(aFactory.createClass("<undef>.EliteBoss",null));
		assertNotNull(clazz);
		assertEquals(clazz, simpleLocal.getDeclaredClass());
	}
	
	@Test
	public void testMethodLocalVariableContainsMulti() {
		FamixMethod simpleMethod = (FamixMethod) aModel.getElement(aFactory.createMethod("testPackage.Test3.gather()",null));
		assertNotNull(simpleMethod);
		
		FamixLocalVariable multiLocal1WithoutAnchor = aFactory.createLocalVariable("testPackage.Test3.gather().gigs1",simpleMethod);
		multiLocal1WithoutAnchor.setSourceAnchor(new SourceAnchor("/TestProject/src/testPackage/Test3.java",249,254));
		FamixLocalVariable multiLocal1 = (FamixLocalVariable) aModel.getElement(multiLocal1WithoutAnchor);
		FamixLocalVariable multiLocal2WithoutAnchor = aFactory.createLocalVariable("testPackage.Test3.gather().gigs2",simpleMethod);
		multiLocal2WithoutAnchor.setSourceAnchor(new SourceAnchor("/TestProject/src/testPackage/Test3.java",257,262));
		FamixLocalVariable multiLocal2 = (FamixLocalVariable) aModel.getElement(multiLocal2WithoutAnchor);
		
		assertNotNull(multiLocal1);
		assertNotNull(multiLocal2);
		
		assertTrue("FamixMethod must contain local variable", simpleMethod.getLocalVariables().size() > 0);
		
		FamixClass clazz = (FamixClass)aModel.getElement(aFactory.createClass("<undef>.Gigs",null));
		assertNotNull(clazz);
		
		boolean containsLocal1 = containsLocalVariable(simpleMethod, multiLocal1);
		assertTrue("FamixMethod must contain local variable multiLocal1", containsLocal1);
		assertEquals("No or wrong parent method for local variable multiLocal1", simpleMethod, multiLocal1.getParent());
		assertEquals(clazz, multiLocal1.getDeclaredClass());

		boolean containsLocal2 = containsLocalVariable(simpleMethod, multiLocal2);
		assertTrue("FamixMethod must contain local variable multiLocal2", containsLocal2);
		assertEquals("No or wrong parent method for local variable multiLocal2", simpleMethod, multiLocal2.getParent());
		assertEquals(clazz, multiLocal2.getDeclaredClass());
	}
	
	@Test
	public void testMethodLocalVariableContainsWithinFor() {
		FamixMethod simpleMethod = (FamixMethod) aModel.getElement(aFactory.createMethod("testPackage.Test4.iterate()",null));
		assertNotNull(simpleMethod);
		
		FamixLocalVariable withinForWithoutAnchor = aFactory.createLocalVariable("testPackage.Test4.iterate().iter",simpleMethod);
		withinForWithoutAnchor.setSourceAnchor(new SourceAnchor("/TestProject/src/testPackage/Test4.java",152,173));
		FamixLocalVariable withinFor = (FamixLocalVariable) aModel.getElement(withinForWithoutAnchor);

		assertNotNull(withinFor);
		
		assertTrue("FamixMethod must contain local variable", simpleMethod.getLocalVariables().size() > 0);

		boolean containsLocal = containsLocalVariable(simpleMethod, withinFor);
		assertTrue("FamixMethod must contain local variable nrs within for loop", containsLocal);
		assertEquals("No or wrong parent method for local variable nrs within for loop", simpleMethod, withinFor.getParent());
		
		FamixClass clazz = (FamixClass) aModel.getElement(aFactory.createClass("<undef>",null));
		assertNotNull(clazz);
		
		assertEquals(clazz, withinFor.getDeclaredClass());
		
	}
	
	@Test
	public void testUnresolvedClassInstanceCreation(){
		FamixMethod calledConstructor = (FamixMethod) aModel.getElement(aFactory.createMethod("<undef>.Player.<init>(double,java.lang.Object,<undef>)",null));
		assertNotNull(calledConstructor);
		
		FamixMethod caller = (FamixMethod)aModel.getElement(aFactory.createMethod("testPackage.Test4.<oinit>()",null));
		assertNotNull(caller);
		
		Set<FamixAssociation> lRelations = aModel.getAssociations(caller);
		assertTrue("FamixMethod " + caller.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		int containsInvocationTo = TestHelper.containsRelationTo(calledConstructor, lRelations);
		assertTrue("Missing instance creation invocation relationship from " + caller.getUniqueName() + " to " + calledConstructor.getUniqueName(), containsInvocationTo > 0);
		
		lRelations = aModel.getAssociations(calledConstructor);
		assertTrue("FamixMethod " + calledConstructor.getUniqueName() + " must contain relationships", lRelations.size() > 0);
		containsInvocationTo = TestHelper.containsRelationTo(calledConstructor, lRelations);
		assertTrue("Missing instance creation invocation relationship from " + caller.getUniqueName() + " to " + calledConstructor.getUniqueName(), containsInvocationTo > 0);
	}
	
	@Test
	public void testArrayLocalVariable(){
		FamixMethod simpleMethod = (FamixMethod) aModel.getElement(aFactory.createMethod("testPackage.Test3.gather()",null));
		assertNotNull(simpleMethod);
		FamixLocalVariable lVarWithout = aFactory.createLocalVariable("testPackage.Test3.gather().items", simpleMethod);
		lVarWithout.setSourceAnchor(new SourceAnchor("/TestProject/src/testPackage/Test3.java",216,235));
		FamixLocalVariable lVar = (FamixLocalVariable)aModel.getElement(lVarWithout);

		assertNotNull(lVar);
		FamixClass declClass = (FamixClass) aModel.getElement(aFactory.createClass(AbstractFamixEntity.ARRAY_TYPE_NAME, null));
		assertNotNull(declClass);
		
		assertEquals(declClass, lVar.getDeclaredClass());
	}
	
	private boolean containsLocalVariable(FamixMethod simpleMethod, FamixLocalVariable simpleLocal) {
		boolean containsLocal = false;
		for (FamixLocalVariable lLocal : simpleMethod.getLocalVariables()) {
			if (lLocal.getUniqueName().equals(simpleLocal.getUniqueName())) {
				containsLocal = true;
				break;
			}
		}
		return containsLocal;
	}
	
}
