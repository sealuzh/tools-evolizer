/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.famix.importer.bindingtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.JavaRuntime;
import org.evolizer.famix.importer.FamixImporterPlugin;
import org.evolizer.famix.importer.FamixModelFactory;
import org.evolizer.famix.importer.ProjectParser;
import org.evolizer.famix.importer.unresolved.UnresolvedInvocationHandler;
import org.evolizer.famix.importer.unresolved.UnresolvedMethodInvocation;
import org.evolizer.famix.model.entities.AbstractFamixEntity;
import org.evolizer.famix.model.entities.FamixAssociation;
import org.evolizer.famix.model.entities.FamixAttribute;
import org.evolizer.famix.model.entities.FamixClass;
import org.evolizer.famix.model.entities.FamixMethod;
import org.evolizer.famix.model.entities.FamixModel;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author pinzger
 *
 */
public class UnresolvedCallTest {
	
	private static FamixModel fModel;
	private static UnresolvedInvocationHandler fUnresolvedInvocationHandler;
	private static FamixModelFactory fFactory = new FamixModelFactory();

	private static void setUpProject() throws CoreException, IOException{
		String name = "TestProject";
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root= workspace.getRoot();
		root.delete(true, false, null);
		IProject project= root.getProject(name);
		project.create(null);
		project.open(null);
		
		IProjectDescription desc = project.getDescription();
		desc.setNatureIds(new String[] {
				JavaCore.NATURE_ID});
		project.setDescription(desc, null);
		
		IJavaProject javaProj = JavaCore.create(project);
		IFolder binDir = project.getFolder("bin");
		IPath binPath = binDir.getFullPath();
		javaProj.setOutputLocation(binPath, null);
		
		IClasspathEntry cpe = JavaRuntime.getDefaultJREContainerEntry();
		javaProj.setRawClasspath(new IClasspathEntry[] {cpe},null);
		
		IFolder folder = project.getFolder("src");
		folder.create(true, true, null);
		IClasspathEntry entry = JavaCore.newSourceEntry(folder.getFullPath());
		IClasspathEntry[] entries = javaProj.getRawClasspath();
		IClasspathEntry[] newEntries = new IClasspathEntry[entries.length + 1];
		System.arraycopy(entries, 0, newEntries, 0, entries.length);
		newEntries[newEntries.length - 1] = entry;
		javaProj.setRawClasspath(newEntries, null);
		
		IPackageFragmentRoot javaRoot = javaProj.getPackageFragmentRoot(folder);
		javaRoot.createPackageFragment("testPackage", true, null);
		javaRoot = javaProj.getPackageFragmentRoot(folder);
		IPackageFragment packageFragment = javaRoot.getPackageFragment("testPackage");
		IFolder packageFolder = project.getFolder(packageFragment.getResource().getProjectRelativePath());
		IFile sourceFile = packageFolder.getFile("UnresolvedCalls.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/UnresolvedCalls.java"), IResource.NONE, null);

		sourceFile = packageFolder.getFile("Dummy.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/Dummy.java"), IResource.NONE, null);

		sourceFile = packageFolder.getFile("DummySub.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/DummySub.java"), IResource.NONE, null);

        sourceFile = packageFolder.getFile("UnresolvedTypeParameters.java");
        sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/UnresolvedTypeParameters.java"), IResource.NONE, null);
        
        sourceFile = packageFolder.getFile("UnresolvedInterface.java");
        sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/UnresolvedInterface.java"), IResource.NONE, null);
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		setUpProject();
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IJavaModel model =  JavaCore.create(workspace.getRoot());
		IJavaProject project = model.getJavaProject("TestProject");
		List<IJavaElement> selection = new ArrayList<IJavaElement>();
		selection.add(project);
		ProjectParser parser = new ProjectParser(selection);
		parser.parse(null);
		fModel = parser.getModel();
		
		fUnresolvedInvocationHandler = new UnresolvedInvocationHandler(fModel, parser.getUnresolvedCalls());
		fUnresolvedInvocationHandler.process(new NullProgressMonitor());
		fUnresolvedInvocationHandler.addInvocations(new NullProgressMonitor());
		
//      output all entities and source anchors
        for (AbstractFamixEntity entity : fModel.getFamixEntities()) {
            System.out.print(entity.getUniqueName());
            if (entity.getSourceAnchor() != null) {
                System.out.print(": " + entity.getSourceAnchor().toString());
            }
            System.out.println();
        }
	}

	@Test
	public void testCallUndefType() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.callUndefType()", null));
		assertNotNull("FamixModel must contain method testPackage.UnresolvedCalls.callUndefType()", caller);
		FamixMethod callee = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef.undefMethod()", null));
		assertNotNull("FamixModel must contain method <undef>.NotDef.undefMethod()", callee);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
		
		callee = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef2.undefMethod()", null));
		assertNotNull("FamixModel must contain method <undef>.NotDef2.undefMethod()", callee);
		nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 2, nrContainsInvocation);
		
		caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.callUndefType(<undef>.NotDef)", null));
		assertNotNull("FamixModel must contain method testPackage.UnresolvedCalls.callUndefType(<undef>.NotDef)", caller);
		callee = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef.undefParameter()", null));
		assertNotNull("FamixModel must contain method <undef>.NotDef.undefParameter()", callee);

		lRelations = fModel.getAssociations(caller);
		nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
	}
	
	@Test
	public void testCallUndefTypeAndAttribute() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.undefTypeAndAttribute()", null));
		FamixMethod callee = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef.undefMethod(int,java.lang.String,<undef>.NotDef2)", null));

		assertNotNull("Callee method <undef>.NotDef.undefMethod(int,java.lang.String,<undef>.NotDef2) must exist", callee);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
	}

	@Test
	public void testUndefAttributeAfterMethod() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.undefAttributeAfterMethod()", null));
		FamixMethod callee = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef.undefMethod(<undef>.NotDef2,<undef>.NotDef3)", null));

		assertNotNull("Callee method <undef>.NotDef.undefMethod(<undef>.NotDef2,<undef>.NotDef3) must exist", callee);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
	}

	@Test
	public void testCallOverwrittenVariable() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.callOverwrittenVariable()", null));
		FamixMethod callee = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef.undefMethod(int,java.lang.String)", null));

		assertNotNull("Callee method <undef>.NotDef.undefMethod(int,java.lang.String) must exist", callee);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
	}
	
	@Test
	public void testVariableScopeInnerClass() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls$inner.variableScope(int)", null));
		FamixMethod callee = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef.undefMethod(<undef>.NotDef2)", null));

		assertNotNull("Callee method <undef>.NotDef.undefMethod(<undef>.NotDef2) must exist", callee);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
	}

	@Test
	public void testUndefNestedCall() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.nestedCall()", null));
		FamixMethod callee1 = (FamixMethod) fModel.getElement(fFactory.createMethod("java.io.PrintStream.println(<undef>)", null));
		FamixMethod callee2 = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef.toString()", null));
		
		assertNotNull("Callee method java.io.PrintStream.println(<undef>) must exist", callee1);
		assertNotNull("Callee method <undef>.NotDef.toString() must exist", callee2);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(callee1, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee1.getUniqueName(), 1, nrContainsInvocation);

		nrContainsInvocation = TestHelper.containsRelationTo(callee2, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee2.getUniqueName(), 1, nrContainsInvocation);
	}
	
	@Test
	public void testStaticUndefParamCall() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.callStaticUndefParam()", null));
		FamixMethod callee = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.Dummy.staticFoo(<undef>.NotDef)", null));
		
		assertNotNull("Callee method testPackage.UnresolvedCalls.callStaticUndefParam() must exist", caller);
		assertNotNull("Callee method testPackage.Dummy.staticFoo(<undef>.NotDef) must exist", callee);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
	}
	
	@Test
	public void testUndefClassInstanceCreation() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.undefClassInstanceCreation()", null));
		FamixMethod calleeClassKnown  = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.Dummy.<init>()", null));
		FamixMethod calleeClassKnownParamUnkown = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.Dummy.<init>(<undef>.NotDef)", null));
		FamixMethod calleeClassUnknown = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef.<init>()", null));
		FamixMethod calleeClassUnknownParamUnkown = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef.<init>(<undef>.NotDef)", null));
		
		assertNotNull("Caller method testPackage.UnresolvedCalls.undefClassInstanceCreation() must exist", caller);
		assertNotNull("Callee method testPackage.Dummy.<init>() must exist", calleeClassKnown);
		assertNotNull("Callee method testPackage.Dummy.<init>(<undef>.NotDef) must exist", calleeClassKnownParamUnkown);
		assertNotNull("Callee method <undef>.NotDef.<init>() must exist", calleeClassUnknown);
		assertNotNull("Callee method <undef>.NotDef.<init>(<undef>.NotDef) must exist", calleeClassUnknownParamUnkown);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(calleeClassKnown, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + calleeClassKnown.getUniqueName(), 1, nrContainsInvocation);
		
		nrContainsInvocation = TestHelper.containsRelationTo(calleeClassKnownParamUnkown, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + calleeClassKnownParamUnkown.getUniqueName(), 1, nrContainsInvocation);
		
		nrContainsInvocation = TestHelper.containsRelationTo(calleeClassUnknown, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + calleeClassUnknown.getUniqueName(), 1, nrContainsInvocation);

		nrContainsInvocation = TestHelper.containsRelationTo(calleeClassUnknownParamUnkown, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + calleeClassUnknownParamUnkown.getUniqueName(), 1, nrContainsInvocation);
	}
	
	@Test
	public void testUndefClassInstanceCreationParents() {
		FamixClass classDummy = (FamixClass) fModel.getElement(fFactory.createClass("testPackage.Dummy", null));
		FamixClass classNotDef = (FamixClass) fModel.getElement(fFactory.createClass("<undef>.NotDef", null));
		
		FamixMethod calleeClassKnownParamUnkown = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.Dummy.<init>(<undef>.NotDef)", null));
		FamixMethod calleeClassUnknown = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef.<init>()", null));
		FamixMethod calleeClassUnknownParamUnkown = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.NotDef.<init>(<undef>.NotDef)", null));
		
		assertNotNull("FamixClass testPackage.Dummy must exist", classDummy);
		assertNotNull("FamixClass <undef>.NotDef must exist", classNotDef);
		assertNotNull("Callee method testPackage.Dummy.<init>(<undef>.NotDef) must exist", calleeClassKnownParamUnkown);
		assertNotNull("Callee method <undef>.NotDef.<init>() must exist", calleeClassUnknown);
		assertNotNull("Callee method <undef>.NotDef.<init>(<undef>.NotDef) must exist", calleeClassUnknownParamUnkown);
		
		assertTrue("FamixClass " + classDummy.getUniqueName() + " must contain method " + calleeClassKnownParamUnkown.getUniqueName(), classDummy.getMethods().contains(calleeClassKnownParamUnkown));
		
		// This containment is currently not considered
//		assertTrue("FamixClass " + classNotDef.getUniqueName() + " must contain method " + calleeClassUnknown.getUniqueName(), classDummy.getMethods().contains(calleeClassUnknown));
//		assertTrue("FamixClass " + classNotDef.getUniqueName() + " must contain method " + calleeClassUnknownParamUnkown.getUniqueName(), classDummy.getMethods().contains(calleeClassUnknownParamUnkown));
	}
	
	@Test
	public void testUndefAnonymClassInstanceCreation() {
		FamixClass unresolvedCalls = (FamixClass) fModel.getElement(fFactory.createClass("testPackage.UnresolvedCalls", null));
		FamixClass undefAnonymClass = (FamixClass) fModel.getElement(fFactory.createClass("testPackage.UnresolvedCalls$1F", null));
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.unresolvedAnonymClass()", null));
		FamixMethod oinitAnonym = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls$1F.<oinit>()", null));
		FamixMethod initAnonym = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls$1F.<init>(int,java.lang.String)", null));
		FamixMethod inUndefAnonym = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls$1F.inUndefAnonym(<undef>.String,<undef>.NotDef2)", null));
		FamixMethod inUndefAnonym2 = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls$1F.inUndefAnonym(<undef>.String)", null));
		FamixAttribute fieldInAnonym = (FamixAttribute) fModel.getElement(fFactory.createAttribute("testPackage.UnresolvedCalls$1F.fieldInAnonym", null));

		assertNotNull("FamixClass testPackage.UnresolvedCalls must exist", unresolvedCalls);
		assertNotNull("Anonym class testPackage.UnresolvedCalls$1 must exist", undefAnonymClass);
		assertNotNull("FamixMethod testPackage.UnresolvedCalls.unresolvedAnonymClass() must exist", caller);
		assertNotNull("Initializer testPackage.UnresolvedCalls$1F.<oinit>() must exist", oinitAnonym);
		assertNotNull("Constructor testPackage.UnresolvedCalls$1.<init>() must exist", initAnonym);
		assertNotNull("FamixMethod testPackage.UnresolvedCalls$1F.inUndefAnonym(<undef>.String,<undef>.NotDef2) must exist", inUndefAnonym);
		assertNotNull("FamixMethod testPackage.UnresolvedCalls$1F.inUndefAnonym(java.lang.String) must exist", inUndefAnonym2);
		assertNotNull("FamixAttribute testPackage.UnresolvedCalls$1F.fieldInAnonym must exist", fieldInAnonym);
		
		assertTrue("FamixMethod " + caller.getUniqueName() + " must contain anonym class " + undefAnonymClass.getUniqueName(), caller.getAnonymClasses().contains(undefAnonymClass));
		assertTrue("Anonym class " + undefAnonymClass.getUniqueName() + " must contain initializer " + oinitAnonym.getUniqueName(), undefAnonymClass.getMethods().contains(oinitAnonym));
		assertTrue("Anonym class " + undefAnonymClass.getUniqueName() + " must contain initializer " + initAnonym.getUniqueName(), undefAnonymClass.getMethods().contains(initAnonym));
		assertTrue("Anonym class " + undefAnonymClass.getUniqueName() + " must contain method " + inUndefAnonym.getUniqueName(), undefAnonymClass.getMethods().contains(inUndefAnonym));
		assertTrue("Anonym class " + undefAnonymClass.getUniqueName() + " must contain method " + inUndefAnonym2.getUniqueName(), undefAnonymClass.getMethods().contains(inUndefAnonym2));
		assertTrue("Anonym class " + undefAnonymClass.getUniqueName() + " must contain attribute " + fieldInAnonym.getUniqueName(), undefAnonymClass.getAttributes().contains(fieldInAnonym));
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(initAnonym, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + initAnonym.getUniqueName(), 1, nrContainsInvocation);
		
		Set<FamixAssociation> lAccessRelations = fModel.getAssociations(oinitAnonym);
		int nrContainsAccess = TestHelper.containsRelationTo(fieldInAnonym, lAccessRelations);
		assertEquals("FamixAccess relationships from " + oinitAnonym.getUniqueName() + " to " + fieldInAnonym.getUniqueName(), 1, nrContainsAccess);
	}
	
	@Test
	public void testUnresolvedThisCall() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.<init>()", null));
		FamixMethod thisCallee  = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.<init>(int,<undef>.NotDef2)", null));
		
		assertNotNull("Caller method testPackage.UnresolvedCalls.<init>() must exist", caller);
		assertNotNull("Callee method testPackage.UnresolvedCalls.<init>(<undef>.NotDef2) must exist", thisCallee);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(thisCallee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + thisCallee.getUniqueName(), 1, nrContainsInvocation);
	}
	
	@Test
	public void testUnresolvedCallSequence() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.unresolvedCallSequence()", null));
		FamixMethod seq1callee1  = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.Dummy.createDummy()", null));
//		FamixMethod seq1callee2  = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.Dummy.foo(<undef>,java.lang.String)", null));
		
		assertNotNull("Caller method testPackage.UnresolvedCalls.unresolvedCallSequence() must exist", caller);
		assertNotNull("Callee method testPackage.Dummy.createDummy() must exist", seq1callee1);
//		assertNotNull("Callee method testPackage.Dummy.foo(<undef>,java.lang.String) must exist", seq1callee2);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(seq1callee1, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + seq1callee1.getUniqueName(), 1, nrContainsInvocation);
//		nrContainsInvocation = TestHelper.containsRelationTo(seq1callee2, lRelations);
//		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + seq1callee2.getUniqueName(), 1, nrContainsInvocation);
	}
	
	@Test
	public void testFindCallInBaseClass() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.findCallInBaseClass()", null));
		FamixMethod callee  = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.Dummy.foo(<undef>.NotDef,int)", null));
		
		Assert.assertNotNull("Caller method testPackage.UnresolvedCalls.findCallInBaseClass() must exist", caller);
		Assert.assertNotNull("Callee method testPackage.Dummy.foo(<undef>.NotDef,int) must exist", callee);

		Assert.assertNotNull("There must be unresolved calls for method testPackage.UnresolvedCalls.undefCallWithinClass()", fUnresolvedInvocationHandler.getUnresolvedCalls().get(caller));
		
		List<UnresolvedMethodInvocation> unresolvedCalls = fUnresolvedInvocationHandler.getUnresolvedCalls().get(caller);
		Assert.assertTrue("FamixMethod " + caller.getUniqueName() + " must contain unresolved calls ", unresolvedCalls.size() > 0);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
	}

	@Test
	public void testSubtypingHandleMultipleMatches() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.handleMultipleMatches()", null));
		FamixMethod callee  = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.DummySub.foo(<undef>.NotDef)", null));
		
		Assert.assertNotNull("Caller method testPackage.UnresolvedCalls.handleMultipleMatches() must exist", caller);
		Assert.assertNotNull("Callee method testPackage.DummySub.foo(<undef>.NotDef) must exist", callee);

		Assert.assertNotNull("There must be unresolved calls for method testPackage.UnresolvedCalls.handleMultipleMatches()", fUnresolvedInvocationHandler.getUnresolvedCalls().get(caller));
		
		// determine the unresolved method call
		List<UnresolvedMethodInvocation> unresolvedCalls = fUnresolvedInvocationHandler.getUnresolvedCalls().get(caller);
		UnresolvedMethodInvocation selectedCall = null;
		for (UnresolvedMethodInvocation unresolved : unresolvedCalls) {
			if (unresolved.getStatement().equals("ds.foo(nd)")) {
				selectedCall = unresolved;
			}
		}
		Assert.assertEquals("Caller of statement ds.foo(nd) not equal ", caller, selectedCall.getCaller());
		Assert.assertNotNull("There must be an unresolved call for the statement ds.foo(nd)", selectedCall);
		Assert.assertEquals("Wrong number of matched callees for unresolved call ds.foo(nd)", 2, selectedCall.getMatchesByAllParametersType().size());
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
	}
	

    @Test
    public void testCallToConstructorWithUndefTemplateParameter() {
        FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.callWithTemplateParameter()",null));
        FamixMethod constructor = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedTypeParameters<T>.<init>()",null));
        
        assertNotNull("FamixModel must contain the method testPackage.UnresolvedCalls.callWithTemplateParameter()", caller);
        assertNotNull("FamixModel must contain the method testPackage.UnresolvedTypeParameters<T>.<init>()", constructor);

        Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
        int containsInvocationTo = TestHelper.containsRelationTo(constructor, lRelations);
        assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + constructor.getUniqueName(), 1, containsInvocationTo);
    }
    
    @Test
    public void testCallToMethodWithUndefTemplateParameter() {
        FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedCalls.callWithTemplateParameter()",null));
        FamixMethod callee = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedTypeParameters<T>.someMethod(testPackage.UnresolvedTypeParameters$T,<undef>.NotDef2)",null));
        FamixMethod alternateCaller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedTypeParameters<T>.someMethod(java.lang.String,<undef>.NotDef2)",null));
        
        assertNotNull("FamixModel must contain the method testPackage.UnresolvedCalls.callWithTemplateParameter()", caller);
        assertNotNull("FamixModel must contain the method testPackage.UnresolvedTypeParameters<T>.someMethod(testPackage.UnresolvedTypeParameters$T,<undef>.NotDef2)", callee);
        assertNotNull("FamixModel must contain the method testPackage.UnresolvedTypeParameters<T>.someMethod(java.lang.String,<undef>.NotDef2)", alternateCaller);

        Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
        int containsInvocationTo = TestHelper.containsRelationTo(alternateCaller, lRelations);
        assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + alternateCaller.getUniqueName(), 1, containsInvocationTo);
    }
}
