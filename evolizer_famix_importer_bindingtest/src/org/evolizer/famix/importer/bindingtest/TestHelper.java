/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.famix.importer.bindingtest;

import java.io.FileReader;
import java.util.Set;

import org.evolizer.famix.model.entities.FamixAssociation;
import org.evolizer.famix.model.entities.AbstractFamixEntity;


/**
 * @author pinzger
 *
 */
public class TestHelper {
	/**
	 * @param foo
	 * @param lRelations
	 */
	public static int containsRelationTo(AbstractFamixEntity to, Set<FamixAssociation> lRelations) {
		int count = 0;
		for (FamixAssociation association : lRelations) {
			if (association.getTo().equals(to)) {
				count++;
			}
		}
		return count;
	}
	public static boolean containsRelationTo(FamixAssociation pRel, Set<FamixAssociation> pRelations) {
		boolean containsRelation = false;
		for (FamixAssociation association : pRelations) {
			if (association.getClass() == pRel.getClass() && association.getTo().equals(pRel.getTo())) {
				containsRelation = true;
			}
		}
		return containsRelation;
	}
	
	public static String readFile(String fileName) throws Exception {
		StringBuffer result = new StringBuffer();
		char b[] = new char[2048];
		FileReader fis = new FileReader(fileName);
		int n;
		while ((n = fis.read(b)) > 0) {
			result.append(b, 0, n);
		}
		fis.close();
		return result.toString();
	}
}
