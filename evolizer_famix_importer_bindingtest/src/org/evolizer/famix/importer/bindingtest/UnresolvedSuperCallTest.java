/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.famix.importer.bindingtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.JavaRuntime;
import org.evolizer.famix.importer.FamixImporterPlugin;
import org.evolizer.famix.importer.FamixModelFactory;
import org.evolizer.famix.importer.ProjectParser;
import org.evolizer.famix.importer.unresolved.UnresolvedInvocationHandler;
import org.evolizer.famix.model.entities.FamixAssociation;
import org.evolizer.famix.model.entities.FamixMethod;
import org.evolizer.famix.model.entities.FamixModel;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author pinzger
 *
 */
public class UnresolvedSuperCallTest {
	private static FamixModel fModel;
	private static UnresolvedInvocationHandler fUnresolvedInvocationHandler;
	private static FamixModelFactory fFactory = new FamixModelFactory();

	private static void setUpProject() throws CoreException, IOException{
		String name = "TestProject";
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root= workspace.getRoot();
		root.delete(true, false, null);
		IProject project= root.getProject(name);
		project.create(null);
		project.open(null);
		
		IProjectDescription desc = project.getDescription();
		desc.setNatureIds(new String[] {
				JavaCore.NATURE_ID});
		project.setDescription(desc, null);
		
		IJavaProject javaProj = JavaCore.create(project);
		IFolder binDir = project.getFolder("bin");
		IPath binPath = binDir.getFullPath();
		javaProj.setOutputLocation(binPath, null);
		
		IClasspathEntry cpe = JavaRuntime.getDefaultJREContainerEntry();
		javaProj.setRawClasspath(new IClasspathEntry[] {cpe},null);
		
		IFolder folder = project.getFolder("src");
		folder.create(true, true, null);
		IClasspathEntry entry = JavaCore.newSourceEntry(folder.getFullPath());
		IClasspathEntry[] entries = javaProj.getRawClasspath();
		IClasspathEntry[] newEntries = new IClasspathEntry[entries.length + 1];
		System.arraycopy(entries, 0, newEntries, 0, entries.length);
		newEntries[newEntries.length - 1] = entry;
		javaProj.setRawClasspath(newEntries, null);
		
		IPackageFragmentRoot javaRoot = javaProj.getPackageFragmentRoot(folder);
		javaRoot.createPackageFragment("testPackage", true, null);
		javaRoot = javaProj.getPackageFragmentRoot(folder);
		IPackageFragment packageFragment = javaRoot.getPackageFragment("testPackage");
		IFolder packageFolder = project.getFolder(packageFragment.getResource().getProjectRelativePath());
		IFile sourceFile = packageFolder.getFile("UnresolvedSuperCallsBase.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/UnresolvedSuperCallsBase.java"), IResource.NONE, null);

		sourceFile = packageFolder.getFile("UnresolvedSuperCalls.java");
		sourceFile.create(FamixImporterPlugin.openBundledFile("./testsource/testPackage/UnresolvedSuperCalls.java"), IResource.NONE, null);
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		setUpProject();
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IJavaModel model =  JavaCore.create(workspace.getRoot());
		IJavaProject project = model.getJavaProject("TestProject");
		List<IJavaElement> selection = new ArrayList<IJavaElement>();
		selection.add(project);
		ProjectParser parser = new ProjectParser(selection);
		parser.parse(null);
		fModel = parser.getModel();
		
		fUnresolvedInvocationHandler = new UnresolvedInvocationHandler(fModel, parser.getUnresolvedCalls());
		fUnresolvedInvocationHandler.process(new NullProgressMonitor());
		fUnresolvedInvocationHandler.addInvocations(new NullProgressMonitor());
	}
	
	@Test
	public void testSuperCall() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedSuperCalls.superCall()", null));
		assertNotNull("FamixModel must contain the method testPackage.UnresolvedSuperCalls.superCall()", caller);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		assertTrue("FamixMethod " + caller.getUniqueName() + " must contain relationships", lRelations.size() > 0);

		FamixMethod callee = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedSuperCallsBase.x()", null));
		assertNotNull("FamixModel must contain the method testPackage.UnresolvedSuperCallsBase.x()", callee);

		int nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
	}
	
	@Test
	public void testCallUndefParameter() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedSuperCalls.superCallUndefParameter()", null));
		FamixMethod callee = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.x(<undef>.NotDef)", null));

		assertNotNull("FamixModel must contain the method testPackage.UnresolvedSuperCalls.superCallUndefParameter()", caller);
		assertNotNull("FamixModel must contain the method <undef>.x(<undef>.NotDef)", callee);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
	}
	
	@Test 
	public void testSuperConstructorCall() {
		FamixMethod caller = (FamixMethod) fModel.getElement(fFactory.createMethod("testPackage.UnresolvedSuperCalls.<init>()", null));
		FamixMethod callee = (FamixMethod) fModel.getElement(fFactory.createMethod("<undef>.<init>(<undef>.NotDef2)", null));

		assertNotNull("FamixModel must contain the method testPackage.UnresolvedSuperCalls.<init>()", caller);
		assertNotNull("FamixModel must contain the method <undef>.<init>(<undef>.NotDef2)", callee);
		
		Set<FamixAssociation> lRelations = fModel.getAssociations(caller);
		int nrContainsInvocation = TestHelper.containsRelationTo(callee, lRelations);
		assertEquals("FamixInvocation relationships from " + caller.getUniqueName() + " to " + callee.getUniqueName(), 1, nrContainsInvocation);
	}
	
}
