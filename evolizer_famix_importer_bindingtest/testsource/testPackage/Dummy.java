package testPackage;

import java.util.HashMap;

public class Dummy {

	public Dummy() {
	}
	
	public Dummy(String str) {
	}
	
	public Dummy(NotDef nd) {
	}
	
	public void foo(String str) {
	}
	
	public void foo(NotDef nd) {
	}
	
	public void foo(NotDef nd, int i) {
		
	}
	
	public static void staticFoo(NotDef nd) {
	}
	
	public Dummy createDummy() {
		Class base = Class.forName("java.util.Map");
		Class sub = Class.forName("java.util.HashMap");
		
		if (sub.newInstance() instanceof sub.getClass()) {
			
		}
		
		return new Dummy();
	}
}
