package testPackage;

public class UnresolvedSuperCalls extends UnresolvedSuperCallsBase {

	public UnresolvedSuperCalls() {
		super(nd2);
	}
	
	public void superCall() {
		super.x();
	}
	
	public void superCallUndefParameter() {
		NotDef notDef = new NotDef();
		super.x(notDef);
	}
	
	NotDef2 nd2 = new NotDef2();
}
