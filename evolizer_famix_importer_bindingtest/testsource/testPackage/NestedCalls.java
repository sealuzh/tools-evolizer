package testPackage;

public class NestedCalls {
	public NestedCalls() {
	}
	
	public int add10(int sum) {
		return sum + 10;
	}
	
	public int sum() {
		return add10(add10(add10(0 + add10(0))));
	}

}
