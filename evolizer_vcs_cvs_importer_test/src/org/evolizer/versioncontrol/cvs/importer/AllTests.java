package org.evolizer.versioncontrol.cvs.importer;

import org.evolizer.versioncontrol.cvs.importer.mapper.VersioningModelBuilderTest;
import org.evolizer.versioncontrol.cvs.importer.parser.CVSParserTest;
import org.evolizer.versioncontrol.cvs.importer.transactions.TransactionReconstructorTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	VersioningModelBuilderTest.class,
	CVSParserTest.class,
	TransactionReconstructorTest.class
})
public class AllTests {

}
