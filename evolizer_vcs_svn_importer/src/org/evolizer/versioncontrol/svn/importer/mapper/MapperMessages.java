/*
 * Copyright 2010 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.mapper;

import org.eclipse.osgi.util.NLS;

/**
 * Standard Eclipse String externalization class. So that the SVNModelMapper does not have hardcoded strings.
 * 
 * @author ghezzi
 * 
 */
public final class MapperMessages extends NLS {

    private static final String BUNDLE_NAME = "org.evolizer.versioncontrol.svn.importer.mapper.messages";
    /*
     * SVN miscellaneous messages
     */
    public static String SVNModelMapper_DELETED;
    public static String SVNModelMapper_MOVED;
    public static String SVNModelMapper_emailRegEx;
    public static String SVNModelMapper_noAuthor;
    public static String SVNModelMapper_emptyMessage;
    /*
     * Errors and Warnings
     */
    public static String SVNModelMapper_copiedPathNotExists;
    public static String SVNModelMapper_misuse;
    public static String SVNModelMapper_neitherDirectoryNorFile;
    public static String SVNModelMapper_fileNotExists;
    public static String SVNModelMapper_isSubBranch;
    public static String SVNModelMapper_cannotCreateRevision;
    public static String SVNModelMapper_addFileMisuse;
    public static String SVNModelMapper_revisionDoesNotBelong;
    /*
     * Entity Creation/Manipulation Messages
     */
    public static String SVNModelMapper_savedModel;
    public static String SVNModelMapper_savedRelease;
    public static String SVNModelMapper_savedTransaction;
    public static String SVNModelMapper_addedRevisionToRelease;
    public static String SVNModelMapper_addedRevisionToBranch;
    public static String SVNModelMapper_addedFileToBranch;
    public static String SVNModelMapper_addedFileToRelease;
    public static String SVNModelMapper_deletedRevision;
    public static String SVNModelMapper_deletedRelease;
    public static String SVNModelMapper_createdRelease;
    public static String SVNModelMapper_removedArtifacts;
    public static String SVNModelMapper_createdBranch;
    public static String SVNModelMapper_deletedBranch;
    public static String SVNModelMapper_createdRevision;
    public static String SVNModelMapper_createdDirectory;
    public static String SVNModelMapper_createdFile;
    public static String SVNModelMapper_createdTransaction;
    public static String SVNModelMapper_createdPerson;
    public static String SVNModelMapper_removedRevisionFromBranch;
    public static String SVNModelMapper_removedRevisionFromRelease;
    public static String SVNModelMapper_createdBranchRevision;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, MapperMessages.class);
    }

    private MapperMessages() {}
}
