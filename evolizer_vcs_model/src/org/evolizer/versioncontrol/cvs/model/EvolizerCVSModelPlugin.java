/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.model;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle.
 * 
 * @author wuersch
 */
public class EvolizerCVSModelPlugin extends Plugin {

    /** The Constant PLUGIN_ID. */
    public static final String PLUGIN_ID = "org.evolizer.model.versioning";

    private static EvolizerCVSModelPlugin plugin;

    /**
     * The constructor.
     */
    public EvolizerCVSModelPlugin() {
        plugin = this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance.
     * 
     * @return the shared instance
     */
    public static EvolizerCVSModelPlugin getDefault() {
        return plugin;
    }
    
    /**
     * Opens a file located within the plug-in-bundle or one of its registered fragments.
     * 
     * @param relativeFSPath
     *            relative path of the file starting at the root of this plug-in
     * @return an InputStream reading the specified file
     * @throws IOException
     *             if file could not be opened
     */
	public static InputStream openFile(String relativeFSPath) throws IOException {
		return FileLocator.openStream(EvolizerCVSModelPlugin.getDefault().getBundle(), new Path(relativeFSPath), false);
	}
}
