/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.model.entities;

import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.evolizer.core.hibernate.model.api.IEvolizerModelEntity;
import org.evolizer.model.resources.entities.fs.File;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.model.resources.entities.misc.Content;

/**
 * A file can have several versions, called Revisions.
 * 
 * @author wuersch, jetter
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Revision implements IEvolizerModelEntity {

    /**
     * Unique ID, used by Hibernate.
     */
    private Long id;

    /**
     * Each version of a file has a unique revision number. Revision numbers look like `1.1', `1.2', `1.3.2.2' or even
     * `1.3.2.2.4.5'. A revision number always has an even number of period-separated decimal integers. By default
     * revision 1.1 is the first revision of a file. Each successive revision is given a new number by increasing the
     * rightmost number by one.
     */
    private String number;

    /**
     * The file which has one or many revisions.
     */
    private File file;

    /**
     * The revision before the modification was made.
     */
    private Revision previousRevision;

    /**
     * The revision after the modification was made.
     */
    private Revision nextRevision;

    /**
     * Wrapper for source code that belongs to the revision.
     */
    private Content content = new Content();

    /**
     * The modification report which led to the current revision.
     */
    private ModificationReport report;

    /**
     * A <Set> of all known releases which contain the source unit with the current revision.
     */
    private Set<Release> releases = new HashSet<Release>();

    /**
     * The state of the Revision. E.g. dead=removed, Exp=experimental
     */
    private String state;

    /**
     * A default constructor is needed for Hibernate to load instances; otherwise an exception is thrown.package
     * visibility because of lazy loading.
     * Shouldn't be used to create new instances of Revision, use Revision(String number) instead.
     */
    public Revision() {
        super();
    }

    /**
     * Instantiates a new revision.
     * 
     * @param number
     *            the number
     */
    public Revision(String number) {
        this();
        this.number = number;
    }

    /**
     * Returns the id.
     * 
     * @return the id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @SuppressWarnings(value = {"unused"})
    private void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns the number.
     * 
     * @return the number.
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the number.
     * 
     * @param number
     *            The number to set.
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * Returns the releases.
     * 
     * @return the releases.
     */
    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "revision_fk")
    public Set<Release> getReleases() {
        return releases;
    }

    /**
     * Sets the releases.
     * 
     * @param releases
     *            The releases to set.
     */
    public void setReleases(Set<Release> releases) {
        this.releases = releases;
    }

    /**
     * Adds the release.
     * 
     * @param release
     *            the release
     */
    public void addRelease(Release release) {
        releases.add(release);
    }

    /**
     * Returns the report.
     * 
     * @return the report.
     */
    @OneToOne(cascade = CascadeType.ALL)
    public ModificationReport getReport() {
        return report;
    }

    /**
     * Sets the report.
     * 
     * @param report
     *            The report to set.
     */
    public void setReport(ModificationReport report) {
        this.report = report;
    }

    /**
     * Returns the file.
     * 
     * @return the file.
     */
    @ManyToOne
    public File getFile() {
        return file;
    }

    /**
     * Sets the file.
     * 
     * @param file
     *            the file
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Returns the state.
     * 
     * @return the state.
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the state.
     * 
     * @param state
     *            The state to set.
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Returns the next revision.
     * 
     * @return the newRevision.
     */
    @OneToOne
    @org.hibernate.annotations.LazyToOne(org.hibernate.annotations.LazyToOneOption.PROXY)
    // Needed for one-to-one lazy fetching with proxies.
    public Revision getNextRevision() {
        return nextRevision;
    }

    /**
     * Sets the next revision.
     * 
     * @param newRevision
     *            The newRevision to set.
     */
    public void setNextRevision(Revision newRevision) {
        nextRevision = newRevision;
    }

    /**
     * Returns the previous revision.
     * 
     * @return the oldRevision.
     */
    @OneToOne
    @org.hibernate.annotations.LazyToOne(org.hibernate.annotations.LazyToOneOption.PROXY)
    // Needed for one-to-one lazy fetching with proxies.
    public Revision getPreviousRevision() {
        return previousRevision;
    }

    /**
     * Sets the previous revision.
     * 
     * @param oldRevision
     *            The oldRevision to set.
     */
    @OneToOne
    public void setPreviousRevision(Revision oldRevision) {
        previousRevision = oldRevision;
    }

    /**
     * Append to commit message.
     * 
     * @param messageLine
     *            the message line
     */
    public void appendToCommitMessage(String messageLine) {
        report.appendToCommitMessage(messageLine);
    }

    /**
     * Returns the date and time when the current revision was created, i.e. when the corresponding modification report
     * was made.
     * 
     * @return the creation time of this revision.
     */
    @Transient
    public Date getCreationTime() {
        return report.getCreationTime();
    }

    /**
     * Sets the creation time.
     * 
     * @param time
     *            the new creation time
     * @throws ParseException
     *             the parse exception
     */
    public void setCreationTime(String time) throws ParseException {
        if (report == null) {
            report = new ModificationReport(time);
        } else {
            report.setCreationTime(time);
        }
    }

    /**
     * Sets the creation time.
     * 
     * @param time
     *            the new creation time
     */
    public void setCreationTime(Date time) {
        if (report == null) {
            report = new ModificationReport(time);
        } else {
            report.setCreationTime(time);
        }
    }

    private void ensureReportExists() {
        if (report == null) {
            report = new ModificationReport();
        }
    }

    /**
     * Returns the author.
     * 
     * @return the author
     */
    @Transient
    public Person getAuthor() {
        if (report != null) {
            return report.getAuthor();
        } else {
            return null;
        }
    }

    /**
     * Sets the author.
     * 
     * @param person
     *            the new author
     */
    public void setAuthor(Person person) {
        ensureReportExists();
        report.setAuthor(person);
    }

    /**
     * Returns the author nick name.
     * 
     * @return the author nick name
     */
    @Transient
    public String getAuthorNickName() {
        if (report != null) {
            return report.getAuthorNickName();
        } else {
            return null;
        }
    }

    /**
     * Sets the author nick name.
     * 
     * @param authorNickName
     *            the new author nick name
     */
    public void setAuthorNickName(String authorNickName) {
        ensureReportExists();
        report.setAuthorNickName(authorNickName);
    }

    /**
     * Returns the lines add.
     * 
     * @return the lines add
     */
    @Transient
    public int getLinesAdd() {
        if (report != null) {
            return report.getLinesAdd();
        } else {
            return 0;
        }
    }

    /**
     * Sets the lines add.
     * 
     * @param linesAdd
     *            the new lines add
     */
    public void setLinesAdd(int linesAdd) {
        ensureReportExists();
        report.setLinesAdd(linesAdd);
    }

    /**
     * Returns the lines del.
     * 
     * @return the lines del
     */
    @Transient
    public int getLinesDel() {
        if (report != null) {
            return report.getLinesDel();
        } else {
            return 0;
        }
    }

    /**
     * Sets the lines del.
     * 
     * @param linesDel
     *            the new lines del
     */
    public void setLinesDel(int linesDel) {
        ensureReportExists();
        report.setLinesDel(linesDel);
    }

    /**
     * Returns the commit message.
     * 
     * @return the commit message
     */
    @Transient
    public String getCommitMessage() {
        if (report != null) {
            return report.getCommitMessage();
        } else {
            return "";
        }
    }

    /**
     * Sets the commit message.
     * 
     * @param message
     *            the new commit message
     */
    public void setCommitMessage(String message) {
        ensureReportExists();
        report.setCommitMessage(message);
    }

    // TODO Add tests for getSource()
    /**
     * Returns the source.
     * 
     * @return the source
     */
    @Transient
    public String getSource() {
        return content.getSource();
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    public void setSource(String source) {
        content.setSource(source);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((file == null) ? 0 : file.hashCode());
        result = prime * result + ((number == null) ? 0 : number.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Revision other = (Revision) obj;
        if (file == null) {
            if (other.file != null) {
                return false;
            }
        } else if (!file.equals(other.file)) {
            return false;
        }
        if (number == null) {
            if (other.number != null) {
                return false;
            }
        } else if (!number.equals(other.number)) {
            return false;
        }
        return true;
    }

    /**
     * Optimization: We need to wrap the source code string in order to enable lazy fetching. Lazy fetching is only
     * possible on associations (and collections), not on basic types.
     * 
     * @return the sourceCodeWrapper
     */
    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @org.hibernate.annotations.LazyToOne(org.hibernate.annotations.LazyToOneOption.PROXY)
    // Needed for one-to-one lazy fetching with proxies.
    public Content getSourceCodeWrapper() {
        return content;
    }

    /**
     * Sets the source code wrapper.
     * 
     * @param sourceCodeWrapper
     *            the sourceCodeWrapper to set
     */
    public void setSourceCodeWrapper(Content sourceCodeWrapper) {
        content = sourceCodeWrapper;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getLabel() {
        return file.getName() + " : " + number;
    }

    /**
     * {@inheritDoc}
     */
    @Transient
    public String getURI() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Adds the nickname to author.
     * 
     * @param name
     *            the name
     */
    public void addNicknameToAuthor(String name) {
        ensureReportExists();
        report.addNickNameToAuthor(name);
    }
}
