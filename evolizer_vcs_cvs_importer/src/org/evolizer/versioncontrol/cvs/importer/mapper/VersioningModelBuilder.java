/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.mapper;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.evolizer.core.exceptions.EvolizerRuntimeException;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.model.resources.entities.fs.Directory;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.versioncontrol.cvs.importer.EvolizerCVSPlugin;
import org.evolizer.versioncontrol.cvs.importer.mapper.api.ILogEntryListener;
import org.evolizer.versioncontrol.cvs.importer.parser.helper_entities.LogEntry;
import org.evolizer.versioncontrol.cvs.importer.parser.helper_entities.RevisionEntry;
import org.evolizer.versioncontrol.cvs.importer.parser.helper_entities.SymbolicName;
import org.evolizer.versioncontrol.cvs.model.entities.Branch;
import org.evolizer.versioncontrol.cvs.model.entities.CommitterRole;
import org.evolizer.versioncontrol.cvs.model.entities.Module;
import org.evolizer.versioncontrol.cvs.model.entities.Release;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.VersionedFile;

/**
 * Extracts the information of a {@link LogEntry}, builds a versioning model and maps it to a database.
 * 
 * @author wuersch, jetter
 */
public class VersioningModelBuilder implements ILogEntryListener {

    private static Logger sLogger =
            EvolizerCVSPlugin.getLogManager().getLogger(VersioningModelBuilder.class.getCanonicalName());

    /**
     * Helper class for the builder. Structs 4tw.
     * 
     * @author wuersch
     */
    private class P {

        private Person fPerson;
        private CommitterRole fRole;
    }

    /**
     * Hibernate Session whic provides connection to the database
     */
    private IEvolizerSession fSession;

    /**
     * Contains all releases apeared so far over all LogEntries.
     */
    private Map<String, Release> fAllocatedReleases = new HashMap<String, Release>();

    /**
     * Contains all branches found so far over all LogEntries.
     * 
     * Key: Release-tag, Value: {@link Release}-object. e.g.: Key: R3_1_maintenance, Value: {@link Release} -object.
     * 
     */
    private Map<String, Branch> fAllocatedBranches = new HashMap<String, Branch>();

    /**
     * Contains all branches found in the current LogEntry.
     * 
     * Key: BranchTag, Value: {@link Branch}-object. e.g.: Key: R3_1_maintenance, Value: {@link Branch} -object.
     * 
     */
    private Map<String, Branch> fTmpBranches = new HashMap<String, Branch>();

    /**
     * Contains all (sub-)directories where the files created from the current log entry are located.
     */
    private Map<String, Directory> fAllocatedDirectories = new HashMap<String, Directory>();

    private Map<String, P> fPersonNickNames = new HashMap<String, P>();

    private Map<String, P> fPersonEmails = new HashMap<String, P>();

    /**
     * Working copy of the current {@link LogEntry}:
     */
    private LogEntry fCurrentLogEntry;

    /**
     * Working copy of the current {@link VersionedFile}
     */
    private VersionedFile fCurrentFile;

    /**
     * Working copy of the current {@link Revision}s:
     */
    private List<Revision> fCurrentRevisions;

    private IProgressMonitor fMonitor;

    private String fRemoteModulePath;

    private Module fParent;

    /**
     * Constructor.
     * 
     * @param session
     *            the session
     * @param monitor
     *            the monitor
     * @param remoteModulePath
     *            the remote module path
     */
    public VersioningModelBuilder(IEvolizerSession session, IProgressMonitor monitor, String remoteModulePath) {
        fSession = session;
        fRemoteModulePath = remoteModulePath;

        // if no ProgressMonitor is passed, we use the NullProgressMonitor
        fMonitor = (monitor == null) ? new NullProgressMonitor() : monitor;

        fParent = new Module(remoteModulePath);
        fAllocatedDirectories.put("/", Directory.ROOT);
    }

    /**
     * Process log entry.
     * 
     * <p>
     * Open a transaction before processLogEntry is invoked for the first time and commit the transaction after it is
     * invoked for the last time (i.e. mapping is completed).
     * 
     * @param currentLogEntry
     *            the current log entry
     */
    public void processLogEntry(LogEntry currentLogEntry) {
        fCurrentLogEntry = currentLogEntry;
        fCurrentLogEntry.setWorkingFile(cutRCSFilePath(fCurrentLogEntry.getRcsFile()));
        sLogger.debug("Processing Versioning Information for " + fCurrentLogEntry.getWorkingFile());

        fMonitor.setTaskName("Building model for: " + fCurrentLogEntry.getWorkingFile() + ".");

        createDirectories();
        createFile();

        createRevisions();
        connectRevisionsAndFile();

        updateAllocatedReleasesAndBranches();
        addRevisionsToBranches();
        connectReleasesAndRevisions();

        makeModelPersistent();

        fTmpBranches.clear();
    }

    private void makeModelPersistent() {
        try {
            fSession.startTransaction();

            fSession.saveObject(fCurrentFile);

            for (Directory dir : fAllocatedDirectories.values()) {
                try {
                    fSession.saveObject(dir);
                } catch (EvolizerRuntimeException e) {
                    e.printStackTrace();
                }
            }

            for (Revision rev : fCurrentRevisions) {
                fSession.saveObject(rev);

            }

            for (Branch bra : fAllocatedBranches.values()) {
                fSession.saveObject(bra);

            }

            for (Release rel : fAllocatedReleases.values()) {
                fSession.saveObject(rel);

            }

            fSession.saveOrUpdate(fParent);
        } catch (EvolizerRuntimeException e) {
            e.printStackTrace();
        } finally {
            fSession.endTransaction();
        }
    }

    /**
     * Uses the current log entry to build the directory hierarchy where the current file is located in.
     */
    private void createDirectories() {
        // fullpath has to be a valid unix path.
        String fullPath = fCurrentLogEntry.getWorkingFile();
        dir(fullPath, Directory.ROOT);
    }

    /**
     * Helper method for {@link VersioningModelBuilder#createDirectories()}.
     * 
     * @param remainingPath
     * @param parent
     */
    private void dir(String remainingPath, Directory parent) {
        int endOfSegment = remainingPath.indexOf('/');
        if (endOfSegment == -1) {
            return;
        }

        String currentSegment = remainingPath.substring(0, endOfSegment);
        remainingPath = remainingPath.substring(endOfSegment + 1, remainingPath.length());

        String parentPath = parent.getPath();
        String currentPath;
        if (parentPath.equals("/")) {
            currentPath = parentPath + currentSegment;
        } else {
            currentPath = parentPath + "/" + currentSegment;
        }

        Directory dir;
        if (!fAllocatedDirectories.containsKey(currentPath)) {
            dir = new Directory(currentPath, parent);
            fAllocatedDirectories.put(currentPath, dir);
            fParent.add(dir);
        } else {
            dir = fAllocatedDirectories.get(currentPath);
        }

        dir(remainingPath, dir);
    }

    /**
     * Connects revisions with the branche(s) they belong too.
     */
    private void addRevisionsToBranches() {
        for (Revision revision : fCurrentRevisions) {
            String revisionNumber = revision.getNumber();

            String branchRevision = revisionNumber.substring(0, revisionNumber.lastIndexOf('.'));
            Branch branch = fTmpBranches.get(branchRevision);
            if (branch != null) {
                branch.addRevision(revision);
            }
        }
    }

    /**
     * Adds the revisions to their file and the file to his revisions.
     */
    private void connectRevisionsAndFile() {
        for (Revision rev : fCurrentRevisions) {
            rev.setFile(fCurrentFile);
            fCurrentFile.addRevision(rev);
        }
    }

    /**
     * Connects releases to revisions and vice-versa.
     */
    private void connectReleasesAndRevisions() {
        HashMap<String, Revision> hashedRevisions = buildHashedRevisions(fCurrentRevisions);

        List<SymbolicName> sns = fCurrentLogEntry.getSymbolicNames();
        for (SymbolicName sn : sns) {
            String relName = sn.getReleaseOrBranchTag();
            String revNumber = sn.getRevisionOrBranchNumber();

            if (fAllocatedReleases.containsKey(relName) && hashedRevisions.containsKey(revNumber)) {
                Release release = fAllocatedReleases.get(relName);
                Revision revision = hashedRevisions.get(revNumber);
                release.addRevision(revision);
                revision.addRelease(release);
            }
        }
    }

    /**
     * Builds a <code>Hashtable</code> of revisions from a <code>List</code> of revisions. Simplifies access to
     * revisions via revision numbers as keys.
     * 
     * @param revisions
     */
    private HashMap<String, Revision> buildHashedRevisions(List<Revision> revisions) {
        HashMap<String, Revision> hashedRevisions = new HashMap<String, Revision>();
        for (Revision revision : revisions) {
            hashedRevisions.put(revision.getNumber(), revision);
        }
        return hashedRevisions;
    }

    /**
     * Iterates through the Symbolic Names of the currentLogEntry and checks if the tag has been already found before.
     */
    private void updateAllocatedReleasesAndBranches() {
        List<SymbolicName> sns = fCurrentLogEntry.getSymbolicNames();
        for (SymbolicName sn : sns) {
            String releaseOrBranchTag = sn.getReleaseOrBranchTag();
            String revisionOrBranchNumber = sn.getRevisionOrBranchNumber();
            // branch
            if (isBranchTag(revisionOrBranchNumber)) {
                String branchNumber = convertMagicBranch(revisionOrBranchNumber);
                Branch branch;
                // new Branch
                if (!fAllocatedBranches.containsKey(releaseOrBranchTag)) {
                    branch = new Branch(releaseOrBranchTag);
                    fAllocatedBranches.put(releaseOrBranchTag, branch);
                    // branch was already allocated
                } else {
                    branch = fAllocatedBranches.get(releaseOrBranchTag);
                }
                fTmpBranches.put(branchNumber, branch); // save the relation between revisions and the branch.
                // release
            } else {
                // new Release
                if (!fAllocatedReleases.containsKey(releaseOrBranchTag)) {
                    Release release = new Release(releaseOrBranchTag);
                    fAllocatedReleases.put(releaseOrBranchTag, release);
                }
            }
        }
    }

    /**
     * branch tags have odd number of segments or have an even number with a zero as the second last segment e.g: 1.1.1,
     * 1.26.0.2 are branch revision numbers
     * 
     * @param tagNumber
     *            tag that specifies
     */
    private boolean isBranchTag(String tagNumber) {
        // First check if we have an odd number of segments (i.e. even number of dots)
        int numberOfDots = 0;
        int lastDot = 0;
        for (int i = 0; i < tagNumber.length(); i++) {
            if (tagNumber.charAt(i) == '.') {
                numberOfDots++;
                lastDot = i;
            }
        }
        if ((numberOfDots % 2) == 0) {
            return true;
        }
        if (numberOfDots == 1) {
            return false;
        }

        // If not, check if the second lat segment is a zero
        if ((tagNumber.charAt(lastDot - 1) == '0') && (tagNumber.charAt(lastDot - 2) == '.')) {
            return true;
        }

        return false;
    }

    /**
     * Removes the zero from a magic branch tag.
     * 
     * @param magicBranchNumber
     * @return Branch number without 0
     */
    private String convertMagicBranch(String magicBranchNumber) {
        int pos = magicBranchNumber.lastIndexOf(".0.");
        if (pos != -1) {
            return magicBranchNumber.substring(0, pos).concat(
                    magicBranchNumber.substring(pos + 2, magicBranchNumber.length()));
        } else {
            return magicBranchNumber;
        }
    }

    /**
     * Factory-method to create a <code>File</code> from a <code>LogEntry</code>.
     */
    private void createFile() {
        String fullPath = "/" + fCurrentLogEntry.getWorkingFile();
        String parentDirPath = fullPath.substring(0, fullPath.lastIndexOf("/"));
        Directory parent = fAllocatedDirectories.get(parentDirPath);
        VersionedFile currentFile = new VersionedFile(fullPath, parent);

        fParent.add(currentFile);
        fCurrentFile = currentFile;
    }

    /**
     * Creates revisions out of the current log entrys.
     */
    private void createRevisions() {
        List<Revision> revisions = new Vector<Revision>();

        List<RevisionEntry> entries = fCurrentLogEntry.getRevisionEntriesOrderedAsc();
        Revision lastRevision = null;
        for (RevisionEntry entry : entries) {
            Revision currentRevision = createRevision(entry);
            revisions.add(currentRevision);

            if (lastRevision != null) {
                String currentRevNumber = currentRevision.getNumber();
                String currentParentNumber = currentRevNumber.substring(0, currentRevNumber.lastIndexOf('.'));
                String lastRevNumber = lastRevision.getNumber();
                String lastParentNumber = lastRevNumber.substring(0, lastRevNumber.lastIndexOf('.'));

                if (currentParentNumber.equals(lastParentNumber)) {
                    currentRevision.setPreviousRevision(lastRevision);
                    lastRevision.setNextRevision(currentRevision);
                }
            }

            lastRevision = currentRevision;

        }
        fCurrentRevisions = revisions;
    }

    /**
     * Factory-method to convert a <code>RevisionEntry</code> into a <code>Revision</code>.
     * 
     * @param entry
     *            a <code>RevisionEntry</code> object.
     * @return the converted <code>Revision</code>-object.
     */
    private Revision createRevision(RevisionEntry entry) {
        String revNumber = entry.getRevisionNumber();
        Date creationDate = entry.getCreationDate();
        String author = entry.getAuthor();
        String state = entry.getState();
        int linesAdd = entry.getLinesAdd();
        int linesDel = entry.getLinesDel();
        String commitMessage = entry.getCommitMessage();

        Revision revision = new Revision(revNumber);
        revision.setState(state);
        revision.setCreationTime(creationDate);
        revision.setLinesAdd(linesAdd);
        revision.setLinesDel(linesDel);
        revision.setCommitMessage(commitMessage);

        createPerson(author, revision);
        return revision;
    }

    private void createPerson(String author, Revision revision) {
        P e = null;
        if (isEmail(author)) {

            if (fPersonEmails.containsKey(author)) {
                e = fPersonEmails.get(author);

            } else {
                Person person = new Person();
                person.setEmail(author);

                e = new P();
                e.fPerson = person;
                e.fRole = new CommitterRole();

                fPersonEmails.put(author, e);
            }

            e.fRole.addRevision(revision);
            revision.setAuthor(e.fPerson);
            revision.setAuthorNickName(author); // MW: facilitates e.g. bug linking
        } else { // We guess that author is a nickname

            if (fPersonNickNames.containsKey(author)) {
                e = fPersonNickNames.get(author);
            } else {
                Person person = new Person();
                person.addNickName(author);

                e = new P();
                e.fPerson = person;
                e.fRole = new CommitterRole();
                e.fPerson.addRole(e.fRole);
                
                fPersonNickNames.put(author, e);
            }

            e.fRole.addRevision(revision);
            revision.setAuthor(e.fPerson);
            revision.setAuthorNickName(author);
        }
    }

    private static boolean isEmail(String input) {
        Pattern p = Pattern.compile("(\\w+)@(\\w+\\.)(\\w+)(\\.\\w+)?"); // name@subdomain.domain.suffix
        Matcher m = p.matcher(input);
        if (m.find()) {
            return true;
        }

        return false;
    }

    private String cutRCSFilePath(String rcsFile) {
        // cutoff the Attics
        String RCSFileCutOfAttics = rcsFile.replaceAll("/Attic/", "/");
        String workingFile = null;
        int remoteModulePathlength = fRemoteModulePath.length();
        // +1 because also the the '/' is cut of
        workingFile = RCSFileCutOfAttics.substring(remoteModulePathlength + 1);
        return workingFile;
    }
}
