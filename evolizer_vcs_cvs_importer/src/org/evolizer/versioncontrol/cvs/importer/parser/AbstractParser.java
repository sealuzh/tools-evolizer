/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.cvs.importer.parser;

import java.util.Vector;

import org.evolizer.core.exceptions.EvolizerRuntimeException;
import org.evolizer.versioncontrol.cvs.importer.mapper.api.ILogEntryListener;
import org.evolizer.versioncontrol.cvs.importer.parser.helper_entities.LogEntry;

/**
 * A CVS log parser has to extend this class. Implements an observer-pattern for notifying listeners whenever the parser
 * has generated a {@link LogEntry}-object.
 * 
 * @author wuersch, jetter
 */
public abstract class AbstractParser {

    private Vector<ILogEntryListener> fListeners = new Vector<ILogEntryListener>();

    /**
     * The Log is read in sequentially line by line. For every line this method needs to be called
     * 
     * @param line
     *            the String a Inputstream reads in.
     */
    public abstract void parseLine(String line);

    /**
     * Adding a observer to the observable parser.
     * 
     * @param listener
     *            the listener
     */
    public void addLogEntryListener(ILogEntryListener listener) {
        fListeners.add(listener);
    }

    /**
     * Removes a observer from the observable parser.
     * 
     * @param listener
     *            the listener
     */
    public void removeLogEntryListener(ILogEntryListener listener) {
        fListeners.remove(listener);
    }

    /**
     * Calls the processLogEntry-method of all registered listeners.
     * 
     * @param logEntry
     *            the log entry
     */
    protected void notifyLogEntryListeners(LogEntry logEntry) {
        if (fListeners.size() == 0) {
            throw new EvolizerRuntimeException("No listeners are attached to the cvs log parser!");
        }
        for (ILogEntryListener listener : fListeners) {
            listener.processLogEntry(logEntry);
        }
    }
}
