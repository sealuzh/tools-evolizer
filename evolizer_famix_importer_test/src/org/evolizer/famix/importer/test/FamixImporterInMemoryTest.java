/**
 * 
 */
package org.evolizer.famix.importer.test;

import static org.junit.Assert.assertNotNull;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * Siomple test class to test the HSQLDB in memory database features with Hibernate.
 *
 *
 * @author pinzger
 *
 */
public class FamixImporterInMemoryTest {
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        
    }
   
    @Test
    public void testCreateSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.setProperties(EvolizerSessionHandler.getDefaultHsqldbInMemoryConfig("test"));

        configuration.addAnnotatedClass(Entity.class);
        SessionFactory factory = configuration.buildSessionFactory();
        Session hibernateSession = factory.openSession();

        assertNotNull("Session must not be null", hibernateSession);
        hibernateSession.close();
    }
    
    @javax.persistence.Entity
    public class Entity {
        private Long Id;
        private String FirstName;
        private String LastName;
        
        private Integer pos;
        private Long endPos;
        private String veryLongText;
        
        public Entity() {
        }

        /**
         * @return the Id
         */
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        public Long getId() {
            return Id;
        }
        /**
         * @param Id the Id to set
         */
        public void setId(Long id) {
            this.Id = id;
        }
        /**
         * @return the firstName
         */
        public String getFirstName() {
            return FirstName;
        }
        /**
         * @param firstName the firstName to set
         */
        public void setFirstName(String firstName) {
            FirstName = firstName;
        }
        /**
         * @return the lastName
         */
        public String getLastName() {
            return LastName;
        }
        /**
         * @param lastName the lastName to set
         */
        public void setLastName(String lastName) {
            LastName = lastName;
        }
        /**
         * @return the pos
         */
        public Integer getPos() {
            return pos;
        }
        /**
         * @param pos the pos to set
         */
        public void setPos(Integer position) {
            this.pos = position;
        }
        /**
         * @return the veryLongText
         */
        @Lob
        public String getVeryLongText() {
            return veryLongText;
        }
        /**
         * @param veryLongText the veryLongText to set
         */
        public void setVeryLongText(String veryLongText) {
            this.veryLongText = veryLongText;
        }
        /**
         * @return the endPos
         */
        public Long getEndPos() {
            return endPos;
        }
        /**
         * @param endPos the endPos to set
         */
        public void setEndPos(Long endPos) {
            this.endPos = endPos;
        }
    }
}
