package org.evolizer.model.resources;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.model.resources.entities.fs.Directory;
import org.evolizer.model.resources.entities.fs.File;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.model.resources.entities.humans.Role;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ResourceModelTest{
	
	private static final String DB_CONFIG_FILE = "./config/db.properties";
	protected static String fDBUrl;
	protected static String fDBDialect;
	protected static String fDBDriverName;
	protected static String fDBUser;
	protected static String fDBPasswd;

	private EvolizerSessionHandler fSessionHandler;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStream propertiesInputStream = ResourceModelTest.openFile(DB_CONFIG_FILE);

		if (propertiesInputStream != null) {
			Properties props = new Properties();
			props.load(propertiesInputStream);
			propertiesInputStream.close();

			fDBUrl = props.getProperty("dbUrl");
			fDBDialect = props.getProperty("dbDialect");
			fDBDriverName = props.getProperty("dbDriverName");
			fDBUser = props.getProperty("dbUser");
			fDBPasswd = props.getProperty("dbPasswd");
		}
		
		propertiesInputStream.close();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		fSessionHandler = EvolizerSessionHandler.getHandler();
		fSessionHandler.initSessionFactory(fDBUrl, fDBUser, fDBPasswd);
		fSessionHandler.createSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
	}

	@After
	public void tearDown() throws Exception {
		fSessionHandler.cleanupHibernateSessions();
		fSessionHandler.dropSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
		fSessionHandler = null;
	}

	@Test
	public void testPerson() {
		Person p = new Person();
		p.setFirstName("Michael");
		p.setLastName("Wuersch");
		p.addNickName("Michili");
		p.addNickName("'se Michiator");
		assertEquals("Michael Wuersch", p.getLabel());
		
		Person pp = saveAndReloadUniqueFromDB(p, "from Person", Person.class);
		
		assertEquals("Michael", pp.getFirstName());
		assertEquals("Wuersch", pp.getLastName());
		assertTrue(pp.getNickNames().contains("Michili"));
		assertTrue(pp.getNickNames().contains("'se Michiator"));
	}
	
	@Test
	public void testRoles() {
		Role role = new Role("Developer");
		
		assertEquals("Developer", role.getLabel());
		
		Role pRole = saveAndReloadUniqueFromDB(role, "from Role", Role.class);
		assertEquals("Developer", pRole.getLabel());
	}
	
	@Test
	public void testPersonWithRoles() {
		Person p = new Person();
		p.setFirstName("Emanuel");
		p.setLastName("Giger");
		p.addNickName("Gigs");
		p.addNickName("Flex0r");

		
		Role firstRole = new Role("Developer");
		Role secondRole = new Role("Gamer");
		
		p.addRole(firstRole);
		p.addRole(secondRole);

		save(p, firstRole, secondRole);
		Person loadedP = loadUnique("from Person", Person.class);
		assertEquals("Emanuel Giger", loadedP.getLabel());
		Set<Role> roles = loadedP.getRoles();
		assertEquals(2, roles.size());
		assertTrue(roles.contains(firstRole));
		assertTrue(roles.contains(secondRole));
	}
	
	@Test
	public void testFilesAndDirectories() {
		File f = new File("/rootFile.txt", Directory.ROOT);
		assertEquals("/rootFile.txt", f.getPath());
		assertEquals("rootFile.txt", f.getName());
		assertEquals(Directory.ROOT, f.getParentDirectory());
		
		Directory d = new Directory("/home", Directory.ROOT);
		assertEquals("/home", d.getPath());
		assertEquals("home", d.getName());
		assertEquals(Directory.ROOT, d.getParentDirectory());
		
		save(f, Directory.ROOT, d);
		File fLoaded = loadUnique("from File as f where f.path like '%txt'", File.class);
		
		assertEquals(f, fLoaded);
		assertEquals(f.getParentDirectory(), Directory.ROOT);
		
		File dLoaded = loadUnique("from Directory as f where f.path like '/home'", File.class);
		assertEquals(d, dLoaded);
		assertEquals(Directory.ROOT, d.getParentDirectory());
		
		Directory rLoaded = loadUnique("from Directory as f where f.path like '/'", Directory.class);
		assertEquals(Directory.ROOT, rLoaded);
		assertEquals(2, rLoaded.getContent().size());
		assertTrue(rLoaded.getContent().contains(f));
		assertTrue(rLoaded.getContent().contains(d));
	}
	
	private static InputStream openFile(String relativeFSPath) throws IOException {
		return FileLocator.openStream(EvolizerModelResourcesPlugin.getDefault().getBundle(), new Path(relativeFSPath), false);
	}
	
	private void save(Object... objects) {
		try {
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			s.startTransaction();
			for(Object o : objects) {
				s.saveObject(o);
			}
			s.endTransaction();
			s.close();
		} catch (EvolizerException e) {
			fail(e.getMessage());
		}
	}
	
	private <T>T loadUnique(String reloadQuery, Class<T> resultType) {
		try {
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			T result = s.uniqueResult(reloadQuery, resultType);
			
			return result;
		} catch (EvolizerException e) {
			fail(e.getMessage());
			return null;
		}
	}

	private <T>T saveAndReloadUniqueFromDB(Object object, String reloadQuery, Class<T> resultType) {
		try {
			IEvolizerSession s = fSessionHandler.getCurrentSession(fDBUrl);
			s.startTransaction();
			s.saveObject(object);
			s.endTransaction();

			s.close();

			s = fSessionHandler.getCurrentSession(fDBUrl);

			T result = s.uniqueResult(reloadQuery, resultType);
			return result;
		} catch (EvolizerException e) {
			fail(e.getMessage());
			return null;
		}
	}
}
