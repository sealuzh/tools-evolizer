/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.git.importer.job;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.versioncontrol.git.importer.EvolizerGITPlugin;
import org.evolizer.versioncontrol.git.importer.EvolizerGitImporter;
import org.evolizer.versioncontrol.git.importer.exceptions.GitImporterException;

/**
 * Job that wraps {@link EvolizerGitImporter} to import the GIT information of a given {@link IProject} into the RHDB.
 * 
 * @see EvolizerGitImporter
 * @author ghezzi
 * 
 */
public class GITImporterJob extends Job {

    /**
     * The standard logger
     */
    private static final Logger LOGGER =
            EvolizerGITPlugin.getLogManager().getLogger(GITImporterJob.class.getCanonicalName());

    /**
     * The Eclipse project associated to this import
     */
    private IProject fProject;
    /**
     * A comma separated list of the file extensions for which the importer should fetch the contents
     */
    private String fFileExtensionRegEx;

    /**
     * Constructor
     * 
     * @param project
     *            the project
     * @param fileExtensionRegEx
     *            the file extension regex
     */
    public GITImporterJob(IProject project, String fileExtensionRegEx) {
        super("Import GIT Versioning Information");
        fProject = project;
        fFileExtensionRegEx = fileExtensionRegEx;
    }

    /**
     * Constructor.
     * 
     * @param project
     *            the project
     */
    public GITImporterJob(IProject project) {
        super("Import GIT Versioning Information");
        fProject = project;
        fFileExtensionRegEx = "*";
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    protected IStatus run(IProgressMonitor monitor) {
        LOGGER.info("Starting import of project ");

        EvolizerSessionHandler handler = EvolizerSessionHandler.getHandler();
        try {
            handler.updateSchema(fProject);
            IEvolizerSession persistenceProvider = handler.getCurrentSession(fProject);
            EvolizerGitImporter importer =
                    new EvolizerGitImporter(
                            fProject.getLocation().toString(),
                            persistenceProvider,
                            fFileExtensionRegEx,
                            monitor);
            importer.importProject();
            persistenceProvider.close();
        } catch (EvolizerException e) {
            LOGGER.error("Error while importing versioning history for project " + fProject.getName(), e);
            return Status.CANCEL_STATUS;
        } catch (GitImporterException e) {
            LOGGER.error("Error while importing versioning history for project " + fProject.getName(), e);
            return Status.CANCEL_STATUS;
        }
        return Status.OK_STATUS;
    }

}
