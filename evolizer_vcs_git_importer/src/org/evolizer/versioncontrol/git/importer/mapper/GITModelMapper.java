/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.git.importer.mapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.model.resources.entities.fs.Directory;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.model.resources.entities.humans.Role;
import org.evolizer.versioncontrol.cvs.model.entities.CommitterRole;
import org.evolizer.versioncontrol.cvs.model.entities.ModificationReport;
import org.evolizer.versioncontrol.cvs.model.entities.Release;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.evolizer.versioncontrol.cvs.model.entities.VersionedFile;
import org.evolizer.versioncontrol.git.importer.EvolizerGITPlugin;
import org.evolizer.versioncontrol.git.importer.exceptions.GitImporterException;

import edu.nyu.cs.javagit.api.commands.GitLogResponse.Commit;
import edu.nyu.cs.javagit.api.commands.GitLogResponse.CommitFile;

/**
 * Maps the git history found by the GitImporter to the versioning model of
 * {@link org.evolizer.versioncontrol.cvs.model.entities} and maps it into a database.
 * 
 * @author ghezzi
 * 
 */
public class GITModelMapper {

    /**
     * The standard logger
     */
    private static final Logger LOGGER =
            EvolizerGITPlugin.getLogManager().getLogger(GITModelMapper.class.getCanonicalName());
    /**
     * The transactions found
     */
    private HashMap<String, Transaction> fTransactions = new HashMap<String, Transaction>();
    /**
     * The files found
     */
    private HashMap<String, VersionedFile> fFiles = new HashMap<String, VersionedFile>();
    /**
     * The directories found
     */
    private HashMap<String, Directory> fDirectories = new HashMap<String, Directory>();
    /**
     * The persons (authors) found
     */
    private HashMap<String, Person> fPersons = new HashMap<String, Person>();
    /**
     * The release that is currently being analyzed at any given time
     */
    private Release fCurrRelease;
    /**
     * Hibernate Session providing the connection to the database
     */
    private IEvolizerSession fSession;

    /**
     * Constructor
     * 
     * @param session
     *            the Evolizer session that will be used to save the model
     */
    public GITModelMapper(IEvolizerSession session) {
        fSession = session;
    }

    /**
     * Saves the model
     */
    public void saveModel() {
        fSession.startTransaction();
        fSession.saveOrUpdate(fCurrRelease);
        for (Transaction t : fTransactions.values()) {
            fSession.saveOrUpdate(t);
        }
        for (VersionedFile v : fFiles.values()) {
            fSession.saveOrUpdate(v);
        }
        for (Directory d : fDirectories.values()) {
            fSession.saveOrUpdate(d);
        }
        for (Person p : fPersons.values()) {
            fSession.saveOrUpdate(p);
        }
        fSession.endTransaction();
    }

    /**
     * Creates a new {@link Release}.
     * 
     * @param tag
     *            the name/tag of the release to create
     * @param date
     *            the creation date
     */
    public void createRelease(String tag, String date) {
        if (fCurrRelease != null) {
            fSession.saveOrUpdate(fCurrRelease);
        }
        Release rel = new Release(tag);
        fCurrRelease = rel;
        try {
            rel.setTimeStamp(this.translateDateString(date));
        } catch (ParseException e) {
            rel.setTimeStamp(null);
        }
    }

    /**
     * Creates a new {@link Directory}.
     * 
     * @param path
     *            the Path of the directory to create
     * @return the {@link Directory} created
     */
    private Directory createDirectory(String path) {
        Directory directory = fDirectories.get(path);
        if (directory == null) {
            directory = new Directory(path, null);
            Directory parent = null;
            if (path != null) {
                String parentPath = this.getParentDirectoryPath(path);
                if (parentPath != null) {
                    parent = fDirectories.get(parentPath);
                    if (parent == null) {
                        parent = createDirectory(parentPath);
                    }
                    directory.setParentDirectory(parent);
                    parent.add(directory);
                }
            }
            fDirectories.put(path, directory);
            LOGGER.debug("Created Directory [" + directory + "]");
        }
        return directory;
    }

    /**
     * Creates a new {@link VersionedFile}
     * 
     * @param path
     *            the Path of the file to create
     * @return the {@link VersionedFile} created
     */
    private VersionedFile createFile(String path) {
        VersionedFile file = fFiles.get(path);
        if (file == null) {
            file = new VersionedFile(path, null);
            String parentPath = this.getParentDirectoryPath(path);
            if (parentPath != null) {
                Directory parent = fDirectories.get(parentPath);
                if (parent == null) {
                    parent = this.createDirectory(parentPath);
                }
                file.setParentDirectory(parent);
                parent.add(file);
            }
            fFiles.put(path, file);
            LOGGER.debug("Created File [" + file + "]");
        }
        return file;
    }

    /**
     * Extracts parent directory path from given path.
     * 
     * @param path
     *            The path from which to extract the parent directory from.
     * @return The parent directory found.
     */
    private String getParentDirectoryPath(String path) {
        if (path.lastIndexOf("/") <= 0) {
            return null;
        } else {
            return path.substring(0, path.lastIndexOf("/"));
        }
    }

    /**
     * Creates a new {@link Transaction}
     * 
     * @param commit
     *            the Git {@link edu.nyu.cs.javagit.api.Commit} making up the transaction
     * @param filesContent
     *            the contents of the files in the commit (can be null, empty or containing only a subset of the files
     *            in the commit)
     * @throws GitImporterException
     *             if problems arise while parsing the commit date
     */
    public void createTransaction(Commit commit, Map<String, String> filesContent) throws GitImporterException {
        Transaction currTrans;
        if (fTransactions.containsKey(commit.getSha())) {
            LOGGER
                    .debug("This commit was already extracted for a previous release, so, I'll just attach its related FileVersions to the current release");
            currTrans = fTransactions.get(commit.getSha());
            for (Revision rev : currTrans.getInvolvedRevisions()) {
                fCurrRelease.addRevision(rev);
                rev.addRelease(fCurrRelease);
            }
        } else {
            if (commit.getFiles() != null) {
                currTrans = new Transaction();
                fTransactions.put(commit.getSha(), currTrans);
                try {
                    currTrans.setStarted(this.translateDateString(commit.getDateString()));
                    currTrans.setFinished(this.translateDateString(commit.getDateString()));
                } catch (ParseException e) {
                    throw new GitImporterException("Error while parsing a commit date:\n" + e.getMessage());
                }
                // List<String> res = commit.getMergeDetails();

                for (CommitFile commitFile : commit.getFiles()) {
                    LOGGER.debug("found file " + commitFile.getName());
                    // Check if the file in this commit has already been found in an earlier commit
                    VersionedFile file = this.createFile(commitFile.getName());
                    Revision revision = new Revision(commit.getSha());
                    revision.setFile(file);
                    if (filesContent != null && !filesContent.isEmpty()) {
                        String source = filesContent.get(commitFile.getName());
                        if (source != null) {
                            revision.setSource(source);
                        }
                    }
                    revision.setState(commitFile.getChangeType());
                    Person author = this.createPerson(commit.getAuthor());
                    this.createModificationReport(commitFile, commit, revision, author);

                    // Getting the committer role and adding this new revision to it
                    for (Role r : author.getRoles()) {
                        if (r instanceof CommitterRole) {
                            ((CommitterRole) r).addRevision(revision);
                            break;
                        }
                    }

                    Revision latestRevision = file.getLatestRevision();
                    if (latestRevision != null) {
                        latestRevision.setNextRevision(revision);
                        revision.setPreviousRevision(latestRevision);
                    }
                    file.addRevision(revision);
                    revision.addRelease(fCurrRelease);
                    fCurrRelease.addRevision(revision);
                    currTrans.addRevision(revision);
                    LOGGER.debug("Created Revision " + revision.getNumber() + " for file " + file.getName()
                            + " in release " + fCurrRelease.getName());
                }
            } else {
                LOGGER.info("Commit " + commit.getSha() + " was empty so I'm skipping it");
            }
        }
    }

    /**
     * Creates a new {@link Person}.
     * 
     * @param author
     *            the String containing all the person's info as extracted from the git log
     * @return the {@link Person} created
     */
    private Person createPerson(String author) {
        String authorName = author.substring(0, author.indexOf("<") - 1).trim();
        String authorEmail = author.substring(author.indexOf("<") + 1, author.indexOf(">"));
        Person person;
        if (author.startsWith("<")) {
            authorEmail = author.substring(author.indexOf("<") + 1, author.indexOf(">"));
            person = fPersons.get(authorEmail);
        } else {
            authorName = author.substring(0, author.indexOf("<") - 1).trim();
            authorEmail = author.substring(author.indexOf("<") + 1, author.indexOf(">"));
            person = fPersons.get(authorName);
        }
        
        if (person == null) {
            person = new Person();

            if (isEmail(authorEmail)) {
                person.setEmail(authorEmail);
            }
            if (authorName != null) {
                person.setFirstName(authorName);
            }
            CommitterRole role = new CommitterRole();
            person.addRole(role);
            fSession.saveObject(role);
            if (authorName != null) {
                fPersons.put(authorName, person);
            } else {
                fPersons.put(authorEmail, person);
            }
        }
        return person;
    }

    /**
     * Checks if a given String contains an email address.
     * 
     * @param input
     *            The string to check.
     * @return if an email address was found.
     */
    private boolean isEmail(String input) {
        Pattern p = Pattern.compile("(\\w+)@(\\w+\\.)(\\w+)(\\.\\w+)?"); // name@subdomain.domain.suffix
        Matcher m = p.matcher(input);
        if (m.find()) {
            return true;
        }

        return false;
    }

    /**
     * Creates a {@link ModificationReport}.
     * 
     * @param commitFile
     *            the {@link CommitFile} associated to the report
     * @param c
     *            the {@link edu.nyu.cs.javagit.api.Commit} from which the report originated
     * @param revision
     *            the {@link Revision} associated to this report
     * @param author
     *            the {@link Person} author of the report
     */
    private void createModificationReport(CommitFile commitFile, Commit c, Revision revision, Person author) {
        ModificationReport report = new ModificationReport();
        // Need to do that (due to a sort of a bug in in javagit)
        if (c.getMessage() == null) {
            report.setCommitMessage("");
        } else {
            report.setCommitMessage(c.getMessage().trim());
        }
        report.setLinesAdd(commitFile.getLinesAdded());
        report.setLinesDel(commitFile.getLinesDeleted());
        try {
            report.setCreationTime(this.translateDateString(c.getDateString()));
        } catch (ParseException e) {
            LOGGER.error("Error while adding the creation date to the modification report for commit " + c.getSha());
        }
        report.setAuthor(author);
        revision.setReport(report);
    }

    // TODO not happy about it as it basically modifies the original date (it switches time zones)
    private Date translateDateString(String date) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy Z");
        return df.parse(date);
    }
}
