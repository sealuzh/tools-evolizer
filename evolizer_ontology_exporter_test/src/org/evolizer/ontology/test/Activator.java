package org.evolizer.ontology.test;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends Plugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.evolizer.ontology.test";

	// The shared instance
	private static Activator plugin;

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Opens a file located within the plugin-bundle
	 * 
	 * @param filePath
	 *            relative path of the file starting
	 * @return an InputStream reading the specifed file
	 * @throws IOException
	 *             if file could not be opened
	 */
	public static InputStream openBundledFile(String filePath) throws IOException {
		return getDefault().getBundle().getEntry(filePath).openStream();
	}
}
