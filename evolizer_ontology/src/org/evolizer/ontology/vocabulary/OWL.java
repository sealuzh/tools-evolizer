package org.evolizer.ontology.vocabulary;

public final class OWL {
	public static final String BASE = "http://www.w3.org/2002/07/owl";
	public static final String LOCAL = "/ontologies/local_owl";
}
