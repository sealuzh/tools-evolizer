package org.evolizer.ontology.vocabulary;

public final class RDF {
	public static final String BASE = "http://www.w3.org/1999/02/22-rdf-syntax-ns";
	public static final String LOCAL = "/ontologies/local_rdf";
}
