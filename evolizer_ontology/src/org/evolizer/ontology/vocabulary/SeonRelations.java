package org.evolizer.ontology.vocabulary;

public class SeonRelations {
	public static final String BASE = "http://evolizer.org/ontologies/seon/2009/06/relations.owl";
	public static final String LOCAL = "local_seon_relations_09_06";
	
	// Object properties:
	public static final String HAS_PARENT = BASE + "#hasParent";
	public static final String HAS_CHILD = BASE + "#hasChild";
	public static final String HAS_SIBLING = BASE + "#hasSibling";
}
