package org.evolizer.ontology.vocabulary;

public final class RDFS {
	public static final String BASE = "http://www.w3.org/2000/01/rdf-schema";
	public static final String LOCAL = "/ontologies/local_rdfs";
}
