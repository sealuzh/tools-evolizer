package org.evolizer.ontology.util;

/**
 * This class represents a multi-purpose data structure to store
 * relationships between various resources in terms of statements,
 * i.e., s-p-o triples.
 * 
 * The statement can be typed using the generic parameters of the
 * class or just used with wild cards.
 * 
 * An example would be:
 * 
 * <pre>
 * S (String) | P (String) | O (String)
 * ------------------------------------
 * Person     | has name   | Michael
 * </pre>
 * 
 * <pre>
 * Statement<String, String, String> s = new Statement<String, String, String>("Person", "has name", "Michael");
 * </pre>
 * 
 * The contract of this class defines that a statement needs to be atomic.
 * Potentially n:n, 1:n, and n:1 relationships could  also be represented
 * by a single statement:
 * 
 * <pre>
 * S (String) | P (String) | O (String)
 * ------------------------------------
 * Michael     | has pet   | Pentium
 * Michael     | has pet   | Bruce
 * </pre>
 *
 * <pre>
 * Set<String> pets = new HashSet<String>();
 * pets.add("Pentium");
 * pets.add("Bruce");
 * 
 * Statement<String, String, Set<String>> s = new Statement<String, String, Set<String>>("Michael", "has pet", pets);
 * </pre>
 * 
 * It is, however, strongly recommended to use two separate triples to represent the information above, as clients will
 * probably not perform a deeper analysis of the three parts of the statement.
 * 
 * The statement can also be serialized by setting an {@link ISerializationStrategy} and calling {@link Statement#serialize()}.
 * 
 * @author wuersch
 *
 * @param <S> the type of the subject.
 * @param <P> the type of the predicate.
 * @param <O> the type of the object.
 */
public class Statement<S, P, O> {
	private S subject;
	private P predicate;
	private O object;
	
	private IStatementSerializationStrategy<S, P, O> serializer;
	
	/**
	 * Constructor.
	 * 
	 * @param pSubject a subject.
	 * @param pPredicate a predicate.
	 * @param pObject an object.
	 */
	public Statement(S subject, P predicate, O object) {
		this.subject = subject;
		this.predicate = predicate;
		this.object = object;
	}
	
	/**
	 * Sets a serialization strategy for this statement.
	 * 
	 * @param pSerializer a strategy that needs to be capable of serializing this kind of statement.
	 */
	public void setSerializationStrategy(IStatementSerializationStrategy<S, P, O> serializer) {
		this.serializer = serializer;
	}
	
	/**
	 * Getter for subject.
	 * 
	 * @return the subject of the statement.
	 */
	public S getSubject() {
		return subject;
	}

	/**
	 * Getter for predicate.
	 * 
	 * @return the predicate of the statement.
	 */
	public P getPredicate() {
		return predicate;
	}

	/**
	 * Getter for the object.
	 * 
	 * @return the object of the statement.
	 */
	public O getObject() {
		return object;
	}
	
	/**
	 * Checks whether the statement is complete.
	 * 
	 * @return <code>true</code> if subject =! null && predicate != null && object != null, otherwise <code>false</code>.
	 */
	public boolean isComplete() {
		return subject != null && predicate != null && object != null;
	}
	
	/**
	 * Serializes this statement.
	 * 
	 * Before invoking this method a suitable {@link IStatementSerializationStrategy} needs to
	 * be set by calling {@link Statement#setSerializationStrategy(IStatementSerializationStrategy)},
	 * otherwise the serialization request will be ignored.
	 */
	public void serialize() {
		if(serializer != null) {
			serializer.serialize(this);
		}
	}
	
	/**
	 * @return a String useful for debugging purposes.
	 */
	@Override
	public String toString() {
		return "<" 
				+ subject.toString()   + " (:" + subject.getClass().getSimpleName()   + "), "
				+ predicate.toString() + " (:" + predicate.getClass().getSimpleName() + "), "
				+ object.toString()    + " (:" + object.getClass().getSimpleName()+ ")"
				+ ">";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((object == null) ? 0 : object.hashCode());
		result = prime * result
				+ ((predicate == null) ? 0 : predicate.hashCode());
		result = prime * result
				+ ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Statement<?, ? ,?> other = (Statement<?, ?, ?>) obj;
		if (object == null) {
			if (other.object != null) {
				return false;
			}
		} else if (!object.equals(other.object)) {
			return false;
		}
		if (predicate == null) {
			if (other.predicate != null) {
				return false;
			}
		} else if (!predicate.equals(other.predicate)) {
			return false;
		}
		if (subject == null) {
			if (other.subject != null) {
				return false;
			}
		} else if (!subject.equals(other.subject)) {
			return false;
		}
		return true;
	}
}
