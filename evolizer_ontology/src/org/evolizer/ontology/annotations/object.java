package org.evolizer.ontology.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Marker annotation. Relations can be described in terms of triples, i.e.,
 * &lt;subject, predicate, object&gt;. Whenever a predicate is modeled as a java
 * class, this annotation comes in handy to denote which getter-Methods returns
 * the object-part of the statement.
 * 
 * @author wuersch
 * 
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface object {
}
