/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.ui.wizards;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.team.core.RepositoryProvider;
import org.eclipse.team.svn.core.SVNTeamProvider;
import org.eclipse.team.svn.core.resource.IRepositoryLocation;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNImporter;
import org.evolizer.versioncontrol.svn.importer.job.SVNImporterJob;
import org.evolizer.versioncontrol.ui.EvolizerVersioningUIPlugin;

/**
 * The Wizard to specify all the import settings required to run an {@link SVNImporterJob}, which takes then care to
 * execute the {@link EvolizerSVNImporter}.
 * 
 * @author ghezzi
 * 
 */
public class SVNImporterWizard extends Wizard {

    /*
     * The actual page.
     */
    private SVNImporterWizardPage page;
    /*
     * The repository path to the trunk/tags/branches folders (no need to care if the repository follows a standard layout).
     */
    private String trunkLocation = "";
    private String branchesLocation = "";
    private String tagsLocation = "";
    private String probableProjectRoot = "";
    /*
     * Whether or not the wizard was canceled.
     */
    private boolean canceled;
    /*
     * The project to import the historical file content from.
     */
    private IProject project;

    /**
     * Constructor.
     * 
     * @param project
     *            The project target of the SVN history extraction.
     */
    public SVNImporterWizard(IProject project) {
        super();
        this.project = project;
        String url = ((SVNTeamProvider) RepositoryProvider.getProvider(project)).getRepositoryResource().getUrl();
        if (url.contains("/trunk")) {
            this.probableProjectRoot = url.substring(0, url.indexOf("/trunk"));
        } else if (url.contains("/tags")) {
            this.probableProjectRoot = url.substring(0, url.indexOf("/tags"));
        } else if (url.contains("/branches")) {
            this.probableProjectRoot = url.substring(0, url.indexOf("/branches"));
        } else {
            this.probableProjectRoot = url;
        }

        IRepositoryLocation loc =
                ((SVNTeamProvider) RepositoryProvider.getProvider(project)).getRepositoryResource()
                        .getRepositoryLocation();
        this.tagsLocation = loc.getUrl();
        this.branchesLocation = loc.getUrl();
        this.trunkLocation = loc.getUrl();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean performFinish() {

        String[] directories = new String[3];
        if (this.page.isStandardLayout()) {
            directories = null;
        } else {
            directories[0] = this.page.getTrunk();
            directories[1] = this.page.getTags();
            directories[2] = this.page.getBranches();
        }
        String srcFilter = "";
        if (this.page.isImportSrc()) {
            srcFilter = this.page.getSourceFilter();
        }
        SVNImporterJob importerJob =
                new SVNImporterJob(
                        this.project,
                        new String[]{this.page.getUser(), this.page.getPwd()},
                        srcFilter,
                        directories,
                        this.page.getStartRevision(),
                        this.page.getEndRevision(),
                        2,
                        this.page.isDiff(),
                        this.page.isUpdate());
        importerJob.setUser(true);
        importerJob.schedule();
        return true;
    }

    /**
     * Returns the user defined SVN Trunk location.
     * 
     * @return A {@link String} containing the the trunk location.
     */
    public String getTrunkLocation() {
        return this.trunkLocation;
    }

    /**
     * Returns the user defined SVN Branches location.
     * 
     * @return A {@link String} containing the the branches location.
     */
    public String getBranchesLocation() {
        return this.branchesLocation;
    }

    /**
     * Returns the user defined SVN Tags location.
     * 
     * @return A {@link String} containing the the tags location.
     */
    public String getTagsLocation() {
        return this.tagsLocation;
    }

    /**
     * Checks if the wizard was canceled.
     * 
     * @return <code>true</code> if is canceled, <code>false</code> otherwise
     */
    public boolean isCanceled() {
        return this.canceled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean performCancel() {
        this.canceled = true;
        return super.performCancel();
    }

    /**
     * 
     * {@inheritDoc}
     */
    public void addPages() {
        this.page = new SVNImporterWizardPage("SVN Importer");
        this.page.setProbableRoot(this.probableProjectRoot);
        this.page.setUserBranches(this.branchesLocation);
        this.page.setUserTags(this.tagsLocation);
        this.page.setUserTrunk(this.trunkLocation);
        this.page.setTitle("SVN Importer settings");
        this.page.setDescription("Required settings for a correct SVN history import");
        this.page
        .setImageDescriptor(ImageDescriptor.createFromFile(
                EvolizerVersioningUIPlugin.class,
                "/icons/versioncontrol_background.png"));
        this.addPage(this.page);
    }

    /**
     * The {@link WizardPage} associated to the {@link SVNImporterWizard} to set all the required importer settings.
     * 
     * @author ghezzi
     * 
     */
    private class SVNImporterWizardPage extends WizardPage {

        /*
         * All the contents: error messages, labels, etc. used in the page.
         */
        private String srcImportMsg = "Import file contents";
        private String srcImportFilterMsg =
                "Filter the extension you want to import, for example \".java\".\nSeparate very extension with a \";\". Use * to import all types of files";
        private String srcImportErrorMsg =
                "Separate every extension with a \";\" or use * to import all types of files";
        private String startRevisionMsg =
                "Start revision (can be left empty if starting from the first ever revision):";
        private String endRevisionMsg = "End revision (can be left empty if importing up to the latest revision):";
        private String standardLayoutMsg =
                "Repository has standard trunk/tags/branches structure with root located at :\n";
        private String customLayoutMsg =
                "Repository has a custom structure, insert the trunk, tags and branches folders path";
        private String customLayoutTrunkMsg = "Trunk location: ";
        private String customLayoutBranchesMsg = "Branches location: ";
        private String customLayoutTagsMsg = "Tags location: ";
        private String svnUserSettingsTitle = "SVN User settings";
        private String svnUserSettingsUserMsg = "User: ";
        private String svnUserSettingsPasswordMsg = "Password: ";
        private String repoStructureTitle = "Repository structure settings";
        private String repoStructureError = "Set a proper path for the trunk, tags and branches folders";
        private String importSettingsMsg = "Import settings";
        private String noImportTypeError = "Select what type of import you want to run from the drop down menu";
        private String selectDiffMsg = "Calculate diff between subsequent file versions";
        private String noEndNegativeZeroValuesError = "Can't have negative/zero valued end revision values";
        private String noNegativeValuesError = "Can't have negative revision values";
        private String endSmallerThenStartError = "End revision can't be smaller or equal to the starting one";
        private String noStartAndEndError =
                "The start or end revision must be defined. Choose the \"Import the whole history\" option from the drop down menu if you want to import the entire repository history";
        /*
         * Regular expressions used.
         */
        private String fileExtensionsRegEx = "(\\*)|(\\.\\w+)(;\\.\\w+)*";
        private String fileExtensionsWildCard = "*";
        /*
         * The different file content fetching strategies
         */
        private String[] importTypes =
                new String[]{
                        "SELECT WHAT TO IMPORT",
                        "Import a specific revision range",
                        "Import the whole history",
                        "Update an already extracted SVN history"};
        /*
         * The error messages related to all the errors that prevent the wizard page from being complete.
         */
        private Set<String> errorMessages = new HashSet<String>();
        /*
         * The SWT related stuff:labels, buttons, combos, text, etc.
         */
        private Combo importTypeCombo;
        private Text trunkText;
        private Text branchesText;
        private Text tagsText;
        private Text srcText;
        private Text passwordText;
        private Text userText;
        private Text startRevisionText;
        private Text endRevisionText;
        private Label srcTextLabel;
        private Label trunkLabel;
        private Label tagsLabel;
        private Label branchesLabel;
        private Label startRevisionLabel;
        private Label endRevisionLabel;
        private Button diffCheck;
        private Button importSrcCheck;
        /*
         * The custom defined tags folder repository path
         */
        private String userTags;
        /*
         * The custom defined trunk folder repository path
         */
        private String userTrunk;
        /*
         * The custom defined branches folder repository path
         */
        private String userBranches;
        /*
         * The probable root of the project releated repository
         */
        private String probableRoot;
        /*
         * Flags indicating wheter or not the repository of the project has a custom or standard layout.
         */
        private static final int STANDARD_LAYOUT = 0;
        private static final int CUSTOM_LAYOUT = 1;
        private int repositoryLayout;
        /*
         * Flag indicating if all the settings in the page are complete.
         */
        private boolean isRepoSettingsComplete = true;
        /*
         * Flag indicating if the selected file extensions are valid
         */
        private boolean isFileExtensionsValid = true;
        /*
         * Flag indicating if the revision range selected is valid.
         */
        private boolean isRevisionValuesValid;
        /*
         * Whether or not the import source option was selected.
         */
        private boolean importSrc;
        /*
         * Whether or not the diff option was selected.
         */
        private boolean diff;
        /*
         * Whether or not the update option was selected.
         */
        private boolean update;

        /**
         * Constructor.
         * 
         * @param pageName
         *            The name of the page.
         */
        protected SVNImporterWizardPage(String pageName) {
            super(pageName);
        }

        /**
         * Sets the project repository probable root, to guide the user in case he/she needs to set the
         * trunk/tags/branches folders in the wizard.
         * 
         * @param probableRoot
         *            The probable root to set
         */
        public void setProbableRoot(String probableRoot) {
            this.probableRoot = probableRoot;
        }

        /**
         * Sets the project repository tags folder path in the wizard.
         * 
         * @param userTags
         *            The tags folder path string.
         */
        public void setUserTags(String userTags) {
            this.userTags = userTags;
        }

        /**
         * Sets the project repository trunk folder path in the wizard.
         * 
         * @param userTrunk
         *            The trunk folder path string.
         */
        public void setUserTrunk(String userTrunk) {
            this.userTrunk = userTrunk;
        }

        /**
         * Sets the project repository branches folder path in the wizard.
         * 
         * @param userBranches
         *            The branches folder path string.
         */
        public void setUserBranches(String userBranches) {
            this.userBranches = userBranches;
        }

        /**
         * Whether or not the import set up in the wizard page is about an SVN repository following a standard layout.
         * 
         * @return true if the repository is standard, otherwise false.
         */
        public boolean isStandardLayout() {
            return this.repositoryLayout == SVNImporterWizardPage.STANDARD_LAYOUT;
        }

        /**
         * Whether or not the import set up in the wizard page will also import source code.
         * 
         * @return true is the source code will be imported, otherwise false.
         */
        public boolean isImportSrc() {
            return this.importSrc;
        }

        /**
         * Whether or not the import set up in the wizard page will calculate diff between subsequent revisions.
         * 
         * @return true if the diff will be calculated, otherwise false.
         */
        public boolean isDiff() {
            return this.diff;
        }

        /**
         * Whether or not the import will update a pre-existing SVN history import.
         * 
         * @return
         */
        public boolean isUpdate() {
            return this.update;
        }

        /**
         * Returns the semicolon separated list of file extensions to import.
         * 
         * @return The string containing the list of file extensions.
         */
        public String getSourceFilter() {
            return this.srcText.getText();
        }

        /**
         * Returns the user defined trunk folder repository path .
         * 
         * @return The string containing the trunk folder path.
         */
        public String getTrunk() {
            return this.trunkText.getText();
        }

        /**
         * Returns the user defined tags folder repository path .
         * 
         * @return The string containing the tags folder path.
         */
        public String getTags() {
            return this.tagsText.getText();
        }

        /**
         * Returns the user defined branches folder repository path .
         * 
         * @return The string containing the branches folder path.
         */
        public String getBranches() {
            return this.branchesText.getText();
        }

        /**
         * Returns the revision from which the set up importer will start.
         * 
         * @return The starting revision.
         */
        public String getStartRevision() {
            return this.startRevisionText.getText();
        }

        /**
         * Returns the revision at which the set up importer will stop.
         * 
         * @return The end revision.
         */
        public String getEndRevision() {
            return this.endRevisionText.getText();
        }

        /**
         * Returns the user name the importer will use to access the SVN repository.
         * 
         * @return The string containing the user name.
         */
        public String getUser() {
            return this.userText.getText();
        }

        /**
         * Returns the password the importer will use to access the SVN repository.
         * 
         * @return The string containing the password.
         */
        public String getPwd() {
            return this.passwordText.getText();
        }

        /**
         * Creates the all the content for the user settings of the wizard page.
         * 
         * @param parent
         *            The page main component.
         */
        private void createUserSettingsGroup(Composite parent) {

            Group svnUserSettingsGroup = new Group(parent, SWT.SHADOW_ETCHED_IN);
            svnUserSettingsGroup.setText(this.svnUserSettingsTitle);

            GridLayout userSettingsGroupGridLayout = new GridLayout();
            userSettingsGroupGridLayout.numColumns = 1;
            svnUserSettingsGroup.setLayout(userSettingsGroupGridLayout);

            GridData repoGroupGridData = new GridData(GridData.FILL_HORIZONTAL);
            repoGroupGridData.horizontalSpan = 2;
            svnUserSettingsGroup.setLayoutData(repoGroupGridData);

            Label userLabel = new Label(svnUserSettingsGroup, SWT.NONE);
            userLabel.setText(this.svnUserSettingsUserMsg);

            this.userText = new Text(svnUserSettingsGroup, SWT.BORDER);
            this.userText.setText("");
            this.userText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

            Label passwordLabel = new Label(svnUserSettingsGroup, SWT.NONE);
            passwordLabel.setText(this.svnUserSettingsPasswordMsg);

            this.passwordText = new Text(svnUserSettingsGroup, SWT.BORDER | SWT.PASSWORD);
            this.passwordText.setText("");
            this.passwordText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        }

        /**
         * Creates the all the content for the repository settings of the wizard page.
         * 
         * @param parent
         *            The page main component.
         */
        private void createRepositorySettingsGroup(Composite parent) {
            Group svnRepositoryGroup = new Group(parent, SWT.SHADOW_ETCHED_IN);
            svnRepositoryGroup.setText(this.repoStructureTitle);

            GridLayout repoSettingsGroupGridLayout = new GridLayout();
            repoSettingsGroupGridLayout.numColumns = 1;
            svnRepositoryGroup.setLayout(repoSettingsGroupGridLayout);

            GridData repoGroupGridData = new GridData(GridData.FILL_HORIZONTAL);
            repoGroupGridData.horizontalSpan = 2;
            svnRepositoryGroup.setLayoutData(repoGroupGridData);

            Button standardLayoutButton = new Button(svnRepositoryGroup, SWT.RADIO);
            GridData standardLayoutGridData = new GridData(GridData.FILL_HORIZONTAL);
            standardLayoutButton.setLayoutData(standardLayoutGridData);
            standardLayoutButton.addSelectionListener(new SelectionAdapter() {

                public void widgetSelected(SelectionEvent e) {
                    if (((Button) e.getSource()).getSelection()) {
                        SVNImporterWizardPage.this.repositorySelectionChanged(SVNImporterWizardPage.STANDARD_LAYOUT);
                    }
                }
            });

            standardLayoutButton.setText(this.standardLayoutMsg + this.probableRoot);
            standardLayoutButton.setSelection(this.repositoryLayout == SVNImporterWizardPage.STANDARD_LAYOUT);
            standardLayoutButton.setEnabled(this.repositoryLayout == SVNImporterWizardPage.STANDARD_LAYOUT);

            Button customLayoutButton = new Button(svnRepositoryGroup, SWT.RADIO);
            GridData customLayoutGridData = new GridData(GridData.FILL_HORIZONTAL);
            customLayoutButton.setLayoutData(customLayoutGridData);
            customLayoutButton.addSelectionListener(new SelectionAdapter() {

                public void widgetSelected(SelectionEvent e) {
                    if (((Button) e.getSource()).getSelection()) {
                        SVNImporterWizardPage.this.repositorySelectionChanged(SVNImporterWizardPage.CUSTOM_LAYOUT);
                    }
                }
            });

            customLayoutButton.setText(this.customLayoutMsg);
            customLayoutButton.setSelection(this.repositoryLayout == SVNImporterWizardPage.CUSTOM_LAYOUT);

            this.trunkLabel = new Label(svnRepositoryGroup, SWT.NONE);
            this.trunkLabel.setText(this.customLayoutTrunkMsg);
            this.trunkLabel.setVisible(false);

            this.trunkText = new Text(svnRepositoryGroup, SWT.BORDER);
            GridData trunkGridData = new GridData(GridData.FILL_HORIZONTAL);
            this.trunkText.setLayoutData(trunkGridData);
            this.trunkText.setVisible(false);
            this.trunkText.setEnabled(false);
            this.trunkText.setText(this.userTrunk);
            this.trunkText.addKeyListener(new KeyListener() {

                public void keyReleased(KeyEvent e) {
                    SVNImporterWizardPage.this.repositoryCustomLayoutChanged();
                }

                public void keyPressed(KeyEvent e) {}
            });

            this.tagsLabel = new Label(svnRepositoryGroup, SWT.NONE);
            this.tagsLabel.setText(this.customLayoutTagsMsg);
            this.tagsLabel.setVisible(false);

            this.tagsText = new Text(svnRepositoryGroup, SWT.BORDER);
            GridData tagsGridData = new GridData(GridData.FILL_HORIZONTAL);
            this.tagsText.setLayoutData(tagsGridData);
            this.tagsText.setVisible(false);
            this.tagsText.setEnabled(false);
            this.tagsText.setText(this.userTags);
            this.tagsText.addKeyListener(new KeyListener() {

                public void keyReleased(KeyEvent e) {
                    SVNImporterWizardPage.this.repositoryCustomLayoutChanged();
                }

                public void keyPressed(KeyEvent e) {}
            });

            this.branchesLabel = new Label(svnRepositoryGroup, SWT.NONE);
            this.branchesLabel.setText(this.customLayoutBranchesMsg);
            this.branchesLabel.setVisible(false);

            this.branchesText = new Text(svnRepositoryGroup, SWT.BORDER);
            GridData branchesGridData = new GridData(GridData.FILL_HORIZONTAL);
            this.branchesText.setLayoutData(branchesGridData);
            this.branchesText.setVisible(false);
            this.branchesText.setEnabled(false);
            this.branchesText.setText(this.userBranches);
            this.branchesText.addKeyListener(new KeyListener() {

                public void keyReleased(KeyEvent e) {
                    SVNImporterWizardPage.this.repositoryCustomLayoutChanged();
                }

                public void keyPressed(KeyEvent e) {}
            });
        }

        /**
         * Creates the all the content for the import settings of the wizard page.
         * 
         * @param parent
         *            The page main component.
         */
        private void createImportSettingsGroup(Composite parent) {

            Group importSettingsGroup = new Group(parent, SWT.SHADOW_ETCHED_IN);
            importSettingsGroup.setText(this.importSettingsMsg);
            GridLayout importGroupGridLayout = new GridLayout();
            importGroupGridLayout.numColumns = 2;
            importSettingsGroup.setLayout(importGroupGridLayout);
            GridData groupGridData = new GridData(GridData.FILL_HORIZONTAL);
            groupGridData.horizontalSpan = 2;
            importSettingsGroup.setLayoutData(groupGridData);

            this.importTypeCombo = new Combo(importSettingsGroup, SWT.READ_ONLY);
            this.importTypeCombo.setLayoutData(new GridData(SWT.NONE, SWT.NONE, false, false, 2, 1));
            this.importTypeCombo.setItems(this.importTypes);
            this.importTypeCombo.select(0);
            this.importTypeCombo.addSelectionListener(new SelectionAdapter() {

                public void widgetSelected(SelectionEvent e) {
                    SVNImporterWizardPage.this.importTypeSelectionChanged(((Combo) e.getSource()).getSelectionIndex());
                }
            });

            this.startRevisionLabel = new Label(importSettingsGroup, SWT.NONE);
            this.startRevisionLabel.setText(this.startRevisionMsg);
            this.startRevisionLabel.setVisible(false);

            this.startRevisionText = new Text(importSettingsGroup, SWT.BORDER);
            this.startRevisionText.setText("");
            this.startRevisionText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
            this.startRevisionText.addKeyListener(new KeyAdapter() {

                public void keyReleased(KeyEvent e) {
                    SVNImporterWizardPage.this.revisionsChanged();
                }
            });
            this.startRevisionText.setVisible(false);

            this.endRevisionLabel = new Label(importSettingsGroup, SWT.NONE);
            this.endRevisionLabel.setText(this.endRevisionMsg);
            this.endRevisionLabel.setVisible(false);

            this.endRevisionText = new Text(importSettingsGroup, SWT.BORDER);
            this.endRevisionText.setText("");
            this.endRevisionText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
            this.endRevisionText.addKeyListener(new KeyAdapter() {

                public void keyReleased(KeyEvent e) {
                    SVNImporterWizardPage.this.revisionsChanged();
                }
            });
            this.endRevisionText.setVisible(false);

            this.diffCheck = new Button(importSettingsGroup, SWT.CHECK);
            GridData diffCheckGridData = new GridData(SWT.FILL, SWT.FILL, true, true);
            diffCheckGridData.horizontalSpan = 2;
            this.diffCheck.setLayoutData(diffCheckGridData);
            this.diffCheck.setText(this.selectDiffMsg);
            this.diffCheck.addSelectionListener(new SelectionAdapter() {

                public void widgetSelected(SelectionEvent e) {
                    SVNImporterWizardPage.this.diffSelectionChanged();
                }
            });
            this.diffCheck.setVisible(false);

            this.importSrcCheck = new Button(importSettingsGroup, SWT.CHECK);
            GridData importSrcGridData = new GridData(SWT.FILL, SWT.FILL, true, true);
            importSrcGridData.horizontalSpan = 2;
            this.importSrcCheck.setLayoutData(importSrcGridData);
            this.importSrcCheck.setText(this.srcImportMsg);
            this.importSrcCheck.addSelectionListener(new SelectionAdapter() {

                public void widgetSelected(SelectionEvent e) {
                    SVNImporterWizardPage.this.importSrcSelectionChanged();
                }
            });
            this.importSrcCheck.setVisible(false);

            this.srcTextLabel = new Label(importSettingsGroup, SWT.NONE);
            GridData srcTextGridData = new GridData(SWT.FILL, SWT.FILL, true, true);
            this.srcTextLabel.setLayoutData(srcTextGridData);
            this.srcTextLabel.setText(this.srcImportFilterMsg);
            this.srcTextLabel.setVisible(false);

            this.srcText = new Text(importSettingsGroup, SWT.BORDER);
            GridData srcGridData = new GridData(GridData.FILL_HORIZONTAL);
            this.srcText.setLayoutData(srcGridData);
            this.srcText.setEnabled(false);
            this.srcText.setText(this.fileExtensionsWildCard);
            this.srcText.setVisible(false);
            this.srcText.addKeyListener(new KeyListener() {

                public void keyReleased(KeyEvent e) {
                    SVNImporterWizardPage.this.fileExtensionsChanged(((Text) e.getSource()).getText());
                }

                public void keyPressed(KeyEvent e) {}
            });
        }

        /**
         * {@inheritDoc}
         */
        public void createControl(Composite parent) {

            Composite mainComposite = new Composite(parent, SWT.NONE);

            GridLayout mainGridLayout = new GridLayout();
            mainComposite.setLayout(mainGridLayout);
            mainGridLayout.numColumns = 1;
            this.createUserSettingsGroup(mainComposite);
            this.createRepositorySettingsGroup(mainComposite);
            this.createImportSettingsGroup(mainComposite);
            this.importTypeSelectionChanged(0);
            this.tryPageComplete();
            this.setControl(mainComposite);
        }

        /**
         * Checks that the revision range defined in the wizard is valid.
         */
        private void revisionsChanged() {
            if (this.getStartRevision().compareTo("") != 0) {
                this.errorMessages.remove(this.noEndNegativeZeroValuesError);
                this.errorMessages.remove(this.noStartAndEndError);
                Long start = Long.parseLong(this.getStartRevision());
                if (start < 0) {
                    this.isRevisionValuesValid = false;
                    this.tryPageComplete();
                    this.errorMessages.add(this.noNegativeValuesError);
                } else if (this.getEndRevision().compareTo("") != 0) {
                    this.errorMessages.remove(this.noNegativeValuesError);
                    Long end = Long.parseLong(this.getEndRevision());
                    if (end <= 0) {
                        this.isRevisionValuesValid = false;
                        this.errorMessages.add(this.noEndNegativeZeroValuesError);
                    } else if (start >= end) {
                        this.isRevisionValuesValid = false;
                        this.errorMessages.add(this.endSmallerThenStartError);
                    } else {
                        this.isRevisionValuesValid = true;
                        this.errorMessages.remove(this.endSmallerThenStartError);
                        this.errorMessages.remove(this.noEndNegativeZeroValuesError);
                    }
                } else {
                    this.isRevisionValuesValid = true;
                    this.errorMessages.remove(this.endSmallerThenStartError);
                    this.errorMessages.remove(this.noNegativeValuesError);
                    this.errorMessages.remove(this.noEndNegativeZeroValuesError);
                }
            } else if (this.getEndRevision().compareTo("") != 0) {
                this.errorMessages.remove(this.endSmallerThenStartError);
                this.errorMessages.remove(this.noNegativeValuesError);
                this.errorMessages.remove(this.noEndNegativeZeroValuesError);
                this.errorMessages.remove(this.noStartAndEndError);

                if (Long.parseLong(this.getEndRevision()) <= 0) {
                    this.isRevisionValuesValid = false;
                    this.errorMessages.add(this.noEndNegativeZeroValuesError);
                } else {
                    this.isRevisionValuesValid = true;
                    this.errorMessages.remove(this.noEndNegativeZeroValuesError);
                }
            } else {
                this.errorMessages.remove(this.endSmallerThenStartError);
                this.errorMessages.remove(this.noNegativeValuesError);
                this.errorMessages.remove(this.noEndNegativeZeroValuesError);

                this.isRevisionValuesValid = false;
                this.errorMessages.add(this.noStartAndEndError);
            }
            this.tryPageComplete();
        }

        /**
         * Checks if the page can be set as complete.
         */
        private void tryPageComplete() {
            if (!this.errorMessages.isEmpty()) {
                this.setErrorMessage(this.errorMessages.iterator().next());
            } else {
                this.setErrorMessage(null);
            }
            this.setPageComplete(this.isFileExtensionsValid && this.isRepoSettingsComplete
                    && this.isRevisionValuesValid && this.importTypeCombo.getSelectionIndex() > 0);
        }

        /**
         * Modifies the Page according the new selection
         * 
         */
        private void diffSelectionChanged() {
            this.diff = !this.diff;
        }

        /**
         * Checks that the given string is a valid instance of the file type regular expression (a semi colon separated
         * list of file extensions).
         * 
         * @param fileExtensions
         *            The string to check.
         */
        private void fileExtensionsChanged(String fileExtensions) {
            Pattern p = Pattern.compile(this.fileExtensionsRegEx);
            Matcher m = p.matcher(fileExtensions);
            this.isFileExtensionsValid = m.matches();
            if (!this.isFileExtensionsValid) {
                this.errorMessages.add(this.srcImportErrorMsg);
            } else {
                this.errorMessages.remove(this.srcImportErrorMsg);
            }
            this.tryPageComplete();
        }

        /**
         * Modifies the Page according the new selection
         * 
         * @param newSelection
         *            The selection driving the change
         */
        private void importSrcSelectionChanged() {
            this.importSrc = !this.importSrc;
            this.srcText.setEnabled(!this.srcText.isEnabled());
            this.srcText.setVisible(this.srcText.isEnabled());
            this.srcTextLabel.setVisible(this.srcText.isEnabled());
            if (!this.srcText.isVisible()) {
                this.srcText.setText(this.fileExtensionsWildCard);
                this.fileExtensionsChanged(this.fileExtensionsWildCard);
            }
            this.tryPageComplete();
        }

        /**
         * Updates the page layout: the labels, the input fields, the buttons, etc. depending on the import type
         * picked.
         * @param selectionIndex
         */
        private void importTypeSelectionChanged(int selectionIndex) {
            this.update = (selectionIndex == 3);
            if (selectionIndex > 0) {
                this.errorMessages.remove(this.noImportTypeError);
                if (selectionIndex == 1) {
                    this.revisionsChanged();
                    this.startRevisionLabel.setVisible(true);
                    this.startRevisionText.setVisible(true);
                    this.endRevisionLabel.setVisible(true);
                    this.endRevisionText.setVisible(true);
                } else {
                    this.startRevisionLabel.setVisible(false);
                    this.startRevisionText.setVisible(false);
                    this.startRevisionText.setText("");
                    this.endRevisionLabel.setVisible(false);
                    this.endRevisionText.setVisible(false);
                    this.endRevisionText.setText("");
                    this.errorMessages.remove(this.endSmallerThenStartError);
                    this.errorMessages.remove(this.noNegativeValuesError);
                    this.errorMessages.remove(this.noEndNegativeZeroValuesError);
                    this.errorMessages.remove(this.noStartAndEndError);
                    this.isRevisionValuesValid = true;
                }
                this.diffCheck.setVisible(true);
                this.importSrcCheck.setVisible(true);
            } else {
                this.startRevisionLabel.setVisible(false);
                this.startRevisionText.setVisible(false);
                this.startRevisionText.setText("");
                this.endRevisionLabel.setVisible(false);
                this.endRevisionText.setVisible(false);
                this.endRevisionText.setText("");
                this.diffCheck.setVisible(false);
                this.importSrcCheck.setVisible(false);
                this.errorMessages.add(this.noImportTypeError);
                this.errorMessages.remove(this.endSmallerThenStartError);
                this.errorMessages.remove(this.noNegativeValuesError);
                this.errorMessages.remove(this.noEndNegativeZeroValuesError);
                this.errorMessages.remove(this.noStartAndEndError);
                this.isRevisionValuesValid = true;
            }
            this.tryPageComplete();
        }

        /**
         * Modifies the Page according the new selection
         * 
         * @param newSelection
         *            The selection driving the change
         */
        private void repositorySelectionChanged(int newSelection) {
            this.repositoryLayout = newSelection;
            switch (this.repositoryLayout) {
                case SVNImporterWizardPage.STANDARD_LAYOUT:
                    this.branchesText.setEnabled(false);
                    this.branchesText.setVisible(false);
                    this.branchesLabel.setVisible(false);
                    this.tagsText.setEnabled(false);
                    this.tagsText.setVisible(false);
                    this.tagsLabel.setVisible(false);
                    this.trunkText.setEnabled(false);
                    this.trunkText.setVisible(false);
                    this.trunkLabel.setVisible(false);
                    this.isRepoSettingsComplete = true;
                    this.errorMessages.remove(this.repoStructureError);
                    this.tryPageComplete();
                    break;

                case SVNImporterWizardPage.CUSTOM_LAYOUT:
                    this.branchesText.setEnabled(true);
                    this.branchesText.setVisible(true);
                    this.branchesLabel.setVisible(true);
                    this.tagsText.setEnabled(true);
                    this.tagsText.setVisible(true);
                    this.tagsLabel.setVisible(true);
                    this.trunkText.setEnabled(true);
                    this.trunkText.setVisible(true);
                    this.trunkLabel.setVisible(true);
                    if (this.trunkText.getText() != null && !this.trunkText.getText().equals("")
                            && this.tagsText.getText() != null && !this.tagsText.getText().equals("")
                            && this.branchesText.getText() != null && !this.branchesText.getText().equals("")) {
                        this.isRepoSettingsComplete = true;
                        this.errorMessages.remove(this.repoStructureError);
                        this.tryPageComplete();
                    } else {
                        this.isRepoSettingsComplete = false;
                        this.errorMessages.add(this.repoStructureError);
                        this.tryPageComplete();
                    }

                    break;
                default:
                    break;
            }
        }

        /**
         * Checks that the custom repository settings are valid.
         */
        private void repositoryCustomLayoutChanged() {
            if (this.trunkText.getText() != null && !this.trunkText.getText().equals("")
                    && this.tagsText.getText() != null && !this.tagsText.getText().equals("")
                    && this.branchesText.getText() != null && !this.branchesText.getText().equals("")) {
                this.isRepoSettingsComplete = true;
                this.errorMessages.remove(this.repoStructureError);
                this.tryPageComplete();
            } else {
                this.isRepoSettingsComplete = false;
                this.errorMessages.add(this.repoStructureError);
                this.tryPageComplete();
            }
        }
    }
}
