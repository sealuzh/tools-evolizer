package org.evolizer.daforjava.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.util.projecthandling.JavaProjectHelper;
import org.evolizer.daforjava.DAForJavaPlugin;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public abstract class AbstractDAForJavaTest {
	private static final String DB_CONFIG_FILE = "./config/db.properties";

	protected static String fDBUrl;

	protected static String fDBDialect;

	protected static String fDBDriverName;

	protected static String fDBUser;

	protected static String fDBPasswd;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		InputStream propertiesInputStream = DAForJavaPlugin.getDefault().openBundledFile(DB_CONFIG_FILE);

		if (propertiesInputStream != null) {
			Properties props = new Properties();
			props.load(propertiesInputStream);
			propertiesInputStream.close();

			fDBUrl = props.getProperty("dbUrl");
			fDBDialect = props.getProperty("dbDialect");
			fDBDriverName = props.getProperty("dbDriverName");
			fDBUser = props.getProperty("dbUser");
			fDBPasswd = props.getProperty("dbPasswd");
		}
		
		propertiesInputStream.close();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		EvolizerSessionHandler.getHandler().cleanupHibernateSessions();
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	protected static void setUpProject() throws CoreException, IOException{
		JavaProjectHelper helper = new JavaProjectHelper();
		helper.createProject("TestTheBigVoid", "bin", null);
		helper.addStandartSourceFolder(null);
		helper.addPackage("thebigvoid", null);
		
		helper.addSourceFile("thebigvoid", "Galaxy.java", getFileContent(DAForJavaPlugin.getDefault().openBundledFile("./data/thebigvoid/Galaxy.java")), null);
		helper.addSourceFile("thebigvoid", "GasGiant.java", getFileContent(DAForJavaPlugin.getDefault().openBundledFile("./data/thebigvoid/GasGiant.java")), null);
		helper.addSourceFile("thebigvoid", "ILawsOfTheUniverse.java", getFileContent(DAForJavaPlugin.getDefault().openBundledFile("./data/thebigvoid/ILawsOfTheUniverse.java")), null);
		helper.addSourceFile("thebigvoid", "INonSolidObject.java", getFileContent(DAForJavaPlugin.getDefault().openBundledFile("./data/thebigvoid/INonSolidObject.java")), null);
		helper.addSourceFile("thebigvoid", "Planet.java", getFileContent(DAForJavaPlugin.getDefault().openBundledFile("./data/thebigvoid/Planet.java")), null);
		helper.addSourceFile("thebigvoid", "StellarObject.java", getFileContent(DAForJavaPlugin.getDefault().openBundledFile("./data/thebigvoid/StellarObject.java")), null);
		helper.addSourceFile("thebigvoid", "Universe.java", getFileContent(DAForJavaPlugin.getDefault().openBundledFile("./data/thebigvoid/Universe.java")), null);
	}
	
	protected static String getFileContent(InputStream in) throws IOException{
		String content = "";
		BufferedReader inReader = new BufferedReader(new InputStreamReader(in));
		while(inReader.ready()){
			String line = inReader.readLine();
			content = content + line +"\n";
		}
		return content;
	}
}
