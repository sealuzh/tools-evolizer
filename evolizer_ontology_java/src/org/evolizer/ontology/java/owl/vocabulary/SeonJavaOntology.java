package org.evolizer.ontology.java.owl.vocabulary;

/**
 * This class defines the essentials of the Seon Java Ontology in terms of
 * constants. It therefore bridges the gap between the OWL description of the entities
 * and the Evolizer environment.
 * 
 * @author wuersch
 *
 */
public class SeonJavaOntology {

	public static final String BASE = "http://evolizer.org/ontologies/seon/2009/06/java.owl";
	public static final String LOCAL = "local_seon_java_09_06";

	// Classes
	public static final String COMPLEX_TYPE = BASE + "#ComplexType";
	public static final String PACKAGE = BASE + "#Package";
	public static final String CLASS = BASE + "#Class";
	public static final String INTERFACE = BASE + "#Interface";
	public static final String FIELD = BASE + "#Field";
	public static final String METHOD = BASE + "#Method";
	public static final String CONSTRUCTOR = BASE + "#Constructor";
	public static final String PARAMETER = BASE + "#Parameter";
	
	// Object properties:
	public static final String HAS_PACKAGE_MEMBER = BASE + "#hasPackageMember";
	public static final String IS_PACKAGE_MEMBER_OF = BASE + "#isPackageMemberOf";
	
	public static final String HAS_DATA_TYPE = BASE + "#hasDatatype";
	public static final String IS_DATA_TYPE_OF = BASE + "#isDatatypeOf";

	public static final String HAS_SUPERCLASS = BASE + "#hasSuperClass";
	public static final String HAS_SUBCLASS = BASE + "#hasSubClass";

	public static final String HAS_SUPER_INTERFACE = BASE + "#hasSuperInterface";
	public static final String HAS_SUB_INTERFACE = BASE + "#hasSubInterface";
	public static final String IMPLEMENTS_INTERFACE = BASE + "#implementsInterface";
	
	public static final String DECLARES_FIELD = BASE + "#declaresField";
	public static final String IS_DECLARED_FIELD_OF = BASE + "#isDeclaredFieldOf";

	public static final String DECLARES_METHOD = BASE + "#declaresMethod";
	public static final String IS_DECLARED_METHOD_OF = BASE + "#isDeclaredMethodOf";
	
	public static final String DECLARES_CONSTRUCTOR = BASE + "#declaresConstructor";
	public static final String IS_DECLARED_CONSTRUCTOR_OF = BASE + "#isDeclaredConstructorOf";

	public static final String HAS_RETURN_TYPE = BASE + "#hasReturnType";
	public static final String IS_RETURN_TYPE_OF = BASE + "#isReturnTypeOf";
	public static final String HAS_PARAMETER = BASE + "#hasParameter";
	public static final String IS_PARAMETER_OF = BASE + "#isParameterOf";
	public static final String INVOKES_METHOD = BASE + "#invokesMethod";
	public static final String IS_INVOKED_BY_METHOD = BASE + "#isInvokedByMethod";
	public static final String INVOKES_CONSTRUCTOR = BASE + "#invokesConstructor";
	public static final String IS_INVOKED_BY_CONSTRUCTOR = BASE + "#isInvokedByConstructor";

	public static final String ACCESSES_FIELD = BASE + "#accessesField";
	public static final String IS_ACCESSED_BY_METHOD = BASE + "#isAccessedByMethod";

	// Datatype properties:
	public static final String HAS_PATH = BASE + "#hasPath";
	public static final String HAS_IDENTIFIER = BASE + "#hasIdentifier";
	public static final String STARTS_AT = BASE + "#startsAt";
	public static final String HAS_LENGTH = BASE + "#hasLength";
	public static final String IS_ABSTRACT = BASE + "#isAbstract";
	public static final String HAS_POSITION = BASE + "#hasPosition";
}
