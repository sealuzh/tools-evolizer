package org.evolizer.ontology.java.builder.model;

import org.evolizer.ontology.java.owl.vocabulary.SeonJavaOntology;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;

public interface IJavaModelEntities {
	public final static Resource JAVA_PACKAGE = ResourceFactory.createResource(SeonJavaOntology.PACKAGE);
	public final static Resource JAVA_CLASS = ResourceFactory.createResource(SeonJavaOntology.CLASS);
	public final static Resource JAVA_INTERFACE = ResourceFactory.createResource(SeonJavaOntology.INTERFACE);
	public final static Resource JAVA_FIELD = ResourceFactory.createResource(SeonJavaOntology.FIELD);
	public final static Resource JAVA_METHOD = ResourceFactory.createResource(SeonJavaOntology.METHOD);
	public final static Resource JAVA_CONSTRUCTOR = ResourceFactory.createResource(SeonJavaOntology.CONSTRUCTOR);
	public final static Resource JAVA_PARAMETER = ResourceFactory.createResource(SeonJavaOntology.PARAMETER);

	public final static Property HAS_PACKAGE_MEMBER = ResourceFactory.createProperty(SeonJavaOntology.HAS_PACKAGE_MEMBER);
	public final static Property IS_PACKAGE_MEMBER_OF = ResourceFactory.createProperty(SeonJavaOntology.IS_PACKAGE_MEMBER_OF);
	public final static Property HAS_SUPERCLASS = ResourceFactory.createProperty(SeonJavaOntology.HAS_SUPERCLASS);
	public static final Property HAS_SUPER_INTERFACE = ResourceFactory.createProperty(SeonJavaOntology.HAS_SUPER_INTERFACE);
	public static final Property IMPLEMENTS_INTERFACE = ResourceFactory.createProperty(SeonJavaOntology.IMPLEMENTS_INTERFACE);
	public final static Property DECLARES_FIELD = ResourceFactory.createProperty(SeonJavaOntology.DECLARES_FIELD);
	public final static Property IS_DECLARED_FIELD_OF = ResourceFactory.createProperty(SeonJavaOntology.IS_DECLARED_FIELD_OF);
	public final static Property HAS_DATA_TYPE = ResourceFactory.createProperty(SeonJavaOntology.HAS_DATA_TYPE);
	public final static Property DECLARES_METHOD = ResourceFactory.createProperty(SeonJavaOntology.DECLARES_METHOD);
	public final static Property IS_DECLARED_METHOD_OF = ResourceFactory.createProperty(SeonJavaOntology.IS_DECLARED_METHOD_OF);
	public final static Property DECLARES_CONSTRUCTOR = ResourceFactory.createProperty(SeonJavaOntology.DECLARES_CONSTRUCTOR);
	public final static Property IS_DECLARED_CONSTRUCTOR_OF = ResourceFactory.createProperty(SeonJavaOntology.IS_DECLARED_CONSTRUCTOR_OF);
	public final static Property HAS_RETURN_TYPE = ResourceFactory.createProperty(SeonJavaOntology.HAS_RETURN_TYPE);
	public final static Property HAS_PARAMETER = ResourceFactory.createProperty(SeonJavaOntology.HAS_PARAMETER);
	public final static Property IS_PARAMETER_OF = ResourceFactory.createProperty(SeonJavaOntology.IS_PARAMETER_OF);
	public static final Property ACCESSES_FIELD = ResourceFactory.createProperty(SeonJavaOntology.ACCESSES_FIELD);
	public final static Property INVOKES_METHOD = ResourceFactory.createProperty(SeonJavaOntology.INVOKES_METHOD);
	public final static Property INVOKES_CONSTRUCTOR = ResourceFactory.createProperty(SeonJavaOntology.INVOKES_CONSTRUCTOR);
	
	
	public final static Property HAS_PATH = ResourceFactory.createProperty(SeonJavaOntology.HAS_PATH);
	public final static Property HAS_IDENTIFIER = ResourceFactory.createProperty(SeonJavaOntology.HAS_IDENTIFIER);
	public final static Property STARTS_AT = ResourceFactory.createProperty(SeonJavaOntology.STARTS_AT);
	public final static Property HAS_LENGTH = ResourceFactory.createProperty(SeonJavaOntology.HAS_LENGTH);
	public final static Property HAS_POSITION = ResourceFactory.createProperty(SeonJavaOntology.HAS_POSITION);
}