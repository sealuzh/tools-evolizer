package org.evolizer.ontology.java.builder.model;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Abstract base class for constructor and method handles. The distinction is
 * important, although a constructor is a special method. However, Java
 * Interfaces and Annotations cannot have constructors. Further a constructor's
 * signature differs from a method's one. Constructor signatures can only have
 * access modifiers, i.e., they cannot be 'abstract', 'final', 'native',
 * 'static', or synchronized. They cannot return anything and they need to have
 * the same identifier as the class they're defined in.
 * 
 * @author wuersch
 * 
 */
public class AbstractCallableHandle extends AbstractSourceCodeEntityHandle {

	/**
	 * Constructor.
	 * 
	 * @param theEntity
	 *            the method or constructor to which this handle refers to.
	 * @param model
	 *            the related Java model.
	 */
	public AbstractCallableHandle(Individual theEntity, JavaOntModel model) {
		super(theEntity, model);
	}

	/**
	 * Adds a parameter to this method.
	 * 
	 * @param type
	 *            the handle for the type of the parameter.
	 * @param localName
	 *            the parameter name.
	 * @param location
	 *            location the source code location of the parameter declaration.
	 */
	public MethodParameterHandle addParameter(String qualifiedMethodParameterName, String localName, String typeName,
			SourceCodeLocationHelper location) {
				MethodParameterHandle paramHandle = model().getMethodParameterHandleFor(qualifiedMethodParameterName, localName, typeName);
				
				addRelation(IJavaModelEntities.HAS_PARAMETER, paramHandle);
				
				return paramHandle;
			}

	/**
	 * Adds a method call from this method to another one.
	 * 
	 * @param calleeHandle
	 *            the handle for the called method.
	 * @param location
	 *            location the source code location of the invocation.
	 */
	public void addMethodInvocation(AbstractCallableHandle calleeHandle, SourceCodeLocationHelper location) { // TODO source location? reification?
		addRelation(IJavaModelEntities.INVOKES_METHOD, calleeHandle);
	}

	/**
	 * Adds a field access from this method.
	 * 
	 * @param fieldHandle
	 *            the handle for the accessed field.
	 * @param location
	 *            location the source code location of the field access.
	 */
	public void addAccess(FieldHandle fieldHandle, SourceCodeLocationHelper location) { // TODO source location? reification?
		addRelation(IJavaModelEntities.ACCESSES_FIELD, fieldHandle);	
	}

}
