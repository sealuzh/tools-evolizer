package org.evolizer.ontology.java.builder.model;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Handle for Java interfaces.
 * 
 * @author wuersch
 *
 */
public class InterfaceHandle extends AbstractTypeHandle {
	/**
	 * Constructor.
	 * 
	 * @param theInterface
	 *            the interface to which this handle refers to.
	 * @param model
	 *            the associated Java source code model.
	 */
	public InterfaceHandle(Individual theInterface, JavaOntModel model) {
		super(theInterface, model);
	}

	/**
	 * Sets the super interface of the underlying interface. Does not perform
	 * any validation - callers of the method need to ensure that they do not
	 * accidentally introduce multiple inheritance (which is not possible in
	 * Java).
	 * 
	 * @param superInterfaceHandle
	 *            the handle of the super interface.
	 */
	public void setSuperInterface(InterfaceHandle superInterfaceHandle) {
		addRelation(IJavaModelEntities.HAS_SUPER_INTERFACE, superInterfaceHandle);
	}

	/**
	 * Adds an interface to the underlying interface. Basically a delegate for
	 * {@link #setSuperInterface(InterfaceHandle)}.
	 * 
	 * @param the
	 *            handle of the interface.
	 */
	@Override
	public void addInterface(InterfaceHandle interfaceHandle) {
		setSuperInterface(interfaceHandle);
	}
}
