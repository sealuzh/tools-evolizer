package org.evolizer.ontology.java.builder.model;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Handle for Java constructors.
 * 
 * @author wuersch
 * 
 */
public class ConstructorHandle extends AbstractCallableHandle {

	/**
	 * Constructor.
	 * 
	 * @param theEntity
	 *            the constructor to which this handle refers to.
	 * @param model
	 *            the associated Java source code model.
	 */
	public ConstructorHandle(Individual theEntity, JavaOntModel model) {
		super(theEntity, model);
	}

	/**
	 * Adds a constructor call from this constructor to another one.
	 * 
	 * @param calleeHandle
	 *            the handle for the called constructor.
	 * @param location
	 *            location the source code location of the invocation.
	 */
	public void addConstructorInvocation(ConstructorHandle calleeHandle, SourceCodeLocationHelper location) { // TODO source location? reification?
		addRelation(IJavaModelEntities.INVOKES_CONSTRUCTOR, calleeHandle);
	}
}
