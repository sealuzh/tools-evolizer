package org.evolizer.ontology.java.builder;

import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.evolizer.core.exceptions.EvolizerRuntimeException;
import org.evolizer.ontology.java.builder.model.CompilationUnitVisitor;
import org.evolizer.ontology.java.builder.model.JavaOntModel;

public class ProjectAnalyzer {
	public void performFullAnalysis(IJavaProject javaProject, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor);
		subMonitor.setTaskName("Performing full analysis of " + javaProject.getElementName());
		
		try {
			IPackageFragment[] packageFragments = javaProject.getPackageFragments();
			
			subMonitor.setWorkRemaining(packageFragments.length);
			
			for(IPackageFragment packageFragment : packageFragments) {
				if(packageFragment.getKind() == IPackageFragmentRoot.K_SOURCE && packageFragment.containsJavaResources()) {
					processPackage(packageFragment, subMonitor.newChild(1));
				}
			}
		} catch(JavaModelException jmex) {
			throw new EvolizerRuntimeException("Error while processing Java project. " + jmex.getMessage(), jmex); // TODO Build exception?
		}
		
		modelOf(javaProject).reconcile();
	}

	public void performDeltaAnalysis(IResourceDelta delta, IProgressMonitor monitor) {
		
	}

	private void processPackage(IPackageFragment packageFragment, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor);
		subMonitor.setTaskName("Processing " + packageFragment.getElementName());
		
		try {
			ICompilationUnit[] compilationUnits = packageFragment.getCompilationUnits();
			
			subMonitor.setWorkRemaining(compilationUnits.length);
			
			for (ICompilationUnit compilationUnit : compilationUnits) {
				processCompilationUnit(compilationUnit, packageFragment.getElementName(), subMonitor.newChild(1));
			}
		} catch(JavaModelException jmex) {
			throw new EvolizerRuntimeException("Error while processing package '" + packageFragment.getElementName() + "'. " + jmex.getMessage(), jmex); // TODO Build exception?
		}
	}

	private void processCompilationUnit(ICompilationUnit compilationUnit, String packageName, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		subMonitor.setTaskName("Parsing " + compilationUnit.getElementName());
		
		ASTParser lParser = ASTParser.newParser(AST.JLS3); // up to J2SE 1.5
		lParser.setSource(compilationUnit);
		lParser.setResolveBindings(true);
		
		CompilationUnit cu = (CompilationUnit) lParser.createAST(subMonitor.newChild(60));
		cu.accept(new CompilationUnitVisitor(compilationUnit, packageName, modelOf(compilationUnit.getJavaProject())));
		subMonitor.worked(40);
	}

	// TODO DI?
	private JavaOntModel modelOf(IJavaProject javaProject) {
		JavaOntModel model;
		
		if(JavaOntModelStore.INSTANCE.hasModel(javaProject)) {
			model = JavaOntModelStore.INSTANCE.getModel(javaProject);
		} else {
			model = JavaOntModelStore.INSTANCE.storeModel(javaProject);
		}
		
		return model;
	}
}
