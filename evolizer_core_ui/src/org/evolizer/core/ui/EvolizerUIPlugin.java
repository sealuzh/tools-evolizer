/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.core.ui;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.evolizer.core.logging.base.PluginLogManager;
import org.evolizer.core.ui.util.ImageCache;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle.
 * 
 * @author wuersch
 */
public class EvolizerUIPlugin extends AbstractUIPlugin {

    /**
     * The plug-in ID
     */
    public static final String PLUGIN_ID = "org.evolizer.core.ui";

    // The shared instance
    private static EvolizerUIPlugin sPlugin;

    // The path to the log4j.properties file
    private static final String LOG_PROPERTIES_FILE = "config/log4j.properties";

    // The log manager
    private PluginLogManager fLogManager;
    
    private ImageCache imageCache;

    /**
     * The constructor.
     */
    public EvolizerUIPlugin() {
        EvolizerUIPlugin.sPlugin = this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        configure();
        
        imageCache = new ImageCache();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop(BundleContext context) throws Exception {
    	EvolizerUIPlugin.sPlugin = null;
    	imageCache.dispose();

        if (fLogManager != null) {
            fLogManager.shutdown();
            fLogManager = null;
        }

        super.stop(context);
    }

    /**
     * Returns the shared instance.
     * 
     * @return the shared instance
     */
    public static EvolizerUIPlugin getDefault() {
        return EvolizerUIPlugin.sPlugin;
    }

	/**
	 * Converts a relative path to an {@link URL} object.
	 * 
	 * @param relativeFSPath a path relative to the root of the current bundle.
	 * @return a {@link URL} pointing to the file/folder specified by the bundle-relative path.
	 */
	public URL getURL(String relativeFSPath) throws IOException {
		return FileLocator.find(getBundle(), new Path(relativeFSPath), null);
	}
	
	public Image getImage(ImageDescriptor descriptor) {
		return imageCache.get(descriptor);
	}
	
	/**
    * Returns an image descriptor for the image file at the given plug-in relative path.
    * 
    * @param localPath
    *            the relative path starting at the root of this plug-in
    * @return the image descriptor
    */
	public ImageDescriptor getImageDescriptor(String localPath) {
		try {
			URL url = getURL(localPath);
			return ImageDescriptor.createFromURL(url);
		} catch (IOException e) {
			// should not happen
			return ImageDescriptor.getMissingImageDescriptor();
		}
	}

    /**
     * Opens a file located within the plugin-bundle.
     * 
     * @param filePath
     *            relative path of the file starting at the root of this plugin
     * @return an InputStream reading the specified file
     * @throws IOException
     *             if file could not be opened
     */
    public InputStream openBundledFile(String filePath) throws IOException {
        return EvolizerUIPlugin.getDefault().getBundle().getEntry(filePath).openStream();
    }

    /**
     * Returns the log manager.
     * 
     * @return the log manager
     */
    public PluginLogManager getLogManager() {
        return getDefault().fLogManager;
    }

    /**
     * Configures logging
     */
    private void configure() {
        try {
            InputStream propertiesInputStream = openBundledFile(LOG_PROPERTIES_FILE);

            if (propertiesInputStream != null) {
                Properties props = new Properties();
                props.load(propertiesInputStream);
                propertiesInputStream.close();

                fLogManager = new PluginLogManager(this, props);
            }

            propertiesInputStream.close();
        } catch (IOException e) {
            String message = "Error while initializing log properties." + e.getMessage();

            IStatus status =
                    new Status(IStatus.ERROR, getDefault().getBundle().getSymbolicName(), IStatus.ERROR, message, e);
            getLog().log(status);

            throw new RuntimeException("Error while initializing log properties.", e);
        }
    }
}
