package org.evolizer.ontology.exporter.reflection.converter;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.evolizer.ontology.exporter.EvolizerOntologyExporterPlugin;
import org.evolizer.ontology.exporter.api.IRDFPropertyConverter;
import org.evolizer.ontology.provider.util.Statement;

/**
 * Converts each element of a collection to rdf by invoking the appropriate converter.
 * 
 * @author wuersch
 *
 */
public class CollectionConverter implements IRDFPropertyConverter {
	private final static Logger logger = EvolizerOntologyExporterPlugin.getLogManager().getLogger(CollectionConverter.class.getName());

	public boolean canConvert(Class<?> type) {
		Class<?>[] interfaces = type.getInterfaces();
		for (Class<?> ifc : interfaces) {
			if(ifc.equals(Collection.class)) {
				return true;
			}
		}
		
		return false;
	}

	public Collection<Statement<?, ?>> convert(Object subject, String propertyURI, Object objectCollection) {
		Collection<Statement<?,?>> statements = new ArrayList<Statement<?,?>>();
		
		Collection<?> collection = (Collection<?>) objectCollection;
		logger.debug("Converting collection with " + collection.size() + " elements.");

		for (Object object : collection) {
			IRDFPropertyConverter converter = ConverterRegistry.getConverterFor(object.getClass());
			Collection<Statement<?, ?>> statementsToAdd = converter.convert(subject,  propertyURI, object);
			statements.addAll(statementsToAdd);
		}
		
		return statements;
	}
	

}
