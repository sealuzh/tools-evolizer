package org.evolizer.ontology.exporter.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.evolizer.core.util.reflection.api.IReflectionProvider;
import org.evolizer.core.util.reflection.api.impl.DefaultReflectionProvider;
import org.evolizer.ontology.annotations.object;
import org.evolizer.ontology.annotations.rdf;
import org.evolizer.ontology.annotations.subject;
import org.evolizer.ontology.exporter.EvolizerOntologyExporterPlugin;
import org.evolizer.ontology.exporter.api.IRDFPropertyConverter;
import org.evolizer.ontology.exporter.reflection.converter.ConverterRegistry;
import org.evolizer.ontology.provider.util.ISerializationStrategy;
import org.evolizer.ontology.provider.util.Statement;

import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * This class fetches all the semantical descriptions from a instance of any given class. For that, it crawls
 * all the {@link rdf}-annotatedÊmethods of an instance of a {@link rdf}-annotated class and builds a {@link Collection}
 * of {@link Statement}s describing its relationships to other objects.
 * 
 * The current instance is usually considered to be the subject in the &lt;subject, predicate, object&gt; triple,
 * its {@link rdf}-annotated methods are establishing the predicates and their return values the objects. There's
 * a special case whenever a Java Class is used to model a association between two entities but does not represent
 * a concept itself (consider e.g. instances of a class <code>Relationship</code> that connects, e.g., two <code>Person</code>
 * instances, instead of a method <code>getRelatedPerson()</code> in <code>Person</code>). The class is then not
 * considered to be a subject, but rather a predicate, and the visitor looks for methods annotated with {@link subject}
 * and {@link object} to  be able make a complete statement. 
 * 
 * @author wuersch
 * 
 */
public class RDF2Visitor implements IReflectionProvider.IVisitor {
	private final static Logger logger = EvolizerOntologyExporterPlugin.getLogManager().getLogger(RDF2Visitor.class.getName());
	
	private enum Mode {NORMAL, PREDICATE, IGNORE};
	private Mode mode = Mode.NORMAL;
	
	private Collection<Statement<?, ?>> statements;
	
	private Statement<Object, Object> tmpStatement;
	
	
	/**
	 * Constructor.
	 */
	public RDF2Visitor() {
		statements = new ArrayList<Statement<?, ?>>();
	}
	
	/**
	 * This method is called first when a instance of class is search for semantical descriptions.
	 * It decides whether the class under investigation models a predicate or a subject and creates a
	 * partial statement accordingly.
	 * 
	 * @see IReflectionProvider.IVisitor#visit(Class, Object)
	 */
	public void visit(Class<?> type, Object instance) {
		rdf annotation = type.getAnnotation(rdf.class);

		if(annotation == null) {
			mode = Mode.IGNORE;
			return;
		}
		
		if(RDFReflectionUtil.isPredicate(type)) {
			mode = Mode.PREDICATE;
			
			try {
				tmpStatement = new Statement<Object, Object>(null, annotation.value(), null);
			} catch (URISyntaxException e) {
				logger.error("Encountered an invalid uri for a property.", e);
			}
			// Do nothing else, but while visiting methods, we will look for @subject and @object tags.
		} else {
			this.mode = Mode.NORMAL;
			
			try {
				// TODO extract a label properly
				String label = RDFReflectionUtil.getLabelFor(instance);
				Statement<Object, Object> statement = new Statement<Object, Object>(instance, RDFS.label.getURI(), label);
				statements.add(statement);
			} catch (URISyntaxException e) {
				logger.error("Encountered an invalid uri for a property.", e);
			}
		}
		
	}
	
	/**
	 * This method is called when all members of the instance of the class are visited and performs
	 * some clean up such as storing the temporary statement in the {@link Collection} of statements
	 * in case the class denoted a predicate.
	 * 
	 * @see IReflectionProvider.IVisitor#visit(Class, Object)
	 */
	public void endVisit(Class<?> type, Object instance) {
		if(mode == Mode.PREDICATE) {
			cleanUp();
		}
		mode = Mode.NORMAL;
	}

	/**
	 * Unused.
	 * @see IReflectionProvider.IVisitor#visit(Constructor, Class, Object)
	 */
	public void visit(Constructor<?> constructor, Class<?> definedIn,
			Object instance) {
		// do nothing
	}

	/**
	 * Unused.
	 * @see IReflectionProvider.IVisitor#visit(Field, Class, Object)
	 */
	public void visit(Field field, Class<?> definedIn, Object instance) {
		// do nothing
	}

	/**
	 * This method called for every {@link Method} in the class. If it is annotated with
	 * {@link rdf}, {@link subject}, or {@link object} annotations, the {@link Method} is invoked
	 * and its return value is added to a statement.
	 * 
	 * @see IReflectionProvider.IVisitor#visit(Method, Class, Object)
	 */
	public void visit(Method method, Class<?> definedIn, Object instance) {
		switch(mode) {
		case NORMAL:
			rdf annotation = method.getAnnotation(rdf.class);
			if(annotation != null) {
				Class<?> returnType = method.getReturnType();
				Object value = DefaultReflectionProvider.invoke(method, instance);
				
				IRDFPropertyConverter converter = ConverterRegistry.getConverterFor(returnType);
				Collection<Statement<?, ?>> statementsToAdd = converter.convert(instance,  RDFReflectionUtil.uriReferenceOf(method), value);
				statements.addAll(statementsToAdd);
			}
			break;
		case PREDICATE:
			subject sAnnotation = method.getAnnotation(subject.class);
			if(sAnnotation != null) {
				Object value = DefaultReflectionProvider.invoke(method, instance);
				tmpStatement.setSubject(value);
			}
			
			object oAnnotation = method.getAnnotation(object.class);
			if(oAnnotation != null) {
				// TODO only 1:1 relations are handled, need to take care of 1:n relations as well.
				Object value = DefaultReflectionProvider.invoke(method, instance);
				tmpStatement.setObject(value);
			}
			
			break;			
		case IGNORE:
			// do nothing, skip
			break;
		}
	}
	
	/**
	 * Serializes the collected statements according to a {@link ISerializationStrategy}. Calling
	 * this method multiple times (possibly to export incrementally) is fine, as long as the same
	 * instance of {@link ISerializationStrategy} is re-used.
	 * 
	 * @param serializer
	 */
	public void serialize(ISerializationStrategy<Statement<?,?>> serializer) {		
		for (Iterator<Statement<?,?>> iterator = statements.iterator(); iterator.hasNext();) {
			Statement<?, ?> statement = iterator.next();
			statement.setSerializer(serializer);
			statement.serialize();	
		
			// remove the serialized statement, so that it will be skipped if the serialize-method
			// is called multiple times.
			iterator.remove();
		}
	}

	/**
	 * Cleanes up temporary data after processing an instance is completed.
	 */
	private void cleanUp() {
		if(
			tmpStatement != null &&
			tmpStatement.isComplete()
		) {
			statements.add(tmpStatement);
		}
		
		tmpStatement = null;
	}
}
