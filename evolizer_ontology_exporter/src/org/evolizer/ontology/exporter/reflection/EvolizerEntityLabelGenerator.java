package org.evolizer.ontology.exporter.reflection;

import org.evolizer.core.hibernate.model.api.IEvolizerModelEntity;


/**
 * Simple strategy for generating a label. Just calls the toString()-method,
 * which is adequate as long as it returns a short but concise name.
 * 
 * @author wuersch
 *
 */
public class EvolizerEntityLabelGenerator implements ILabelGenerator {
	public String getLabelFor(Object resource) {	
		return ((IEvolizerModelEntity)resource).getLabel();
	}
}
