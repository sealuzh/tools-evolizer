package org.evolizer.ontology.exporter.reflection;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLEncoder;

import org.apache.log4j.Logger;
import org.evolizer.core.util.reflection.api.impl.DefaultReflectionProvider;
import org.evolizer.ontology.exporter.EvolizerOntologyExporterPlugin;
import org.evolizer.ontology.exporter.api.IURIGenerator;

/**
 * Simple strategy for generating an uri. Uses the fully qualified name of the class of an object and appends an
 * id to distinguish it from other instances of the same class. If the object does not provide an getId()-method,
 * the strategy fails.
 * 
 * @author wuersch
 *
 */
public class DefaultURIGenerator implements IURIGenerator {
	private final static Logger logger = EvolizerOntologyExporterPlugin.getLogManager().getLogger(DefaultURIGenerator.class.getName());
	
	public String getIdentifier(Object resource) {
		String uri = resource.getClass().getName().toLowerCase() + idFor(resource);
		
		try {
			return URLEncoder.encode(uri, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace(); // should not happen
			return null;
		}
	}
	
	public static Long idFor(Object object) {
		Long id = null;

		try {
			Method method = object.getClass().getMethod("getId");
			id = (Long) DefaultReflectionProvider.invoke(method, object);
		} catch (SecurityException e) {
			logger.error("Could not generate an uri for resource because of some strange reflection/security issues.", e);
		} catch (NoSuchMethodException e) {
			logger.error("Could not generate an uri because resource does not have a getId() method", e);
		}

		return id;
	}
	
	@Override
	public boolean canGenerate(Class<?> type) {
		return true;
	}
}
