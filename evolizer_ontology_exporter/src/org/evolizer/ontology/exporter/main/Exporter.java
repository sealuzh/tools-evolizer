package org.evolizer.ontology.exporter.main;

import java.io.OutputStream;

import org.evolizer.core.util.reflection.api.IReflectionProvider;
import org.evolizer.core.util.reflection.api.impl.DefaultReflectionProvider;
import org.evolizer.ontology.exporter.reflection.RDF2Visitor;
import org.evolizer.ontology.provider.util.ISerializationStrategy;
import org.evolizer.ontology.provider.util.RDFStatementSerializer;
import org.evolizer.ontology.provider.util.Statement;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Model;

/**
 * Instances of this class allow to export rdf-annotated Evolizer models to rdf. Objects
 * intended to be exported need to be annotated correctly (see
 * {@link org.evolizer.ontology.annotations.rdf}) and have to respond to
 * <code>getId()</code> by returning an unique identifier (unique for all
 * instances of their Java class). <br>
 * <br>
 * Usage: <br>
 * <br>
 * 
 * <pre>
 * List&lt;MyObject&gt; result = session.query(&quot;from MyObject);
 * Exporter exporter = new Exporter();
 * 
 * for(MyObject object : result) {
 * 	exporter.export(object);
 * }
 * 
 * exporter.writeModel();
 * 
 * </pre>
 * 
 * TODO Individuals should also incorporate project/cvs url, otherwise the URI is not really valid (because it is not an unique identifier)
 * TODO Inference?
 * @author wuersch
 * 
 */
public class Exporter {
	//private final static Logger logger = Activator.getLogManager().getLogger(Exporter.class.getName());
	
	private RDF2Visitor visitor;
	private IReflectionProvider reflectionProvider;
	private ISerializationStrategy<Statement<?,?>> serializer;
	
	private String baseURI;

	/**
	 * Constructor. Expects a base uri for the exported ontology instances.
	 * 
	 * @param baseURI a uri that will be the base of all exported instances.
	 * 		  Can be null but then {@link #writeModel()} will fail. 
	 */
	public Exporter(String baseURI) {
		this.baseURI = baseURI;
		visitor = new RDF2Visitor();
		reflectionProvider = new DefaultReflectionProvider();
		serializer = new RDFStatementSerializer();
	}
	
	/**
	 * Allows to preload ontologies, so that they can be also exported together with
	 * the instances. Not yet implemented.
	 * 
	 * @param paths
	 */
	public void addOntologies(String... paths) {
//		try {
//			((RDFStatementSerializer)serializer).loadOntologies(paths);
//		} catch (IOException e) {
//			logger.error("Could not add ontologies to the export.", e);
//		}
	}

	/**
	 * Allows to preload an ontology, so that it can be also exported together with
	 * the instances. Not yet implemented.
	 * 
	 * TODO path should be probably replaced by something more useful/convenient (file handlers? Jena Models?).
	 * 
	 * @param path
	 */
	public void addOntology(String path) {
//			try {
//				((RDFStatementSerializer)serializer).loadOntology(path);
//			} catch (IOException e) {
//				logger.error("Could not add ontology to the export.", e);
//			}
	}

	/**
	 * Adds an instance to the ontology model.
	 * 
	 * @param object an object that has to be rdf-annotated. 
	 */
	public void export(Object object) {
		reflectionProvider.process(object, visitor);
	}

	/**
	 * Writes the information stored in the current model to the <code>PrintStream</code>.
	 * 
	 * @param out
	 */
	public void writeModel(OutputStream out) {
		visitor.serialize(serializer);
		
		((OntModel) getModel()).writeAll(out, "RDF/XML", baseURI);
		
		/*
		   	TODO baseURI does not work. Fix, suggested by mhert might be: 
		  
		   	Hier ein Code-Snippet um mit Jena ein xml:base ins output file zu schreiben:

		  	// Einlesen des Inputs, wie gewohnt
			String xmlBase = "http://www.example.com/";
			Model model = ModelFactory.createDefaultModel();
			model.read(new FileInputStream("input.xml"), xmlBase, "RDF/XML");

		 	// ...

		 	// Schreiben des Outputs mit xml:base
			RDFWriter writer = model.getWriter("RDF/XML"); // funktioniert auch mit "RDF/XML-ABBREV"
			writer.setProperty("xmlbase", xmlBase);
			writer.write(model, new FileOutputStream("output.xml"), xmlBase); // Model �ber den RDFWriter serialisieren, �ber model.write() funktioniert es nicht!
		 */
	}

	/**
	 * Writes the information stored in the current model to the <code>System.out</code>.
	 */
	public void writeModel() {
		writeModel(System.out);
	}

	/**
	 * Returns the current Jena model.
	 * 
	 * @return
	 */
	public Model getModel() {
		return ((RDFStatementSerializer) serializer).getModel();
	}

//	public void query() {
//		Model model = visitor.getModel();
//		Query query = QueryFactory.create(
//											"SELECT ?x ?fp " +
//											"WHERE {	?x <http://www.evolizer.org/ontology/cvs.owl#contains> ?y ." +
//											"		?x <http://www.evolizer.org/ontology/cvs.owl#fullPath> ?fp ." +
//											"		?y <http://www.evolizer.org/ontology/cvs.owl#fullPath> \"/src/Main.java\"}"
//										) ;
//		QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
//		try {
//			ResultSet results = qexec.execSelect() ;
//			ResultSetFormatter.out(System.out, results, query) ;
////			for ( ; results.hasNext() ; ) {
////				QuerySolution soln = results.nextSolution() ;
////				RDFNode x = soln.get("varName") ;       // Get a result variable by name.
////				Resource r = soln.getResource("VarR") ; // Get a result variable - must be a resource
////				Literal l = soln.getLiteral("VarL") ;   // Get a result variable - must be a literal
////			}
//		} finally { qexec.close() ; }
//	}
}