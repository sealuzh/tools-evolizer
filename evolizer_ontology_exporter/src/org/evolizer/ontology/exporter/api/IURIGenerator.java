package org.evolizer.ontology.exporter.api;


/**
 * A strategy for generating uri's for a given resource.
 * 
 * @author wuersch
 *
 * @param <T> The type of resource that the uri generator applies to.
 */
public interface IURIGenerator {
	// TODO Should probably throw a checked exception, since errors need to be handled, otherwise an inconsistent export will be the result.
	public String getIdentifier(Object resource);

	public boolean canGenerate(Class<?> type);
}
