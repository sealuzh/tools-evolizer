package org.evolizer.ontology.provider.util;


public interface ISerializationStrategy<T> {
	public void serialize(T serializableObject);
}
