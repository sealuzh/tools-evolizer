package org.evolizer.core.util;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.evolizer.core.util.collections.FilteringIterator;
import org.evolizer.core.util.collections.IPredicate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FilteringIteratorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testEmptyPredicate() {
		List<Dummy> dummyList = new ArrayList<Dummy>();
		dummyList.add(new Dummy("first element"));
		dummyList.add(new Dummy("second element"));
		dummyList.add(new Dummy("third element"));
		
		Iterator<Dummy> dummyIterator = dummyList.iterator();
		
		IPredicate<Dummy> emptyPredicate = new IPredicate<Dummy>() {
			public boolean evaluate(Dummy object) {
				return true;
			}
		};
		
		FilteringIterator<Dummy> filteringIterator = new FilteringIterator<Dummy>(dummyIterator, emptyPredicate);
		
		assertTrue("The iterator should have more elements.", filteringIterator.hasNext());
		assertEquals("The iterator returned an unexpected element.", "first element", filteringIterator.next().getContent());
		assertTrue("The iterator should have more elements.", filteringIterator.hasNext());
		assertEquals("The iterator returned an unexpected element.", "second element", filteringIterator.next().getContent());
		assertTrue("The iterator should have more elements.", filteringIterator.hasNext());
		assertEquals("The iterator returned an unexpected element.", "third element", filteringIterator.next().getContent());
		assertFalse("The iterator should be empty by now.", filteringIterator.hasNext());
	}
	
	@Test(expected = NoSuchElementException.class)
	public void testEmptyListIterator() {
		List<Dummy> emptyList = new ArrayList<Dummy>();
		
		
		Iterator<Dummy> dummyIterator = emptyList.iterator();
		
		IPredicate<Dummy> emptyPredicate = new IPredicate<Dummy>() {
			public boolean evaluate(Dummy object) {
				return true;
			}
		};
		
		FilteringIterator<Dummy> filteringIterator = new FilteringIterator<Dummy>(dummyIterator, emptyPredicate);
		
		assertFalse("The iterator should have no elements.", filteringIterator.hasNext());
		filteringIterator.next();
	}
	
	@Test
	public void testSimplePredicate() {
		List<Dummy> dummyList = new ArrayList<Dummy>();
		dummyList.add(new Dummy("first element"));
		dummyList.add(new Dummy("second element - remove me!"));
		dummyList.add(new Dummy("third element"));
		
		Iterator<Dummy> dummyIterator = dummyList.iterator();
		
		IPredicate<Dummy> emptyPredicate = new IPredicate<Dummy>() {
			public boolean evaluate(Dummy object) {
				String content = object.getContent();
				return !content.equals("second element - remove me!");
			}
		};
		
		FilteringIterator<Dummy> filteringIterator = new FilteringIterator<Dummy>(dummyIterator, emptyPredicate);
		
		assertTrue("The iterator should have more elements.", filteringIterator.hasNext());
		assertEquals("The iterator returned an unexpected element.", "first element", filteringIterator.next().getContent());
		assertTrue("The iterator should have more elements.", filteringIterator.hasNext());
		assertEquals("The iterator returned an unexpected element.", "third element", filteringIterator.next().getContent());
		assertFalse("The iterator should be empty by now.", filteringIterator.hasNext());
	}

	private class Dummy {
		private String content;
	
		public Dummy(String content) {
			this.content = content;
		}
		
		public String getContent() {
			return content;
		}
	}
}
