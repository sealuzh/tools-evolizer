package org.evolizer.core.hibernate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.evolizer.core.hibernate.model.api.IEvolizerModelEntity;

@Entity
public class DummyBusinessEntity implements IEvolizerModelEntity{
	private Long id;
	private String aString;
	
	public String getAString() {
		return aString;
	}

	public void setAString(String string) {
		aString = string;
	}

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Transient
	public String getLabel() {
		return "aString";
	}

	@Transient
	public String getURI() {
		// TODO Auto-generated method stub
		return null;
	}

}
