/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.svn.importer.test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.model.resources.entities.fs.Directory;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.versioncontrol.cvs.model.entities.Branch;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.evolizer.versioncontrol.cvs.model.entities.VersionedFile;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNImporter;
import org.evolizer.versioncontrol.svn.importer.EvolizerSVNPlugin;
import org.evolizer.versioncontrol.svn.model.entities.SVNRelease;
import org.evolizer.versioncontrol.svn.model.entities.SVNRevision;
import org.evolizer.versioncontrol.svn.model.entities.SVNVersionedFile;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.SVNProperty;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

/**
 * This abstract test should be extended by any test of the {@link SVNImportTest}. It offers all the basic
 * functionalities to create, connect and talk to the DB through Hibernate, hiding the details to the actual tests.
 * 
 * @author ghezzi
 * 
 */
public abstract class AbstractEvolizerSVNImporterTest {

    protected static String sSVNUrl;
    protected static String sSVNUser;
    protected static String sSVNPwd;
    protected static String sDBConfig = "./config/db.properties";
    protected static String sDBUrl = "";
    protected static String sDBDialect = "";
    protected static String sDBDriverName = "";
    protected static String sDBUser = "";
    protected static String sDBPasswd = "";
    protected static EvolizerSVNImporter sImporter;

    /*** DB Settings ***/
    protected static IEvolizerSession sSession;
    protected static EvolizerSessionHandler sSessionHandler;

    /**
     * Sets up the Hibernate session, using the db.properties configuration file.
     * 
     * @throws Exception
     *             If problems setting up the Hibernate session or reading the configuration file arise.
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        /***** Loading the DB settings *****/
        InputStream propertiesInputStream = EvolizerSVNPlugin.openFile(sDBConfig);

        if (propertiesInputStream != null) {
            Properties props = new Properties();
            props.load(propertiesInputStream);
            propertiesInputStream.close();

            sDBUrl = props.getProperty("dbUrl");
            sDBDialect = props.getProperty("dbDialect");
            sDBDriverName = props.getProperty("dbDriverName");
            sDBUser = props.getProperty("dbUser");
            sDBPasswd = props.getProperty("dbPasswd");
        }
        propertiesInputStream.close();

        /***** Setting up the Hibernate Session *****/

        sSessionHandler = EvolizerSessionHandler.getHandler();
        sSessionHandler.initSessionFactory(sDBUrl, sDBUser, sDBPasswd);
        sSessionHandler.createSchema(sDBUrl, sDBDialect, sDBDriverName, sDBUser, sDBPasswd);
        sSession = sSessionHandler.getCurrentSession(sDBUrl);
    }

    public static void setUpBeforeClass2() throws Exception {
        /***** Loading the DB settings *****/
        InputStream propertiesInputStream = EvolizerSVNPlugin.openFile(sDBConfig);

        if (propertiesInputStream != null) {
            Properties props = new Properties();
            props.load(propertiesInputStream);
            propertiesInputStream.close();

            sDBUrl = props.getProperty("dbUrl");
            sDBDialect = props.getProperty("dbDialect");
            sDBDriverName = props.getProperty("dbDriverName");
            sDBUser = props.getProperty("dbUser");
            sDBPasswd = props.getProperty("dbPasswd");
        }
        propertiesInputStream.close();

        /***** Setting up the Hibernate Session *****/

        sSessionHandler = EvolizerSessionHandler.getHandler();
        sSessionHandler.initSessionFactory(sDBUrl, sDBUser, sDBPasswd);
        sSession = sSessionHandler.getCurrentSession(sDBUrl);
    }
    /**
     * Closes the Hibernate session and drops the schema created during the test.
     * 
     * @throws Exception
     *             If problems tearing down the Hibernate session happen.
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        sSession.close();
        sSessionHandler.cleanupHibernateSessions();
        sSessionHandler.dropSchema(sDBUrl, sDBDialect, sDBDriverName, sDBUser, sDBPasswd);
        sSessionHandler = null;
    }

    /***** Helper Methods *****/

    /**
     * Fetches all the existing releases.
     * 
     * @return The releases found.
     * @see SVNRelease
     */
    public List<SVNRelease> getReleases() {
        return sSession.query("from SVNRelease", SVNRelease.class);
    }

    /**
     * Fetches a specific release, given its name.
     * 
     * @param relName
     *            The release name.
     * @return The release found.
     * @see SVNRelease
     */
    public SVNRelease getRelease(String relName) {
        return sSession.uniqueResult("from SVNRelease where name = '" + relName + "'", SVNRelease.class);
    }

    /**
     * Fetches all the existing branches.
     * 
     * @return The branches found.
     * @see Branch
     */
    public List<Branch> getBranches() {
        return sSession.query("from Branch", Branch.class);
    }

    /**
     * Fetches a specific branch, given its name.
     * 
     * @param branchName
     *            The branch name.
     * @return The branch found.
     * @see Branch
     */
    public Branch getBranch(String branchName) {
        return sSession.uniqueResult("from Branch where name = '" + branchName + "'", Branch.class);
    }

    /**
     * Fetches a specific transaction given its date.
     * 
     * @param date
     *            The date of the transaction to be fetched.
     * @return The transaction found.
     * @see Transaction
     */
    public Transaction getTransaction(String date) {
        String query =
                "Select t from Transaction as t join t.involvedRevisions as inv where t.started = '" + date + "'";
        return sSession.uniqueResult(query, Transaction.class);

    }

    /**
     * Fetches a specific file revision.
     * 
     * @param versionNumber
     *            The revision number.
     * @param path
     *            The svn path to identifying the file to be fetched.
     * @return The revision found.
     * @see SVNRevision
     */
    public SVNRevision getSpecificFileVersion(long versionNumber, String path) {
        return sSession.uniqueResult("select rv from SVNRevision as rv, File as fl where rv.number = " + versionNumber
                + " and rv.file.id = fl.id and fl.path = '" + path + "'", SVNRevision.class);
    }

    /**
     * Fetches a specific versioned file.
     * 
     * @param name
     *            The complete file name.
     * @return the file found.
     * @see VersionedFile
     */
    public SVNVersionedFile getFile(String name) {
        return sSession.uniqueResult("from SVNVersionedFile where path = '" + name + "'", SVNVersionedFile.class);
    }

    /**
     * Fetches a specific versione directory.
     * 
     * @param name
     *            The complete directory name.
     * @return The directory found.
     * @see Directory
     */
    public Directory getDirectory(String name) {
        return sSession.uniqueResult("from Directory where path = '" + name + "'", Directory.class);
    }

    /**
     * Fetches all the existing persons (svn committers).
     * 
     * @return The persons found.
     * @see Person
     */
    public List<Person> getPersons() {
        return sSession.query("from Person", Person.class);
    }

    /**
     * Fetches a specific person (svn committer).
     * 
     * @param name
     *            The name of the person to be fetched.
     * @return The specific person found.
     * @see Person
     */
    public Person getSpecificPerson(String name) {
        return sSession.uniqueResult("from Person where firstName ='" + name + "'", Person.class);
    }

    /**
     * Creates a new {@link SVNRepository}.
     * 
     * @return The repository created.
     * @throws SVNException
     *             If problems setting up the repositories arise.
     */
    protected static SVNRepository getRepository() throws SVNException {
        // For using over http:// and https://
        DAVRepositoryFactory.setup();
        // For using over svn:// and svn+xxx://
        SVNRepositoryFactoryImpl.setup();
        // For using over file:///
        FSRepositoryFactory.setup();

        SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(sSVNUrl));

        // Create Authentication manager
        ISVNAuthenticationManager authManager;
        if ((sSVNUser == null) && (sSVNPwd == null)) {
            authManager = SVNWCUtil.createDefaultAuthenticationManager();
        } else {
            authManager = SVNWCUtil.createDefaultAuthenticationManager(sSVNUser, sSVNPwd);
        }
        repository.setAuthenticationManager(authManager);

        return repository;
    }

    /**
     * Fetches the content of a specific revision of a file.
     * 
     * @param path
     *            The repository path to the file of interest.
     * @param revisionNumber
     *            The revision number.
     * @return The content of the file.
     * @throws SecurityException
     *             If problems arise
     * @throws NoSuchFieldException
     *             If problems arise
     * @throws IllegalArgumentException
     *             If problems arise
     * @throws IllegalAccessException
     *             If problems arise
     */
    protected static String fetchFile(String path, long revisionNumber)
            throws SecurityException,
            NoSuchFieldException,
            IllegalArgumentException,
            IllegalAccessException {
        SVNProperties fileProperties = new SVNProperties();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String res = "";
        try {
            getRepository().getFile(path, revisionNumber, fileProperties, baos);
            String mimeType = fileProperties.getStringValue(SVNProperty.MIME_TYPE);
            boolean isTextType = SVNProperty.isTextMimeType(mimeType);
            if (isTextType) {
                try {
                    res = baos.toString();
                    baos.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            } else {
                System.out.println("Not a text file.");
            }
        } catch (SVNException e) {
            e.printStackTrace();
        }
        return res;
    }

    protected static Revision getFirstRevision(VersionedFile f) {
        Revision res = null;
        for (Revision r : f.getRevisions()) {
            if (res == null) {
                res = r;
            } else {
                if (Long.parseLong(r.getNumber()) < Long.parseLong(res.getNumber())) {
                    res = r;
                }
            }
        }
        return res;
    }
}
