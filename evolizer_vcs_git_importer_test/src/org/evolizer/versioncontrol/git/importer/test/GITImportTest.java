/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.versioncontrol.git.importer.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.evolizer.core.hibernate.session.EvolizerSessionHandler;
import org.evolizer.core.hibernate.session.api.IEvolizerSession;
import org.evolizer.model.resources.entities.humans.Person;
import org.evolizer.model.resources.entities.humans.Role;
import org.evolizer.versioncontrol.cvs.model.entities.CommitterRole;
import org.evolizer.versioncontrol.cvs.model.entities.ModificationReport;
import org.evolizer.versioncontrol.cvs.model.entities.Release;
import org.evolizer.versioncontrol.cvs.model.entities.Revision;
import org.evolizer.versioncontrol.cvs.model.entities.Transaction;
import org.evolizer.versioncontrol.cvs.model.entities.VersionedFile;
import org.evolizer.versioncontrol.git.importer.EvolizerGITPlugin;
import org.evolizer.versioncontrol.git.importer.EvolizerGitImporter;
import org.evolizer.versioncontrol.git.importer.exceptions.GitImporterException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * SVNImportTest
 * 
 * Tests the SVNImport class
 * 
 * @author ghezzi
 * 
 */
public class GITImportTest {

    private static EvolizerSessionHandler sSessionHandler;
    private static IEvolizerSession sSession;
    private static String sDBConfig = "./config/db.properties";
    private static String sDBUrl;
    private static String sDBDialect;
    private static String sDBDriverName;
    private static String sDBUser;
    private static String sDBPasswd;
    private static EvolizerGitImporter sImporter;

    /**
     * Setting up stuff before the test.
     * 
     * @throws Exception
     *             if problems arise setting up the importer
     */
    @BeforeClass
    public static void setUp() throws Exception {
        InputStream propertiesInputStream = EvolizerGITPlugin.openFile(sDBConfig);

        if (propertiesInputStream != null) {
            Properties props = new Properties();
            props.load(propertiesInputStream);
            propertiesInputStream.close();

            sDBUrl = props.getProperty("dbUrl");
            sDBDialect = props.getProperty("dbDialect");
            sDBDriverName = props.getProperty("dbDriverName");
            sDBUser = props.getProperty("dbUser");
            sDBPasswd = props.getProperty("dbPasswd");

            sSessionHandler = EvolizerSessionHandler.getHandler();
            sSessionHandler.initSessionFactory(sDBUrl, sDBUser, sDBPasswd);
            sSessionHandler.createSchema(sDBUrl, sDBDialect, sDBDriverName, sDBUser, sDBPasswd);
            sSession = sSessionHandler.getCurrentSession(sDBUrl);
        }

        propertiesInputStream.close();

        try {
            sImporter =
                    new EvolizerGitImporter(
                            "./git_test_repo/",
                            sSession,
                            ".java",
                            null);
            sImporter.importProject();
        } catch (GitImporterException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cleaning up after the test
     * 
     * @throws Exception
     *             if problems arise while closing up the test
     */
    @AfterClass
    public static void tearDown() throws Exception {
        sSession.close();
        sSessionHandler.cleanupHibernateSessions();
        // fSessionHandler.dropSchema(fDBUrl, fDBDialect, fDBDriverName, fDBUser, fDBPasswd);
        sSessionHandler = null;
    }

    /**
     * Tests the number and the names of the releases.
     */
    @Test
    public void testReleases() {
        List<Release> releases = sSession.query("from Release", Release.class);
        assertEquals("Number of releases found is not what expected", 2, releases.size());
        ArrayList<String> relNames = new ArrayList<String>();
        relNames.add("first_tag");
        relNames.add("second_tag");
        for (String r : relNames) {
            assertEquals("Release " + r + " was not found", 1, sSession.query(
                    "from Release where name = '" + r + "'",
                    Release.class).size());
        }
    }

    /**
     * Tests a specific release
     * 
     * @throws ParseException
     *             if Date parsing problems happen
     */
    @Test
    public void testSpecificRelease() throws ParseException {
        Release release = sSession.uniqueResult("from Release where name = 'first_tag'", Release.class);
        SimpleDateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy ZZZZ");
        Date d = df.parse("Mon Jul 6 12:13:01 2009 +0200");
        assertTrue(d.equals(release.getTimeStamp()));
        List<Transaction> allTransactions = sSession.query("from Transaction", Transaction.class);
        int count = 0;
        for (Transaction t : allTransactions) {
            if (t.getInvolvedRevisions() != null && t.getInvolvedRevisions().iterator().hasNext()
                && t.getInvolvedRevisions().iterator().next().getReleases().contains(release)) {
                count++;
            }
        }
        assertEquals("Wrong number of transactions found for the tested release", 6, count);
        assertEquals("Wrong number of revisions associated to the tested release", 14, release.getRevisions().size());
    }
    /**
     * Tests the number of transactions
     */
    @Test
    public void testTransactions() {
        List<Transaction> transactions = sSession.query("from Transaction", Transaction.class);
        assertEquals("Wrong number of total transactions", 10, transactions.size());
    }

    /**
     * Tests a specific transaction
     */
    @Test
    public void testSpecificTransaction() {
        Transaction transaction =
                sSession.uniqueResult("from Transaction where started = '2009-07-06 12:06:57'", Transaction.class);
        assertEquals(
                "Start and End date in tested transaction are not the same",
                transaction.getFinished(),
                transaction.getStarted());
        assertEquals("Wrong number of revisions in tested transaction", 3, transaction.getInvolvedRevisions().size());
        Set<Revision> revs = transaction.getInvolvedRevisions();
        ArrayList<Revision> found = new ArrayList<Revision>();
        Iterator<Revision> iter = revs.iterator();
        while (iter.hasNext()) {
            Revision t = iter.next();
            assertFalse("The transaction is associated to the same revision more than one time", found.contains(t));
            assertTrue("The name of one of the files associated to the transaction is wrong", t.getFile().getPath()
                    .equals("test_package/Base.java")
                    || t.getFile().getPath().equals("test_package/Sum.java")
                    || t.getFile().getPath().equals("test_package/ae/Test.java"));
        }
    }

    /**
     * Tests the numebr of revisions
     */
    @Test
    public void testRevisions() {
        List<Revision> revisions = sSession.query("from Revision", Revision.class);
        assertEquals("Wrong total number of revisions", 18, revisions.size());
    }

    /**
     * Tests a specific release
     */
    @Test
    public void testFileRevisions() {
        VersionedFile file =
                sSession.uniqueResult("from VersionedFile where path ='test_package/Base.java'", VersionedFile.class);
        // Testing the tot number of revisions
        assertEquals("The tested VersionedFile has the wrong number of revisions", 3, file.getRevisions().size());
        // Testing first revision
        Revision rev = file.getRevisions().get(0);
        assertEquals(
                "Wrong revision associated to VersionedFile test_package/Base.java",
                "3c7a872a1cf0f1a15b17cee278b356f7113e7005",
                rev.getNumber());
        assertNull("Previous revision of the first ever revision is not null", rev.getPreviousRevision());
        assertEquals("Wrong number of releases associated to VersionedFile test_package/Base.java", 2, rev
                .getReleases().size());
        Object[] rel = rev.getReleases().toArray();
        assertTrue(
                "Wrong releases associated to the first revision of File test_package/Base.java",
                (((Release) rel[0]).getName().equals("first_tag") && ((Release) rel[1]).getName().equals("second_tag"))
                        || (((Release) rel[0]).getName().equals("second_tag") && ((Release) rel[0]).getName().equals(
                                "first_tag")));
        // Testing second revision
        rev = file.getRevisions().get(1);
        assertEquals(
                "Wrong revision associated to VersionedFile test_package/Base.java",
                "49affa954287880a1d569a39c089723523567dee",
                rev.getNumber());
        assertEquals(
                "Broken link between the first and second releases of VersionedFile test_package/Base.java",
                rev,
                file.getRevisions().get(0).getNextRevision());
        assertEquals(
                "Broken link between the second and the first releases of VersionedFile test_package/Base.java",
                file.getRevisions().get(0),
                rev.getPreviousRevision());
        rel = rev.getReleases().toArray();
        assertTrue(
                "Wrong releases associated to the second revision of File test_package/Base.java",
                (((Release) rel[0]).getName().equals("first_tag") && ((Release) rel[1]).getName().equals("second_tag"))
                        || (((Release) rel[0]).getName().equals("second_tag") && ((Release) rel[0]).getName().equals(
                                "first_tag")));
        // Testing third revision
        rev = file.getRevisions().get(2);
        assertEquals(
                "Wrong revision associated to VersionedFile test_package/Base.java",
                "6c92c63b36973d991ca84d8ce6e8a74f8c45cec3",
                rev.getNumber());
        assertEquals(
                "Broken link between the second and third releases of VersionedFile test_package/Base.java",
                rev,
                file.getRevisions().get(1).getNextRevision());
        assertEquals("Broken link between the thrid and second releases of VersionedFile test_package/Base.java", file
                .getRevisions().get(1), rev.getPreviousRevision());
        rel = rev.getReleases().toArray();
        assertTrue(
                "Wrong releases associated to the third revision of File test_package/Base.java",
                (((Release) rel[0]).getName().equals("first_tag") && ((Release) rel[1]).getName().equals("second_tag"))
                        || (((Release) rel[0]).getName().equals("second_tag") && ((Release) rel[0]).getName().equals(
                                "first_tag")));

    }

    /**
     * Tests the number of modification reports
     */
    @Test
    public void testModificationReports() {
        List<ModificationReport> reports = sSession.query("from ModificationReport", ModificationReport.class);
        assertEquals("Wrong total number of ModificationReports", 18, reports.size());
    }

    /**
     * Tests a specific modification report
     */
    @Test
    public void testSpecificModificationReport() {
        VersionedFile file =
                sSession.uniqueResult(
                        "from VersionedFile where path = 'test_package/Variables.java'",
                        VersionedFile.class);
        ModificationReport report = file.getRevisions().get(1).getReport();
        assertEquals("Wrong commit message for the report being tested", "change in trial branch", report
                .getCommitMessage());
        assertEquals("Wrong number of lines deleted for the report being tested", 9, report.getLinesDel());
        assertEquals("Wrong number of lines added for the report being tested", 1, report.getLinesAdd());
        Person p = sSession.uniqueResult("from Person where firstName='Giacomo Ghezzi'", Person.class);
        assertEquals("ModificationReport author is not what expected", p, report.getAuthor());
    }

    /**
     * Tests the number of authors
     */
    @Test
    public void testAuthors() {
        List<Person> authors = sSession.query("from Person", Person.class);
        assertEquals("Wrong total number of Persons/Authors", 1, authors.size());
    }

    /**
     * Tests a specific author
     */
    @Test
    public void testSpecificAuthor() {
        Person p = sSession.uniqueResult("from Person where firstName='Giacomo Ghezzi'", Person.class);
        assertEquals("Wrong author email", "Giacomo@ifidyn165.ifi.uzh.ch", p.getEmail());
        Set<Role> roles = p.getRoles();
        assertEquals("The author being tested has the wrong number of associtated roles", 1, roles.size());
        assertTrue("Wrong role associated to the author being tested", roles.iterator().next() instanceof CommitterRole);
    }
    
    /**
     * Tests a specific file revision contents
     * @throws IOException If some problems arise while fetching the file contents to use in the test
     */
    @Test
    public void testFileContents() throws IOException {
        VersionedFile vf = sSession.uniqueResult("from VersionedFile where path = 'test_package/Sum.java'", VersionedFile.class);
        for (Revision r : vf.getRevisions()) {
            if (r.getNumber().compareTo("614e3cc9b980a6fc317c1816867f2fa3dbf84aad") == 0) {
                
                BufferedReader in = new BufferedReader(new InputStreamReader(EvolizerGITPlugin.openFile("./Sum.java")));
                String res = "";
                String i = in.readLine();
                while (i != null) {
                    res = res + i + "\n";
                    i = in.readLine();
                }
                res = res.substring(0, res.length() - 1);
                assertTrue("Contents fetched by the importer for /test_package/Sum.java are not the ones expeceted", r.getSource().compareTo(res) == 0);
            }
        }
    }
}
