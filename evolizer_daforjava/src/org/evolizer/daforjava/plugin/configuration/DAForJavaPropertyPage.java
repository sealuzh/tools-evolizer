/*
 * Copyright 2009 Martin Pinzger, Delft University of Technology,
 * and University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.daforjava.plugin.configuration;


import org.apache.log4j.Logger;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.dialogs.PropertyPage;
import org.evolizer.core.exceptions.EvolizerException;
import org.evolizer.core.exceptions.EvolizerRuntimeException;
import org.evolizer.daforjava.DAForJavaPlugin;

/**
 * The Eclipse property page for general DAForJava settings. There will be more
 * properties coming soon.
 * 
 * @author pinzger
 */
public class DAForJavaPropertyPage extends PropertyPage implements Listener {
    
    /** The logger. */
    private Logger fLogger = DAForJavaPlugin.getLogManager().getLogger(DAForJavaPropertyPage.class.getCanonicalName());

    /** The project. */
    private IProject fProject;

    /** The enable in memory db button. */
    private Button fEnableInMemoryDBButton;
    
    /** The enable evolizer db button. */
    private Button fEnableEvolizerDBButton;

    /** The status label for preference store. */
    private Label fStatusLabelForPreferenceStore;

    /** The is in memory db enabled. */
    private Boolean fIsInMemoryDBEnabled;
    
    /** The is evolizer db enabled. */
    private Boolean fIsEvolizerDBEnabled;

    /**
     * The constructor.
     */
    public DAForJavaPropertyPage() {
        super();
    }

    /**
     * Initialize values.
     */
    private void initializeValues() {
        try {
            String isInMemoryDBEnabled = (fProject.getPersistentProperty(DAForJavaPreferences.DB_USE_HSQLDB_INMEMORY) != null) ? fProject.getPersistentProperty(DAForJavaPreferences.DB_USE_HSQLDB_INMEMORY) : "true";
            fIsInMemoryDBEnabled = Boolean.valueOf(isInMemoryDBEnabled);
            String isEvolizerDBEnabled = (fProject.getPersistentProperty(DAForJavaPreferences.DB_USE_EVOLIZER) != null) ? fProject.getPersistentProperty(DAForJavaPreferences.DB_USE_EVOLIZER) : "false";
            fIsEvolizerDBEnabled = Boolean.valueOf(isEvolizerDBEnabled);
        } catch (CoreException e) {
            e.printStackTrace();
        }
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    protected Control createContents(Composite parent) {
        retrieveProject();
        initializeValues();

        Composite mainComposite = new Composite(parent, SWT.NONE);

        GridLayout mainGridLayout = new GridLayout();
        mainComposite.setLayout(mainGridLayout);
        mainGridLayout.numColumns = 2;

        Group group = new Group(mainComposite, SWT.BORDER);
        group.setText("Database configuration");
        group.setLayoutData(new GridData(GridData.FILL_BOTH));

        GridLayout gridLayout = new GridLayout();
        group.setLayout(gridLayout);
        gridLayout.numColumns = 1;

        fEnableInMemoryDBButton = new Button(group, SWT.RADIO);
        fEnableInMemoryDBButton.setText("Use HSQLDB in-memory database");
        fEnableInMemoryDBButton.setSelection(fIsInMemoryDBEnabled);
        fEnableInMemoryDBButton.addListener(SWT.Selection, this);

        fEnableEvolizerDBButton = new Button(group, SWT.RADIO);
        fEnableEvolizerDBButton.setText("Use Evolizer databse");
        fEnableEvolizerDBButton.setSelection(fIsEvolizerDBEnabled);
        fEnableEvolizerDBButton.addListener(SWT.Selection, this);

        new Label(mainComposite, SWT.NONE);

        fStatusLabelForPreferenceStore = new Label(mainComposite, SWT.NONE);
        fStatusLabelForPreferenceStore.setText(" ");
        GridData statusGridData = new GridData(GridData.FILL_HORIZONTAL);
        fStatusLabelForPreferenceStore.setLayoutData(statusGridData);

        try {
            setInitialState(isJavaEnabled()); //Properties are only available for Java projects.
        } catch (EvolizerException e) { //If something goes wrong, it's better to remove user controls and to display an error msg instead.
            fLogger.error("Error while initializing property page" + e.getMessage(), e);

            mainComposite.dispose();

            mainComposite = new Composite(parent, SWT.NONE);
            mainGridLayout = new GridLayout();
            mainComposite.setLayout(mainGridLayout);

            Label label = new Label(mainComposite, SWT.NONE);
            label.setText(e.getMessage());
        }

        return mainComposite;
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    public void createControl(Composite parent) {
        super.createControl(parent);
    }

    /**
     * Update radio buttons.
     */
    private void updateRadioButtons() {
        if (fEnableInMemoryDBButton.getSelection()) {
            fIsInMemoryDBEnabled = true;
            fIsEvolizerDBEnabled = false;
        } else if (fEnableEvolizerDBButton.getSelection()) {
            fIsInMemoryDBEnabled = false;
            fIsEvolizerDBEnabled = true;
        }
    }

    /**
     * Retrieve project.
     */
    private void retrieveProject() {
        fProject = (IProject) ((IAdaptable) getElement()).getAdapter(IProject.class);
    }

    /**
     * Sets the initial state.
     * 
     * @param enabled the new initial state
     */
    private void setInitialState(boolean enabled) {
        setEnabled(enabled);
    }
    
    /**
     * Sets the enabled.
     * 
     * @param enabled true or false
     */
    private void setEnabled(boolean enabled) {
        fEnableInMemoryDBButton.setEnabled(enabled);
        fEnableEvolizerDBButton.setEnabled(enabled);
    }
    
    /**
     * Checks if the project has the Java nature.
     * 
     * @return true, if is java enabled
     * 
     * @throws EvolizerException the evolizer exception
     */
    private boolean isJavaEnabled() throws EvolizerException {
        try {
            return fProject.hasNature("org.eclipse.jdt.core.javanature");
        } catch (CoreException e) {
            throw new EvolizerException(e);
        }
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    protected void performApply() {
        storeUserInput();
        fStatusLabelForPreferenceStore.setText("Database configuration stored/updated.");
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    protected void performDefaults() {
        fEnableInMemoryDBButton.setSelection(true);
        fEnableEvolizerDBButton.setSelection(false);

        fStatusLabelForPreferenceStore.setText("Defaults restored.");
    }

    /**
     * Store user input.
     */
    private void storeUserInput() {
        try {
            fProject.setPersistentProperty(DAForJavaPreferences.DB_USE_HSQLDB_INMEMORY, fIsInMemoryDBEnabled.toString());
            fProject.setPersistentProperty(DAForJavaPreferences.DB_USE_EVOLIZER, fIsEvolizerDBEnabled.toString());
        } catch (CoreException ce) {
            fLogger.error("Error storing the properties", ce);
            throw new EvolizerRuntimeException("Error storing the properties", ce);
        }
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    public boolean performOk() {
        storeUserInput();

        return super.performOk();
    }

    /** 
     * {@inheritDoc}
     */
    public void handleEvent(Event event) {
        if (event.widget.equals(fEnableInMemoryDBButton) 
                || event.widget.equals(fEnableEvolizerDBButton)) {
            updateRadioButtons();
        }
    }
}
