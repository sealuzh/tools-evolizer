/*
 * Copyright 2009 Martin Pinzger, Delft University of Technology,
 * and University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.evolizer.daforjava.plugin.configuration;

import org.eclipse.core.runtime.QualifiedName;

/**
 * This class holds the constants used to reference preference settings.
 * More settings to come.
 * 
 * @author pinzger
 */
public final class DAForJavaPreferences {

    private DAForJavaPreferences() {
    }
    
    /** Preference for the using the HSQLDB in-memory database. */
    public static final QualifiedName DB_USE_HSQLDB_INMEMORY = new QualifiedName("org.daforjava", "db.inmemory");
    
    /** Preference for the using the database specified by the Evolizer properties. */
    public static final QualifiedName DB_USE_EVOLIZER = new QualifiedName("org.daforjava", "db.useEvolizer");
}
